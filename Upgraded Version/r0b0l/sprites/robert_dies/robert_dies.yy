{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 111,
  "bbox_top": 0,
  "bbox_bottom": 47,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 112,
  "height": 48,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"1d562afb-12f0-4599-aa0a-1405dbda8336","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1d562afb-12f0-4599-aa0a-1405dbda8336","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"1d562afb-12f0-4599-aa0a-1405dbda8336","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d21616f0-a9c9-4c69-88b6-ab52c8460788","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d21616f0-a9c9-4c69-88b6-ab52c8460788","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"d21616f0-a9c9-4c69-88b6-ab52c8460788","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"04c6641a-8097-4bb2-875d-e71b21ef640e","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"04c6641a-8097-4bb2-875d-e71b21ef640e","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"04c6641a-8097-4bb2-875d-e71b21ef640e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8c6e0d67-0c3e-4f6f-a1b5-c79438e2de75","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8c6e0d67-0c3e-4f6f-a1b5-c79438e2de75","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"8c6e0d67-0c3e-4f6f-a1b5-c79438e2de75","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c9ee29a9-87e5-41a9-ba4f-28901dc89b5c","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c9ee29a9-87e5-41a9-ba4f-28901dc89b5c","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"c9ee29a9-87e5-41a9-ba4f-28901dc89b5c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fd1fa484-cb9e-4e81-8c5d-bd1979c978a9","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fd1fa484-cb9e-4e81-8c5d-bd1979c978a9","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"fd1fa484-cb9e-4e81-8c5d-bd1979c978a9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9affedec-5232-4802-a426-87f57b166c56","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9affedec-5232-4802-a426-87f57b166c56","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"9affedec-5232-4802-a426-87f57b166c56","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"24b57f77-cb35-4316-bf32-86e442a2cc18","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"24b57f77-cb35-4316-bf32-86e442a2cc18","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"24b57f77-cb35-4316-bf32-86e442a2cc18","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e6a6eade-7a26-4fa5-9d18-a7bbd42862a9","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e6a6eade-7a26-4fa5-9d18-a7bbd42862a9","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"e6a6eade-7a26-4fa5-9d18-a7bbd42862a9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"32555a80-010e-4848-baed-5785dcdaa238","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"32555a80-010e-4848-baed-5785dcdaa238","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"32555a80-010e-4848-baed-5785dcdaa238","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"276ddb37-5a63-4a6f-8895-bff87887d870","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"276ddb37-5a63-4a6f-8895-bff87887d870","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"276ddb37-5a63-4a6f-8895-bff87887d870","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1714a615-efd1-4d70-a2d1-228ba5a94144","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1714a615-efd1-4d70-a2d1-228ba5a94144","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"1714a615-efd1-4d70-a2d1-228ba5a94144","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c73b9017-3263-4f57-81f9-5b7d53fac206","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c73b9017-3263-4f57-81f9-5b7d53fac206","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"c73b9017-3263-4f57-81f9-5b7d53fac206","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"131aa12d-dd47-4e1d-a789-077afa09ea06","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"131aa12d-dd47-4e1d-a789-077afa09ea06","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"131aa12d-dd47-4e1d-a789-077afa09ea06","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d4d08b62-7d47-44d6-8122-482b5857a698","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d4d08b62-7d47-44d6-8122-482b5857a698","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"d4d08b62-7d47-44d6-8122-482b5857a698","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"628817da-d7f9-40fd-a554-2ccbda9873d4","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"628817da-d7f9-40fd-a554-2ccbda9873d4","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"628817da-d7f9-40fd-a554-2ccbda9873d4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ef07ca9f-ecc1-41d8-b56c-1fe2c8c459bd","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ef07ca9f-ecc1-41d8-b56c-1fe2c8c459bd","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"ef07ca9f-ecc1-41d8-b56c-1fe2c8c459bd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"755afbe7-1c4b-4c24-8f9f-c6e6f6d27f92","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"755afbe7-1c4b-4c24-8f9f-c6e6f6d27f92","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"755afbe7-1c4b-4c24-8f9f-c6e6f6d27f92","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8667b21b-12ea-4c0f-a211-9a3bc5d69f5a","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8667b21b-12ea-4c0f-a211-9a3bc5d69f5a","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"8667b21b-12ea-4c0f-a211-9a3bc5d69f5a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bcca11bc-d19f-4bd6-91cb-4fe97315c27c","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bcca11bc-d19f-4bd6-91cb-4fe97315c27c","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"bcca11bc-d19f-4bd6-91cb-4fe97315c27c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7b864b7b-6bad-41ad-a985-4de8d15554a9","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7b864b7b-6bad-41ad-a985-4de8d15554a9","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"7b864b7b-6bad-41ad-a985-4de8d15554a9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"65bf327e-3fe1-48e8-8b72-3ff95e6eae6c","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"65bf327e-3fe1-48e8-8b72-3ff95e6eae6c","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"65bf327e-3fe1-48e8-8b72-3ff95e6eae6c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d53c841a-eec1-4ea5-a3b8-576d396c8293","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d53c841a-eec1-4ea5-a3b8-576d396c8293","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"d53c841a-eec1-4ea5-a3b8-576d396c8293","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5c72616d-4bf9-4f80-9335-9b7b396a589c","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5c72616d-4bf9-4f80-9335-9b7b396a589c","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"5c72616d-4bf9-4f80-9335-9b7b396a589c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"54df120f-b648-4756-a5eb-1c43cf11c9bb","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"54df120f-b648-4756-a5eb-1c43cf11c9bb","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"54df120f-b648-4756-a5eb-1c43cf11c9bb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cb1a9b28-ea54-4eaa-bfbd-7f569bb77093","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cb1a9b28-ea54-4eaa-bfbd-7f569bb77093","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"cb1a9b28-ea54-4eaa-bfbd-7f569bb77093","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"be3b5b7e-bc73-41a5-addc-821e7ae45563","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"be3b5b7e-bc73-41a5-addc-821e7ae45563","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"be3b5b7e-bc73-41a5-addc-821e7ae45563","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c094a118-5bed-43a3-b395-64bdb6f7b39f","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c094a118-5bed-43a3-b395-64bdb6f7b39f","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"c094a118-5bed-43a3-b395-64bdb6f7b39f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6660fe5c-ded0-4ae1-87d9-d33e54d32d2d","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6660fe5c-ded0-4ae1-87d9-d33e54d32d2d","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"6660fe5c-ded0-4ae1-87d9-d33e54d32d2d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"733d56fb-343b-4c56-a25e-93c41204b0f1","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"733d56fb-343b-4c56-a25e-93c41204b0f1","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"733d56fb-343b-4c56-a25e-93c41204b0f1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"af7665d4-3838-4325-8988-81a65c5a4932","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"af7665d4-3838-4325-8988-81a65c5a4932","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"af7665d4-3838-4325-8988-81a65c5a4932","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"78f622ec-2032-4981-b6b2-e74bc40c942b","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"78f622ec-2032-4981-b6b2-e74bc40c942b","path":"sprites/robert_dies/robert_dies.yy",},"LayerId":{"name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","name":"78f622ec-2032-4981-b6b2-e74bc40c942b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 34.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 32.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6b4b20c3-3cbb-4260-bef8-babadfa519d8","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1d562afb-12f0-4599-aa0a-1405dbda8336","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e9e29edb-03df-46a0-a4c6-91e5f6fdb800","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d21616f0-a9c9-4c69-88b6-ab52c8460788","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0463ff32-6ae7-4516-a4a0-5ddd43b0815c","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"04c6641a-8097-4bb2-875d-e71b21ef640e","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"28edeefe-fc74-42ba-baa1-c39fccb693e4","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8c6e0d67-0c3e-4f6f-a1b5-c79438e2de75","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2980d444-4be5-4980-9075-85d71e4c69bb","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c9ee29a9-87e5-41a9-ba4f-28901dc89b5c","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d6eae8af-00bb-4339-9b30-decf6f1bebcf","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fd1fa484-cb9e-4e81-8c5d-bd1979c978a9","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b367a69a-e6e4-45b5-ba37-54aba4fbbe28","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9affedec-5232-4802-a426-87f57b166c56","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"834270af-bd60-43db-94ea-25975e507785","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"24b57f77-cb35-4316-bf32-86e442a2cc18","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1f3dfebd-458b-4e53-af37-a413f3346a57","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e6a6eade-7a26-4fa5-9d18-a7bbd42862a9","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"45e8c168-ec53-45c4-b6dc-1d88bbca6556","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"32555a80-010e-4848-baed-5785dcdaa238","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"73727b27-a725-46fa-a0bc-f649b8ad6f17","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"276ddb37-5a63-4a6f-8895-bff87887d870","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3eca1980-a65b-4b35-b595-2ddfa134bcbf","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1714a615-efd1-4d70-a2d1-228ba5a94144","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fa60c905-5db6-4c33-a9af-0143a437f8c7","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c73b9017-3263-4f57-81f9-5b7d53fac206","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a6764602-69cf-4818-a8a6-6847719c1d5f","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"131aa12d-dd47-4e1d-a789-077afa09ea06","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"939e31b8-12af-46a7-aab7-94c508e1a753","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d4d08b62-7d47-44d6-8122-482b5857a698","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9176b100-aba7-4a87-b203-f6ede3b258a8","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"628817da-d7f9-40fd-a554-2ccbda9873d4","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"77dd61c1-d64d-41d2-bd0a-1aaff7bd1a5f","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ef07ca9f-ecc1-41d8-b56c-1fe2c8c459bd","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"da75ff6b-100f-4700-a2ef-1ca39f692de1","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"755afbe7-1c4b-4c24-8f9f-c6e6f6d27f92","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c4b92148-1346-42c7-b58e-33b557800e0c","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8667b21b-12ea-4c0f-a211-9a3bc5d69f5a","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"eae7533e-873f-4af7-9210-9631c80077aa","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bcca11bc-d19f-4bd6-91cb-4fe97315c27c","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"933732b6-ebc3-4360-bdf0-9c10c3db1dc4","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7b864b7b-6bad-41ad-a985-4de8d15554a9","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d8eb7dfe-bce1-46bc-8aea-af64d94b408c","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"65bf327e-3fe1-48e8-8b72-3ff95e6eae6c","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9cccec69-4dcd-48a6-9219-1f9ceaeeaede","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d53c841a-eec1-4ea5-a3b8-576d396c8293","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1fc30333-40c9-46ba-b751-0b5c5f8b7b70","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5c72616d-4bf9-4f80-9335-9b7b396a589c","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"81246338-e2e7-4367-9e8a-84dacb9a1179","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"54df120f-b648-4756-a5eb-1c43cf11c9bb","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6454754e-75e7-42b9-8cd1-b7bfb2e645f6","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cb1a9b28-ea54-4eaa-bfbd-7f569bb77093","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b10c29ba-3f3a-42a8-a122-e4144e15aa04","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"be3b5b7e-bc73-41a5-addc-821e7ae45563","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"755c757c-8365-44d4-b056-ffa080c88760","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c094a118-5bed-43a3-b395-64bdb6f7b39f","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d54f8a1d-ce71-414d-959c-be4098b9a757","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6660fe5c-ded0-4ae1-87d9-d33e54d32d2d","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"982b00cb-0ed1-4539-9e60-69febcbb74ac","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"733d56fb-343b-4c56-a25e-93c41204b0f1","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"969fcbdc-23fe-4ad1-ab09-f1276e8771de","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"af7665d4-3838-4325-8988-81a65c5a4932","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a7f66350-eaf1-446a-8c14-50a7ba61604b","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"78f622ec-2032-4981-b6b2-e74bc40c942b","path":"sprites/robert_dies/robert_dies.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 55,
    "yorigin": 24,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"robert_dies","path":"sprites/robert_dies/robert_dies.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"28a93c26-43bb-4d28-8564-40bdb7b25be5","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Player",
    "path": "folders/Sprites/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "robert_dies",
  "tags": [],
  "resourceType": "GMSprite",
}