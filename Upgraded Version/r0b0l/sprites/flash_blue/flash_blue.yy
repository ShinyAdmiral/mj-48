{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 39,
  "bbox_top": 0,
  "bbox_bottom": 29,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 40,
  "height": 40,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"c92de9f1-d917-41bd-bd38-37d0416d98fb","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c92de9f1-d917-41bd-bd38-37d0416d98fb","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"c92de9f1-d917-41bd-bd38-37d0416d98fb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eb8cb095-ce70-461a-ab4e-127a52c14cc5","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eb8cb095-ce70-461a-ab4e-127a52c14cc5","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"eb8cb095-ce70-461a-ab4e-127a52c14cc5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7ee0cee7-95d8-426c-bf8a-bdcc127279cc","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7ee0cee7-95d8-426c-bf8a-bdcc127279cc","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"7ee0cee7-95d8-426c-bf8a-bdcc127279cc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"629ea6c8-a677-4b8d-887a-977f4f55ddec","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"629ea6c8-a677-4b8d-887a-977f4f55ddec","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"629ea6c8-a677-4b8d-887a-977f4f55ddec","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dd7ecb73-c5be-4ce5-8748-5e3475a43476","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dd7ecb73-c5be-4ce5-8748-5e3475a43476","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"dd7ecb73-c5be-4ce5-8748-5e3475a43476","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c2494700-c136-40e2-ae15-5c5a8d515c4c","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c2494700-c136-40e2-ae15-5c5a8d515c4c","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"c2494700-c136-40e2-ae15-5c5a8d515c4c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a7dcafdd-46bf-4b83-84ae-7c2b37f408a0","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a7dcafdd-46bf-4b83-84ae-7c2b37f408a0","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"a7dcafdd-46bf-4b83-84ae-7c2b37f408a0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"abbd6a6e-41c2-43ad-886b-2f17b68e4636","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"abbd6a6e-41c2-43ad-886b-2f17b68e4636","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"abbd6a6e-41c2-43ad-886b-2f17b68e4636","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"00c7bd3a-afbc-45c3-9e0b-2e398d838f5f","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"00c7bd3a-afbc-45c3-9e0b-2e398d838f5f","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"00c7bd3a-afbc-45c3-9e0b-2e398d838f5f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"48e1a8c6-4539-4fca-af3f-bdc613f78030","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"48e1a8c6-4539-4fca-af3f-bdc613f78030","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"48e1a8c6-4539-4fca-af3f-bdc613f78030","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c6e0dde7-2c29-4be0-810d-0fc5195e77dc","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c6e0dde7-2c29-4be0-810d-0fc5195e77dc","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"c6e0dde7-2c29-4be0-810d-0fc5195e77dc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"77558f85-5630-4fc2-ab9d-ebc74b5ed791","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"77558f85-5630-4fc2-ab9d-ebc74b5ed791","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"77558f85-5630-4fc2-ab9d-ebc74b5ed791","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3e0e6197-d319-49ad-a8ef-92fe4f7af1c3","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3e0e6197-d319-49ad-a8ef-92fe4f7af1c3","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"3e0e6197-d319-49ad-a8ef-92fe4f7af1c3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ff637f4a-3813-4dae-9818-bcd9fac1543e","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ff637f4a-3813-4dae-9818-bcd9fac1543e","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"ff637f4a-3813-4dae-9818-bcd9fac1543e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1e6e7db9-bc6d-4227-b3e7-0fa70f773cd7","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1e6e7db9-bc6d-4227-b3e7-0fa70f773cd7","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"1e6e7db9-bc6d-4227-b3e7-0fa70f773cd7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b980e1eb-a453-4ea0-885f-de360c04bb01","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b980e1eb-a453-4ea0-885f-de360c04bb01","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"b980e1eb-a453-4ea0-885f-de360c04bb01","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"71556a7a-1443-4431-8c4a-c3a8e1240bad","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"71556a7a-1443-4431-8c4a-c3a8e1240bad","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"71556a7a-1443-4431-8c4a-c3a8e1240bad","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d1ea556a-3cc8-44cd-98fc-c30c08cc85e6","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d1ea556a-3cc8-44cd-98fc-c30c08cc85e6","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"d1ea556a-3cc8-44cd-98fc-c30c08cc85e6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d01424eb-832c-4ff0-b9b0-b73b0ded2c09","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d01424eb-832c-4ff0-b9b0-b73b0ded2c09","path":"sprites/flash_blue/flash_blue.yy",},"LayerId":{"name":"13f41a9f-3151-4828-9d85-618970930e23","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","name":"d01424eb-832c-4ff0-b9b0-b73b0ded2c09","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 33.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 19.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4af48176-ed0f-49fb-8fa6-e9fd7e54b605","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c92de9f1-d917-41bd-bd38-37d0416d98fb","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"db9d878c-1a4d-4b98-810b-8ca432f8a9ed","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eb8cb095-ce70-461a-ab4e-127a52c14cc5","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a32c2058-ac64-4ea8-a536-d721928c33c5","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7ee0cee7-95d8-426c-bf8a-bdcc127279cc","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"66298a4d-a961-4ef1-a040-370b9223c14f","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"629ea6c8-a677-4b8d-887a-977f4f55ddec","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e9181a81-5697-44a4-8916-a5aa443a1a37","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dd7ecb73-c5be-4ce5-8748-5e3475a43476","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c3bb73d9-7ad3-4260-9011-917d13828867","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c2494700-c136-40e2-ae15-5c5a8d515c4c","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"aac42676-6752-4d69-853c-4d28b589dd1b","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a7dcafdd-46bf-4b83-84ae-7c2b37f408a0","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"96b5c856-86aa-44d9-9a26-935d841374aa","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"abbd6a6e-41c2-43ad-886b-2f17b68e4636","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c20ee2eb-3e6f-4f9c-babc-bd153136abb3","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"00c7bd3a-afbc-45c3-9e0b-2e398d838f5f","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fa8a6a53-621e-4289-8e14-2c7729f720bc","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"48e1a8c6-4539-4fca-af3f-bdc613f78030","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"30d6d304-becc-41a8-a5df-c18ae25aef47","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c6e0dde7-2c29-4be0-810d-0fc5195e77dc","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b5ee02c8-e3de-472f-a2be-e008d2270402","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"77558f85-5630-4fc2-ab9d-ebc74b5ed791","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4a46b1dc-8c7f-4dbc-a346-4150a0bab6a9","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3e0e6197-d319-49ad-a8ef-92fe4f7af1c3","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"09464a71-d8ff-4d01-bc9b-edee73b3ed94","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ff637f4a-3813-4dae-9818-bcd9fac1543e","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8ac8247a-5ba7-4f54-aafb-b0887dcde3b6","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1e6e7db9-bc6d-4227-b3e7-0fa70f773cd7","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1a0d6df5-1d6e-4f5d-98fa-9260d005bcbf","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b980e1eb-a453-4ea0-885f-de360c04bb01","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"104f3894-f48a-476a-aec5-413772560d34","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"71556a7a-1443-4431-8c4a-c3a8e1240bad","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e5bc1097-878c-402e-863e-a18033f1868a","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d1ea556a-3cc8-44cd-98fc-c30c08cc85e6","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"06dc7cb6-43ba-4c6b-8e28-59688146d426","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d01424eb-832c-4ff0-b9b0-b73b0ded2c09","path":"sprites/flash_blue/flash_blue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 21,
    "yorigin": 20,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"flash_blue","path":"sprites/flash_blue/flash_blue.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"13f41a9f-3151-4828-9d85-618970930e23","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Particle",
    "path": "folders/Sprites/Pieces/Particle.yy",
  },
  "resourceVersion": "1.0",
  "name": "flash_blue",
  "tags": [],
  "resourceType": "GMSprite",
}