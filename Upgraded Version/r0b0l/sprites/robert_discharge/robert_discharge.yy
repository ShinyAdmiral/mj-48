{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 18,
  "bbox_right": 87,
  "bbox_top": 19,
  "bbox_bottom": 87,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 106,
  "height": 106,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"435ca59c-b833-4842-957a-8ab28d6f0bc3","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"435ca59c-b833-4842-957a-8ab28d6f0bc3","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":{"name":"5a6cf84c-4708-4006-b862-7dff8fe0873e","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_discharge","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"435ca59c-b833-4842-957a-8ab28d6f0bc3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"12f5d8b7-858b-4251-a736-21b4b14fc0b8","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"12f5d8b7-858b-4251-a736-21b4b14fc0b8","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":{"name":"5a6cf84c-4708-4006-b862-7dff8fe0873e","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_discharge","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"12f5d8b7-858b-4251-a736-21b4b14fc0b8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b5321339-2149-4776-8404-78198264dbb4","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b5321339-2149-4776-8404-78198264dbb4","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":{"name":"5a6cf84c-4708-4006-b862-7dff8fe0873e","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_discharge","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"b5321339-2149-4776-8404-78198264dbb4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"361d340d-00ad-4dd7-b9a2-63280dc99d02","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"361d340d-00ad-4dd7-b9a2-63280dc99d02","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":{"name":"5a6cf84c-4708-4006-b862-7dff8fe0873e","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_discharge","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"361d340d-00ad-4dd7-b9a2-63280dc99d02","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f73a7972-172b-4bd3-b32a-fb59b375e922","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f73a7972-172b-4bd3-b32a-fb59b375e922","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":{"name":"5a6cf84c-4708-4006-b862-7dff8fe0873e","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_discharge","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"f73a7972-172b-4bd3-b32a-fb59b375e922","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"09511bc4-49c1-4f1c-9aef-3bdb4db403c1","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"09511bc4-49c1-4f1c-9aef-3bdb4db403c1","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":{"name":"5a6cf84c-4708-4006-b862-7dff8fe0873e","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_discharge","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"09511bc4-49c1-4f1c-9aef-3bdb4db403c1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e6c727df-c7b3-46b6-881a-96c260433e14","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e6c727df-c7b3-46b6-881a-96c260433e14","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":{"name":"5a6cf84c-4708-4006-b862-7dff8fe0873e","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_discharge","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"e6c727df-c7b3-46b6-881a-96c260433e14","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e21f151a-25ed-4d8d-9918-b29b6fcdee57","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e21f151a-25ed-4d8d-9918-b29b6fcdee57","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":{"name":"5a6cf84c-4708-4006-b862-7dff8fe0873e","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_discharge","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"e21f151a-25ed-4d8d-9918-b29b6fcdee57","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1697d6ca-216d-4ef4-a066-c6224883453b","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1697d6ca-216d-4ef4-a066-c6224883453b","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":{"name":"5a6cf84c-4708-4006-b862-7dff8fe0873e","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_discharge","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"1697d6ca-216d-4ef4-a066-c6224883453b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ce4bba96-6556-440d-a399-b88f95af294b","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ce4bba96-6556-440d-a399-b88f95af294b","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":{"name":"5a6cf84c-4708-4006-b862-7dff8fe0873e","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_discharge","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"ce4bba96-6556-440d-a399-b88f95af294b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aba62247-5e51-4257-9c9e-f56ee8009922","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aba62247-5e51-4257-9c9e-f56ee8009922","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":{"name":"5a6cf84c-4708-4006-b862-7dff8fe0873e","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_discharge","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"aba62247-5e51-4257-9c9e-f56ee8009922","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dcdc34b8-766a-4b28-9834-001cef18f9f9","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dcdc34b8-766a-4b28-9834-001cef18f9f9","path":"sprites/robert_discharge/robert_discharge.yy",},"LayerId":{"name":"5a6cf84c-4708-4006-b862-7dff8fe0873e","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_discharge","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","name":"dcdc34b8-766a-4b28-9834-001cef18f9f9","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"robert_discharge","path":"sprites/robert_discharge/robert_discharge.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 33.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 12.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"08fc14eb-6d85-4141-9d0c-d9cf7aacc41e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"435ca59c-b833-4842-957a-8ab28d6f0bc3","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"61a83cfe-3d81-4f98-9175-46e348e23006","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"12f5d8b7-858b-4251-a736-21b4b14fc0b8","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c73aed38-e1b8-404f-877b-e8b33fbeee72","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b5321339-2149-4776-8404-78198264dbb4","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d6b84456-d6f7-4a3d-ad0b-3a7fb0cf1e7f","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"361d340d-00ad-4dd7-b9a2-63280dc99d02","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"43482cbc-1ebb-4e02-b83e-372d16bfd3e5","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f73a7972-172b-4bd3-b32a-fb59b375e922","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"56a06344-5731-4e8e-9554-7f351e301f48","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"09511bc4-49c1-4f1c-9aef-3bdb4db403c1","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5b46f21f-17c6-4b04-826a-f84177f5d17b","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e6c727df-c7b3-46b6-881a-96c260433e14","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7644454d-b00e-48ca-bd6b-d4b31f623c30","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e21f151a-25ed-4d8d-9918-b29b6fcdee57","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3a8fb986-9918-488b-95a1-af00f8f64c7a","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1697d6ca-216d-4ef4-a066-c6224883453b","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"42c2e828-aab1-416f-a5f9-a613446f6154","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ce4bba96-6556-440d-a399-b88f95af294b","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"399afbf7-6c86-406c-a961-47db9953547c","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aba62247-5e51-4257-9c9e-f56ee8009922","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"50605a5f-467b-4a1b-b75c-b27ec0583f0e","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dcdc34b8-766a-4b28-9834-001cef18f9f9","path":"sprites/robert_discharge/robert_discharge.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 53,
    "yorigin": 53,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"robert_discharge","path":"sprites/robert_discharge/robert_discharge.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"5a6cf84c-4708-4006-b862-7dff8fe0873e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "robert_discharge",
  "tags": [],
  "resourceType": "GMSprite",
}