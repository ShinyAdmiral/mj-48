{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 83,
  "bbox_top": 0,
  "bbox_bottom": 57,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 84,
  "height": 58,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"61ba2327-656d-4e84-846a-fd2eb3c1a931","path":"sprites/BO_RedJ/BO_RedJ.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"61ba2327-656d-4e84-846a-fd2eb3c1a931","path":"sprites/BO_RedJ/BO_RedJ.yy",},"LayerId":{"name":"77cb35b8-e7cf-425c-85b1-4dfd2f56e8d1","path":"sprites/BO_RedJ/BO_RedJ.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"BO_RedJ","path":"sprites/BO_RedJ/BO_RedJ.yy",},"resourceVersion":"1.0","name":"61ba2327-656d-4e84-846a-fd2eb3c1a931","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"BO_RedJ","path":"sprites/BO_RedJ/BO_RedJ.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"2ca6c0bb-c3e4-447f-81c3-58318b520f65","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"61ba2327-656d-4e84-846a-fd2eb3c1a931","path":"sprites/BO_RedJ/BO_RedJ.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"BO_RedJ","path":"sprites/BO_RedJ/BO_RedJ.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"77cb35b8-e7cf-425c-85b1-4dfd2f56e8d1","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "UI",
    "path": "folders/Sprites/UI.yy",
  },
  "resourceVersion": "1.0",
  "name": "BO_RedJ",
  "tags": [],
  "resourceType": "GMSprite",
}