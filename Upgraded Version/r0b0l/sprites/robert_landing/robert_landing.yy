{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 59,
  "bbox_top": 15,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"5ae09c85-b316-4563-87c6-8ed76206f060","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5ae09c85-b316-4563-87c6-8ed76206f060","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"5ae09c85-b316-4563-87c6-8ed76206f060","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"00659320-27a1-48bb-8c0d-8e144c1c6220","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"00659320-27a1-48bb-8c0d-8e144c1c6220","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"00659320-27a1-48bb-8c0d-8e144c1c6220","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"19818273-cdfd-44e5-ae54-adc3f8688fda","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"19818273-cdfd-44e5-ae54-adc3f8688fda","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"19818273-cdfd-44e5-ae54-adc3f8688fda","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2f243c55-4482-498b-bb32-9c824155b70b","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2f243c55-4482-498b-bb32-9c824155b70b","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"2f243c55-4482-498b-bb32-9c824155b70b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ba0f206e-468c-440c-a7d6-0c661cc0f507","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ba0f206e-468c-440c-a7d6-0c661cc0f507","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"ba0f206e-468c-440c-a7d6-0c661cc0f507","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eb39838d-6265-4e35-b4fe-0d92c4331f56","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eb39838d-6265-4e35-b4fe-0d92c4331f56","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"eb39838d-6265-4e35-b4fe-0d92c4331f56","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bce26b9f-ab4a-41bf-9492-614aee88b2f1","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bce26b9f-ab4a-41bf-9492-614aee88b2f1","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"bce26b9f-ab4a-41bf-9492-614aee88b2f1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"811141c4-c5dc-4921-9fdd-a0e66516ecf8","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"811141c4-c5dc-4921-9fdd-a0e66516ecf8","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"811141c4-c5dc-4921-9fdd-a0e66516ecf8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7827c400-a618-403d-9f01-abab27f8443b","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7827c400-a618-403d-9f01-abab27f8443b","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"7827c400-a618-403d-9f01-abab27f8443b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dcb1d3eb-3e5c-4a77-902c-5efb5bb4d4d9","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dcb1d3eb-3e5c-4a77-902c-5efb5bb4d4d9","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"dcb1d3eb-3e5c-4a77-902c-5efb5bb4d4d9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ca115764-1ab8-42db-8d42-f9373d91f985","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ca115764-1ab8-42db-8d42-f9373d91f985","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"ca115764-1ab8-42db-8d42-f9373d91f985","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"27fc05fa-2c72-4821-8b06-ed7fb3aa01bf","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"27fc05fa-2c72-4821-8b06-ed7fb3aa01bf","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"27fc05fa-2c72-4821-8b06-ed7fb3aa01bf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4621a10b-c740-4a0f-afbd-bb292f217ef1","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4621a10b-c740-4a0f-afbd-bb292f217ef1","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"4621a10b-c740-4a0f-afbd-bb292f217ef1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5a6aa65c-61ed-4be8-811f-8eb2de3a717c","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5a6aa65c-61ed-4be8-811f-8eb2de3a717c","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"5a6aa65c-61ed-4be8-811f-8eb2de3a717c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a9ede9c6-a076-4d50-9155-fb13861fb6e4","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a9ede9c6-a076-4d50-9155-fb13861fb6e4","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"a9ede9c6-a076-4d50-9155-fb13861fb6e4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1ed9fcdb-68e1-421a-bf8a-c872ef6808f5","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1ed9fcdb-68e1-421a-bf8a-c872ef6808f5","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"1ed9fcdb-68e1-421a-bf8a-c872ef6808f5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"119a2ddc-1bd5-4563-894c-cb58c7002a31","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"119a2ddc-1bd5-4563-894c-cb58c7002a31","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"119a2ddc-1bd5-4563-894c-cb58c7002a31","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9397e159-85ea-4cb5-a84a-eaf1f7ce976d","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9397e159-85ea-4cb5-a84a-eaf1f7ce976d","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"9397e159-85ea-4cb5-a84a-eaf1f7ce976d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7c09517b-d620-4710-a0ae-41b53696c683","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7c09517b-d620-4710-a0ae-41b53696c683","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"7c09517b-d620-4710-a0ae-41b53696c683","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b4cab69e-015d-4a85-aef2-468846efc443","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b4cab69e-015d-4a85-aef2-468846efc443","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"b4cab69e-015d-4a85-aef2-468846efc443","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e6b6bd0e-b4e4-4f23-9350-7184ed4f0046","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e6b6bd0e-b4e4-4f23-9350-7184ed4f0046","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"e6b6bd0e-b4e4-4f23-9350-7184ed4f0046","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ccd56121-e3de-4980-8a39-3aa5f62b972b","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ccd56121-e3de-4980-8a39-3aa5f62b972b","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"ccd56121-e3de-4980-8a39-3aa5f62b972b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0955d959-5c55-45f2-a81b-24a45e14f265","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0955d959-5c55-45f2-a81b-24a45e14f265","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"0955d959-5c55-45f2-a81b-24a45e14f265","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f2e4f542-7fc9-4a3a-aafe-50da83e2bbbf","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f2e4f542-7fc9-4a3a-aafe-50da83e2bbbf","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"f2e4f542-7fc9-4a3a-aafe-50da83e2bbbf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6278cdc3-92db-48f8-8904-4d4e690fd2f7","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6278cdc3-92db-48f8-8904-4d4e690fd2f7","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"6278cdc3-92db-48f8-8904-4d4e690fd2f7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6c97e0da-9884-4ece-b643-99a82078a0ae","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6c97e0da-9884-4ece-b643-99a82078a0ae","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"6c97e0da-9884-4ece-b643-99a82078a0ae","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a5c74b9b-951b-4943-b8a5-970a706b63a2","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a5c74b9b-951b-4943-b8a5-970a706b63a2","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"a5c74b9b-951b-4943-b8a5-970a706b63a2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"722145c2-5e52-43da-bc70-fe8bd3220d11","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"722145c2-5e52-43da-bc70-fe8bd3220d11","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"722145c2-5e52-43da-bc70-fe8bd3220d11","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6b3900df-f8f3-41e5-8833-d2ac8d186647","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6b3900df-f8f3-41e5-8833-d2ac8d186647","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"6b3900df-f8f3-41e5-8833-d2ac8d186647","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f298b672-0ff9-4551-8c9b-6d457bdabac8","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f298b672-0ff9-4551-8c9b-6d457bdabac8","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"f298b672-0ff9-4551-8c9b-6d457bdabac8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c93bcab3-506d-4a3c-a672-7cf0d3932253","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c93bcab3-506d-4a3c-a672-7cf0d3932253","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"c93bcab3-506d-4a3c-a672-7cf0d3932253","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"11100aca-25f7-4efd-b66f-194c04a992ca","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"11100aca-25f7-4efd-b66f-194c04a992ca","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"11100aca-25f7-4efd-b66f-194c04a992ca","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"390af2ed-0d94-4446-a36c-3043b318ad48","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"390af2ed-0d94-4446-a36c-3043b318ad48","path":"sprites/robert_landing/robert_landing.yy",},"LayerId":{"name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","name":"390af2ed-0d94-4446-a36c-3043b318ad48","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 33.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 33.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5e273eaf-4cf8-46da-baa9-f8174b312469","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5ae09c85-b316-4563-87c6-8ed76206f060","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"785ca7ea-6893-4d1b-999a-ce1170318625","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"00659320-27a1-48bb-8c0d-8e144c1c6220","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dee1ca96-6fc1-492f-bfbd-8eb43c4a9bd1","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"19818273-cdfd-44e5-ae54-adc3f8688fda","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bc00424f-49b0-447d-8d81-3fb88c9bc32c","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2f243c55-4482-498b-bb32-9c824155b70b","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8763e5c2-c60a-4017-acba-6cef8d6a80ba","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ba0f206e-468c-440c-a7d6-0c661cc0f507","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"880165fb-1258-4829-a0f8-a83103b1a12e","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eb39838d-6265-4e35-b4fe-0d92c4331f56","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dbba8040-21bd-4b14-b80d-fa88f6f3f8d1","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bce26b9f-ab4a-41bf-9492-614aee88b2f1","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8f69efef-57ee-4f31-b3ed-4d2de1e5cf9e","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"811141c4-c5dc-4921-9fdd-a0e66516ecf8","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"34c83bd0-64a6-420e-98a0-97e37448bfa9","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7827c400-a618-403d-9f01-abab27f8443b","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5fe4cd3f-cf3e-4509-973b-61f33e228eb9","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dcb1d3eb-3e5c-4a77-902c-5efb5bb4d4d9","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"04c12ceb-afab-4430-9c90-bbd1942d8812","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ca115764-1ab8-42db-8d42-f9373d91f985","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"24193f43-4f9f-4e79-859f-71f14ace55e6","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"27fc05fa-2c72-4821-8b06-ed7fb3aa01bf","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"641a8938-b4d3-4bd1-9bc8-fd7f0e11ed52","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4621a10b-c740-4a0f-afbd-bb292f217ef1","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c572c962-48a7-478d-9006-3193be7f06f2","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5a6aa65c-61ed-4be8-811f-8eb2de3a717c","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"36b72eac-3972-44e1-adf4-a26f13279b1e","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a9ede9c6-a076-4d50-9155-fb13861fb6e4","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"33d35a9b-89a8-43c8-ace1-af045a8d090b","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1ed9fcdb-68e1-421a-bf8a-c872ef6808f5","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"df1759fb-e235-4750-ae7d-74e8a84239b3","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"119a2ddc-1bd5-4563-894c-cb58c7002a31","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5760fd0e-d6a0-4611-8f0e-fed90f7ff2c1","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9397e159-85ea-4cb5-a84a-eaf1f7ce976d","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8df33d2c-0f73-4eba-8666-07588a2d7bca","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7c09517b-d620-4710-a0ae-41b53696c683","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8174e0ec-80f2-4f0e-b972-bd453f924da8","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b4cab69e-015d-4a85-aef2-468846efc443","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"44525b07-1e2d-4a57-9a8d-1a7740b3fd61","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e6b6bd0e-b4e4-4f23-9350-7184ed4f0046","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8f328962-24cc-4033-add4-2ed515a4e377","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ccd56121-e3de-4980-8a39-3aa5f62b972b","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ab5d9d6c-0e0d-44e6-b12f-7cca8a7dceaa","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0955d959-5c55-45f2-a81b-24a45e14f265","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1d33f159-a7fd-4f20-a857-bbb1c35b846e","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f2e4f542-7fc9-4a3a-aafe-50da83e2bbbf","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d5455216-e2d7-4dd8-9f58-762a9c33ee9b","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6278cdc3-92db-48f8-8904-4d4e690fd2f7","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"14151c6f-8aa9-40ae-aa13-69d543b0db84","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6c97e0da-9884-4ece-b643-99a82078a0ae","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"439694ac-b9c8-42f3-9d29-ff5d511b410d","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a5c74b9b-951b-4943-b8a5-970a706b63a2","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6c27860c-c335-4e50-a4b9-1bc96958fa03","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"722145c2-5e52-43da-bc70-fe8bd3220d11","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3d8fa371-d829-4509-adb1-1968bd052b52","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6b3900df-f8f3-41e5-8833-d2ac8d186647","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9a86209b-2b55-4cae-aa7d-90f28322fe13","Key":29.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f298b672-0ff9-4551-8c9b-6d457bdabac8","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9ed2e6b4-5f0d-45d7-96c6-637db88a69a4","Key":30.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c93bcab3-506d-4a3c-a672-7cf0d3932253","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"80eddb34-0dac-42a8-8609-ba659e439438","Key":31.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"11100aca-25f7-4efd-b66f-194c04a992ca","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"09af7978-c7d6-4186-8dad-0d4e18254a11","Key":32.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"390af2ed-0d94-4446-a36c-3043b318ad48","path":"sprites/robert_landing/robert_landing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"robert_landing","path":"sprites/robert_landing/robert_landing.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"abe60db6-0a74-4bdb-a466-c57e40cb25c2","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "robert_landing",
  "tags": [],
  "resourceType": "GMSprite",
}