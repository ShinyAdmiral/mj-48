{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 39,
  "bbox_top": 8,
  "bbox_bottom": 29,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 40,
  "height": 40,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"2f6bc94e-26b4-4d96-a3c6-fbe65c000c8c","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2f6bc94e-26b4-4d96-a3c6-fbe65c000c8c","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"2f6bc94e-26b4-4d96-a3c6-fbe65c000c8c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bdeefb28-bc0e-40b8-8366-f43a908bb8c4","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bdeefb28-bc0e-40b8-8366-f43a908bb8c4","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"bdeefb28-bc0e-40b8-8366-f43a908bb8c4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2baf8121-c6f0-4692-a56c-782acb4691ed","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2baf8121-c6f0-4692-a56c-782acb4691ed","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"2baf8121-c6f0-4692-a56c-782acb4691ed","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2f7da1d8-8f1f-48cb-8574-6a92b35ba913","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2f7da1d8-8f1f-48cb-8574-6a92b35ba913","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"2f7da1d8-8f1f-48cb-8574-6a92b35ba913","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"41d34918-82fb-4bd0-b7c7-c70d6ab7c419","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"41d34918-82fb-4bd0-b7c7-c70d6ab7c419","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"41d34918-82fb-4bd0-b7c7-c70d6ab7c419","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5c3fcea3-2f03-480a-b754-c70d840b983d","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5c3fcea3-2f03-480a-b754-c70d840b983d","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"5c3fcea3-2f03-480a-b754-c70d840b983d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"006664c2-b1aa-42c7-8ff6-d9b628f89359","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"006664c2-b1aa-42c7-8ff6-d9b628f89359","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"006664c2-b1aa-42c7-8ff6-d9b628f89359","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e6e54dd0-7bdd-40a1-a6d4-8e0786f338bd","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e6e54dd0-7bdd-40a1-a6d4-8e0786f338bd","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"e6e54dd0-7bdd-40a1-a6d4-8e0786f338bd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"94b52b89-61e2-47ce-935d-ac3d3991a42d","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"94b52b89-61e2-47ce-935d-ac3d3991a42d","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"94b52b89-61e2-47ce-935d-ac3d3991a42d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"195a1f22-0660-4929-9de3-f820475a5a97","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"195a1f22-0660-4929-9de3-f820475a5a97","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"195a1f22-0660-4929-9de3-f820475a5a97","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"18726e2d-8281-449b-ab7a-bc2aebd7f8d5","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"18726e2d-8281-449b-ab7a-bc2aebd7f8d5","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"18726e2d-8281-449b-ab7a-bc2aebd7f8d5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"49f5f084-c63e-40c2-85ed-29b05e456804","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"49f5f084-c63e-40c2-85ed-29b05e456804","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"49f5f084-c63e-40c2-85ed-29b05e456804","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3fd722ad-af00-40e4-a746-dbb8800d0cd4","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3fd722ad-af00-40e4-a746-dbb8800d0cd4","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"3fd722ad-af00-40e4-a746-dbb8800d0cd4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"db006bf9-4845-4bf0-9834-9732d86d273e","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"db006bf9-4845-4bf0-9834-9732d86d273e","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"db006bf9-4845-4bf0-9834-9732d86d273e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"83d68fdf-7358-4f58-b1fa-c011d43304a8","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"83d68fdf-7358-4f58-b1fa-c011d43304a8","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"83d68fdf-7358-4f58-b1fa-c011d43304a8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d499e1ae-4f32-4df6-a03a-9489312df375","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d499e1ae-4f32-4df6-a03a-9489312df375","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"d499e1ae-4f32-4df6-a03a-9489312df375","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9325ea55-59f1-4ebd-a3a9-048c64feb5cf","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9325ea55-59f1-4ebd-a3a9-048c64feb5cf","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"9325ea55-59f1-4ebd-a3a9-048c64feb5cf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fd95db5d-f3ab-4ba2-ada9-3bccc247a4b0","path":"sprites/flash_green/flash_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fd95db5d-f3ab-4ba2-ada9-3bccc247a4b0","path":"sprites/flash_green/flash_green.yy",},"LayerId":{"name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","name":"fd95db5d-f3ab-4ba2-ada9-3bccc247a4b0","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 33.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 18.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"c8eba68f-5877-49a1-adb7-9e7bc3c48ee7","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2f6bc94e-26b4-4d96-a3c6-fbe65c000c8c","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6e4df8ad-4114-45a5-96c8-aa38ca599eed","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bdeefb28-bc0e-40b8-8366-f43a908bb8c4","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"92233cc6-e79b-45a2-afe0-749a97a930a7","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2baf8121-c6f0-4692-a56c-782acb4691ed","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"75678c4c-e417-4da3-aaf5-a7cb674b8f47","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2f7da1d8-8f1f-48cb-8574-6a92b35ba913","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5400d2a3-5684-4e51-9610-9937271d399f","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"41d34918-82fb-4bd0-b7c7-c70d6ab7c419","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"79983a85-779c-4e14-9298-a2aeadcc332a","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5c3fcea3-2f03-480a-b754-c70d840b983d","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cecfb30c-c848-4cec-b710-7bddf6feac00","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"006664c2-b1aa-42c7-8ff6-d9b628f89359","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fb91ad81-1344-49ec-b3df-80833baa658e","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e6e54dd0-7bdd-40a1-a6d4-8e0786f338bd","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9a3fdfd9-33ba-4538-8175-10eeb478bbc0","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"94b52b89-61e2-47ce-935d-ac3d3991a42d","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4e33dd11-de0b-4cd1-801f-bcfab5de2ab4","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"195a1f22-0660-4929-9de3-f820475a5a97","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6114ec16-0bd7-4abf-afb3-a0ebcd2f8037","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"18726e2d-8281-449b-ab7a-bc2aebd7f8d5","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"743fa244-faaa-4c2d-8613-976cf6c9d19a","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"49f5f084-c63e-40c2-85ed-29b05e456804","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5fbf162c-ae37-4ff6-875d-7ccbee079e50","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3fd722ad-af00-40e4-a746-dbb8800d0cd4","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5d9461c8-280b-4647-8d7f-0069fb696738","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"db006bf9-4845-4bf0-9834-9732d86d273e","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"56ff6b61-c639-4519-84e4-ebbcb7847aa5","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"83d68fdf-7358-4f58-b1fa-c011d43304a8","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"46cf0f18-6019-46b4-98b6-f55685719168","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d499e1ae-4f32-4df6-a03a-9489312df375","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"db0805f8-0e30-454f-b0f0-fd2fad587498","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9325ea55-59f1-4ebd-a3a9-048c64feb5cf","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"423e37f3-28eb-4215-a539-4f4394d2dd89","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fd95db5d-f3ab-4ba2-ada9-3bccc247a4b0","path":"sprites/flash_green/flash_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 21,
    "yorigin": 20,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"flash_green","path":"sprites/flash_green/flash_green.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"636004d4-c2e8-4f56-992c-1e39a72e7de3","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Particle",
    "path": "folders/Sprites/Pieces/Particle.yy",
  },
  "resourceVersion": "1.0",
  "name": "flash_green",
  "tags": [],
  "resourceType": "GMSprite",
}