/// @description Insert description here
// You can write your code in this editor
a = instance_create_layer(x,y,"Blocks", obj_Block);
b = instance_create_layer(x-32,y,"Blocks", obj_Block);
c = instance_create_layer(x+32,y,"Blocks", obj_Block);
d = instance_create_layer(x-64,y,"Blocks", obj_Block);

a.sprite_index = middle_block_horizontal_brown;
b.sprite_index = middle_block_horizontal_brown;
c.sprite_index = end_block_right_brown;
d.sprite_index = end_block_left_brown;

rotation = 0; 

locked = false; 