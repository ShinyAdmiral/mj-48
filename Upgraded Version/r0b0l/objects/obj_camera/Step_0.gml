if (mag)
{
	if (!set)
	{
		reset_x = x;
		reset_y = y;
		set = true;
	}
	ran_x = random_range(-shake,shake);
	ran_y = random_range(-shake,shake);
	
	x = x + ran_x;
	y = y + ran_y;
}
else
{
	x = room_width/2
	y = clamp(y,0, room_height - 768)
	if (obj_player.y > y + 128)
	{
		y = lerp(y,obj_player.y - 128, .03)
	}
	if (obj_player.y < y && obj_player.ver_speed < obj_player.maxGrav)
	{
		y = lerp(y,obj_player.y - 128, .01)
	}
}