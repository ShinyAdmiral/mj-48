/// @description Die on contact with hurt
if (!cool && !charge)
{
	hp--;
	cool = true;
	flash = 1;
	alarm[0] = 30;
	ran = irandom_range(0,2);
	
	if (hp > 0)
	{
		
		instance_create_layer(x,y,"Particles", obj_damagevfx);
		
		switch (ran){
		case 0:
			audio_sound_pitch(Hurt1, random_range(.8,1.2));
			audio_play_sound(Hurt1,10,false);
			break
		
		case 1:
			audio_sound_pitch(scanner_pain2, random_range(.8,1.2));
			audio_play_sound(scanner_pain2,10,false);
			break;
		
		default:
			audio_sound_pitch(ROBOTIC_Transformation_Insect_02_Clicks_Digital_stereo, random_range(.8,1.2));
			audio_play_sound(ROBOTIC_Transformation_Insect_02_Clicks_Digital_stereo,10,false);
		}
	}
	screen_shake(10,7);
}
