if(obj_difficulty.liquid_difficulty < 1.5 * threshold){
	obj_difficulty.liquid_difficulty = threshold * 1.5;
	obj_difficulty.difficulty_falling = 1 - threshold;
}
if(obj_liquid.y > obj_camera.y + 384)
{
	obj_liquid.y = obj_camera.y + 384;
}

if (enemy)
{	
	instance_create_layer(x,y,"Instances",obj_enemySpawner);
}
instance_destroy(self);