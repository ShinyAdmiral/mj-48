/// @description Insert description here
// You can write your code in this editor
if (!locked)
{
	if(keyboard_check_pressed(vk_left) && obj_Block.x > 144)
	{
		if (!collision_rectangle(a.x-16, a.y-16, a.x-47, a.y+15, obj_BlockStatic, true, true) &&
		!collision_rectangle(b.x-16, b.y-16, b.x-47, b.y+15, obj_BlockStatic, true, true) &&
		!collision_rectangle(c.x-16, c.y-16, c.x-47, c.y+15, obj_BlockStatic, true, true) &&
		!collision_rectangle(d.x-16, d.y-16, d.x-47, d.y+15, obj_BlockStatic, true, true))
		{
			
			a.x -= 32;
			b.x -= 32;
			c.x -= 32;
			d.x -= 32;
			
			with (a){
				if (place_meeting(x,y+1, obj_BlockStatic)){
					obj_PieceOrganizer.locked = true;
					locked = true
				}
				if collision_rectangle(x-16,y+16, x+16, y-16,obj_player,true,true)
				obj_player.x = x-16 - 10;
			}
			with (b){
				if (place_meeting(x,y+1, obj_BlockStatic)){
					obj_PieceOrganizer.locked = true;
					locked = true
				}
				if collision_rectangle(x-16,y+16, x+16, y-16,obj_player,true,true)
				obj_player.x = x-16 - 10;
			}
			with (c){
				if (place_meeting(x,y+1, obj_BlockStatic)){
					obj_PieceOrganizer.locked = true;
					locked = true
				}
				if collision_rectangle(x-16,y+16, x+16, y-16,obj_player,true,true)
				obj_player.x = x-16 - 10;
			}
			with (d){
				if (place_meeting(x,y+1, obj_BlockStatic)){
					obj_PieceOrganizer.locked = true;
					locked = true
				}
				if collision_rectangle(x-16,y+16, x+16, y-16,obj_player,true,true)
				obj_player.x = x-16 - 10;
			}
		}
	}
	
	if(keyboard_check_pressed(vk_right) && obj_Block.x < 416)
	{

		if (!collision_rectangle(a.x+15, a.y-15, a.x+47, a.y+15, obj_BlockStatic, true, true) &&
		!collision_rectangle(b.x+15, b.y-15, b.x+47, b.y+15, obj_BlockStatic, true, true) &&
		!collision_rectangle(c.x+15, c.y-15, c.x+47, c.y+15, obj_BlockStatic, true, true) &&
		!collision_rectangle(d.x+15, d.y-15, d.x+47, d.y+15, obj_BlockStatic, true, true))
		{
			a.x += 32;
			b.x += 32;
			c.x += 32;
			d.x += 32;
			
			with (a){
				if (place_meeting(x,y+1, obj_BlockStatic)){
					obj_PieceOrganizer.locked = true;
					locked = true
				}
				if collision_rectangle(x-16,y+16, x+16, y-16,obj_player,true,true)
				obj_player.x = x+16 + 10;
			}
			with (b){
				if (place_meeting(x,y+1, obj_BlockStatic)){
					obj_PieceOrganizer.locked = true;
					locked = true
				}
				if collision_rectangle(x-16,y+16, x+16, y-16,obj_player,true,true)
				obj_player.x = x+16 + 10;
			}
			with (c){
				if (place_meeting(x,y+1, obj_BlockStatic)){
					obj_PieceOrganizer.locked = true;
					locked = true
				}
				if collision_rectangle(x-16,y+16, x+16, y-16,obj_player,true,true)
				obj_player.x = x+16 + 10;
			}
			with (d){
				if (place_meeting(x,y+1, obj_BlockStatic)){
					obj_PieceOrganizer.locked = true;
					locked = true
				}
				if collision_rectangle(x-16,y+16, x+16, y-16,obj_player,true,true)
				obj_player.x = x+16 + 10;
			}
		}
	}
	
	if (keyboard_check_pressed(vk_up))
	{
		if (rotation > 1)
			rotation = 0;

		if (rotation == 0)
		{
			if (!collision_rectangle(a.x-15, a.y-15, a.x+47, a.y+15, obj_player, true, true) &&
			!collision_rectangle(a.x-15, a.y+15, a.x+15, a.y+47, obj_player, true, true) &&
			!collision_rectangle(a.x+15, a.y+15, a.x+47, a.y+47, obj_player, true, true))
			{
				if (!collision_rectangle(a.x-15, a.y-15, a.x+47, a.y+15, obj_BlockStatic, true, true) &&
				!collision_rectangle(a.x-15, a.y+15, a.x+15, a.y+47, obj_BlockStatic, true, true) &&
				!collision_rectangle(a.x+15, a.y+15, a.x+47, a.y+47, obj_BlockStatic, true, true))
				{
					check();
					b.x = a.x - 32;
					b.y = a.y;
		
					c.x = a.x;
					c.y = a.y + 32;
		
					d.x = a.x+32;
					d.y = a.y+32;
					
					check();
					
					a.sprite_index = corner_top_right_yellow;
					b.sprite_index = end_block_left_yellow;
					c.sprite_index = corner_bottom_left_yellow;
					d.sprite_index = end_block_right_yellow;
					
					rotation++;
				}
			}
		}
		else
		{
			if (!collision_rectangle(a.x-15, a.y-15, a.x+15, a.y-47, obj_player, true, true) &&
			!collision_rectangle(a.x-15, a.y-15, a.x-47, a.y+15, obj_player, true, true) &&
			!collision_rectangle(a.x-15, a.y+15, a.x-47, a.y+47, obj_player, true, true))
			{
				if (!collision_rectangle(a.x-15, a.y-15, a.x+15, a.y-47, obj_BlockStatic, true, true) &&
				!collision_rectangle(a.x-15, a.y-15, a.x-47, a.y+15, obj_BlockStatic, true, true) &&
				!collision_rectangle(a.x-15, a.y+15, a.x-47, a.y+47, obj_BlockStatic, true, true))
				{
					check();
					b.x = a.x;
					b.y = a.y - 32;
		
					c.x = a.x - 32;
					c.y = a.y;
		
					d.x = a.x-32;
					d.y = a.y+32;
					
					check();
					
					a.sprite_index = corner_bottom_right_yellow;
					b.sprite_index = end_block_up_yellow;
					c.sprite_index = corner_top_left_yellow;
					d.sprite_index = end_block_down_yellow;
			
					rotation++;
				}
			}
		}
	}
}
else
{
	var i = instance_create_layer(a.x,a.y,"Blocks", obj_BlockStatic);
	var j = instance_create_layer(b.x,b.y,"Blocks", obj_BlockStatic);
	var k = instance_create_layer(c.x,c.y,"Blocks", obj_BlockStatic);
	var l = instance_create_layer(d.x,d.y,"Blocks", obj_BlockStatic);
	
	i.sprite_index = a.sprite_index;
	j.sprite_index = b.sprite_index;
	k.sprite_index = c.sprite_index;
	l.sprite_index = d.sprite_index;
	
	instance_create_layer(a.x,a.y,"Particles", obj_lockedSL);
	instance_create_layer(b.x,b.y,"Particles", obj_lockedSL);
	instance_create_layer(c.x,c.y,"Particles", obj_lockedSL);
	instance_create_layer(d.x,d.y,"Particles", obj_lockedSL);
	
	screen_shake(5,6);
	
	audio_sound_pitch(EXPLOSION_Short_Impact_Explosion_stereo, random_range(.8,1.2));
	audio_play_sound(EXPLOSION_Short_Impact_Explosion_stereo,10,false);
	
	instance_destroy(a);
	instance_destroy(b);
	instance_destroy(c);
	instance_destroy(d);
	instance_destroy(self);
}

if(keyboard_check_pressed(vk_left) && x > 144)
{
		x -= 32;
}
if(keyboard_check_pressed(vk_right) && x < 416)
{
		x += 32;
}