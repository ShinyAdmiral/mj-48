/// @description Insert description here
// You can write your code in this editor
a = instance_create_layer(x,y,"Blocks", obj_Block);
b = instance_create_layer(x,y-32,"Blocks", obj_Block);
c = instance_create_layer(x-32,y,"Blocks", obj_Block);
d = instance_create_layer(x-32,y-32,"Blocks", obj_Block);

a.sprite_index = corner_bottom_right_evergreen;
b.sprite_index = corner_top_right_evergreen;
c.sprite_index = corner_bottom_left_evergreen;
d.sprite_index = corner_top_left_evergreen;

rotation = 0; 

locked = false;