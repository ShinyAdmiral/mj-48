if (!spawn)
{
	if (!instance_exists(obj_enemy)){
		alarm[0] = 10 * obj_difficulty.difficulty_falling;
		spawn = true;
	}
}

if (spawn && !set)
{
	if (!instance_exists(obj_enemy)){
		alarm[1] = 20 * obj_difficulty.difficulty_falling;
		set = true;
	}
}