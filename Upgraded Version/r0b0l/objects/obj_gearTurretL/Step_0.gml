if (!fire)
{
	if (y >= obj_player.y + ran)
	{
		alarm[0] = freeze;
		vspeed = 0;
		image_speed = 0;
		
		audio_sound_pitch(Settle11, random_range(.8,1.2));
		audio_play_sound(Settle11,10,false);
		
		fire = true;
	}
}

if(fire)
{
	if (vspeed<0)
	{
		if(y <= obj_camera.y - 512)
		{
			instance_destroy(self);
		}
	}
}