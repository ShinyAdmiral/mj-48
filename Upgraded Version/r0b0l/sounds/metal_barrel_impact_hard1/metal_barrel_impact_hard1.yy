{
  "compression": 0,
  "volume": 0.33,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "metal_barrel_impact_hard1",
  "duration": 1.21534,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "metal_barrel_impact_hard1",
  "tags": [],
  "resourceType": "GMSound",
}