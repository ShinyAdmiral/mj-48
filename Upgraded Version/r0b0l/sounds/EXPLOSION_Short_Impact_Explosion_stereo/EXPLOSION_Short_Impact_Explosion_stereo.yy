{
  "compression": 0,
  "volume": 0.34,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "EXPLOSION_Short_Impact_Explosion_stereo",
  "duration": 0.526088,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "EXPLOSION_Short_Impact_Explosion_stereo",
  "tags": [],
  "resourceType": "GMSound",
}