{
  "compression": 0,
  "volume": 0.21,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "ROBOTIC_Transformation_Insect_02_Clicks_Digital_stereo",
  "duration": 1.406633,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "ROBOTIC_Transformation_Insect_02_Clicks_Digital_stereo",
  "tags": [],
  "resourceType": "GMSound",
}