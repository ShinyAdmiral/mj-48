{
    "id": "7e99ba51-acaf-4d77-8885-363b5bb6e37b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_up_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b44645c0-f80c-4e61-a88e-b5b927ce473a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e99ba51-acaf-4d77-8885-363b5bb6e37b",
            "compositeImage": {
                "id": "dad2bb97-1213-4b8b-b707-799617dc918f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b44645c0-f80c-4e61-a88e-b5b927ce473a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7913cd42-f72a-441b-b59d-d1fa15dfce3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b44645c0-f80c-4e61-a88e-b5b927ce473a",
                    "LayerId": "96e4eaa8-e499-430d-ba51-522d96e1b95f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "96e4eaa8-e499-430d-ba51-522d96e1b95f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e99ba51-acaf-4d77-8885-363b5bb6e37b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}