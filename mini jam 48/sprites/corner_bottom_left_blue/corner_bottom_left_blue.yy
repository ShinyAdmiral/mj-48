{
    "id": "48dbb090-3fbe-4bde-bdc8-5ac0a364672a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_bottom_left_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88d6c684-3865-4c0b-8e8f-ec52d2e3e684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48dbb090-3fbe-4bde-bdc8-5ac0a364672a",
            "compositeImage": {
                "id": "3b0a6a0a-5f5a-496e-badb-34fbf33a2878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88d6c684-3865-4c0b-8e8f-ec52d2e3e684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "997da4b7-9077-4351-b4e3-1d54165ca13c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88d6c684-3865-4c0b-8e8f-ec52d2e3e684",
                    "LayerId": "b9953b5a-d878-4a58-86ad-4921ca29e811"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b9953b5a-d878-4a58-86ad-4921ca29e811",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48dbb090-3fbe-4bde-bdc8-5ac0a364672a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}