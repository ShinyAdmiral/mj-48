{
    "id": "50797436-285d-4e7d-aa2b-dadd5a24787c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_right_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c0ab5b5-eba3-4aca-b6e4-58bd82694f71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50797436-285d-4e7d-aa2b-dadd5a24787c",
            "compositeImage": {
                "id": "87f88c4c-e48a-4209-87d3-7a3fb63e92ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c0ab5b5-eba3-4aca-b6e4-58bd82694f71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab6199d8-3f09-4238-b078-96222e77c94d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c0ab5b5-eba3-4aca-b6e4-58bd82694f71",
                    "LayerId": "7c83fd29-eaa0-49d0-9b94-ec6505826d2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7c83fd29-eaa0-49d0-9b94-ec6505826d2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50797436-285d-4e7d-aa2b-dadd5a24787c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}