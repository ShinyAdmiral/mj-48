{
    "id": "e032301e-6e3c-4f3e-aa15-52f8db8b3eba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_bottom_left_evergreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b60714b7-8365-4fba-bf36-8887d4beaac4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e032301e-6e3c-4f3e-aa15-52f8db8b3eba",
            "compositeImage": {
                "id": "2556d1ce-780f-4bda-b3c2-7d85e8946140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b60714b7-8365-4fba-bf36-8887d4beaac4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b75ad62-4b30-4385-af89-6f2534eda150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b60714b7-8365-4fba-bf36-8887d4beaac4",
                    "LayerId": "44bddb4f-725f-42a1-a6dd-d8e996089ec7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "44bddb4f-725f-42a1-a6dd-d8e996089ec7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e032301e-6e3c-4f3e-aa15-52f8db8b3eba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}