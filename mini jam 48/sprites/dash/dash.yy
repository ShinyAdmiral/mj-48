{
    "id": "df88f139-061d-45b0-a34d-eb50ac105e6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 9,
    "bbox_right": 30,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c955a30-b620-42db-b449-7b2d2ff0c150",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df88f139-061d-45b0-a34d-eb50ac105e6f",
            "compositeImage": {
                "id": "2a41800f-fb53-46ff-95ad-d36086ca1c6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c955a30-b620-42db-b449-7b2d2ff0c150",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7271523-58df-4c31-99fb-1fa6230bbce2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c955a30-b620-42db-b449-7b2d2ff0c150",
                    "LayerId": "dc3c3d81-2d00-4bee-8611-213e0f07ca3d"
                }
            ]
        },
        {
            "id": "2fcbd62b-3aed-4c98-a5d4-8c3f060422b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df88f139-061d-45b0-a34d-eb50ac105e6f",
            "compositeImage": {
                "id": "e5f73fac-8f24-44d6-8f5d-c4fe4dfc857d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fcbd62b-3aed-4c98-a5d4-8c3f060422b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "053a24bf-5ea8-49fe-86ac-2ab9175ebb0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fcbd62b-3aed-4c98-a5d4-8c3f060422b2",
                    "LayerId": "dc3c3d81-2d00-4bee-8611-213e0f07ca3d"
                }
            ]
        },
        {
            "id": "d9c828ce-5c05-4164-80fc-98bd42f9661d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df88f139-061d-45b0-a34d-eb50ac105e6f",
            "compositeImage": {
                "id": "96d15beb-7ea3-400d-b226-746b4c517e7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9c828ce-5c05-4164-80fc-98bd42f9661d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35249ec2-ac25-41e1-849d-cac132b00176",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9c828ce-5c05-4164-80fc-98bd42f9661d",
                    "LayerId": "dc3c3d81-2d00-4bee-8611-213e0f07ca3d"
                }
            ]
        },
        {
            "id": "772020b3-f725-40bb-ab01-fa09f0f35e6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df88f139-061d-45b0-a34d-eb50ac105e6f",
            "compositeImage": {
                "id": "bbb7b9c9-a6ae-4b11-8867-230f8f036207",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "772020b3-f725-40bb-ab01-fa09f0f35e6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31c9c2f1-2290-4412-b5e2-40a50bfe700a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "772020b3-f725-40bb-ab01-fa09f0f35e6d",
                    "LayerId": "dc3c3d81-2d00-4bee-8611-213e0f07ca3d"
                }
            ]
        },
        {
            "id": "c93cf5d8-adff-46c5-875d-98c1694050d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df88f139-061d-45b0-a34d-eb50ac105e6f",
            "compositeImage": {
                "id": "bf947b82-60f1-4857-9e1b-c9c585f1c77a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c93cf5d8-adff-46c5-875d-98c1694050d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b67d77ca-66c2-452c-a525-7f3e76168e4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c93cf5d8-adff-46c5-875d-98c1694050d1",
                    "LayerId": "dc3c3d81-2d00-4bee-8611-213e0f07ca3d"
                }
            ]
        },
        {
            "id": "8d903199-61f0-480d-aa00-7adfbf3a5c22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df88f139-061d-45b0-a34d-eb50ac105e6f",
            "compositeImage": {
                "id": "3e9c72d4-55be-4bb4-97ad-5b6b3e96baef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d903199-61f0-480d-aa00-7adfbf3a5c22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "394f4221-3ac2-4aa4-b497-2f9e3c660e67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d903199-61f0-480d-aa00-7adfbf3a5c22",
                    "LayerId": "dc3c3d81-2d00-4bee-8611-213e0f07ca3d"
                }
            ]
        },
        {
            "id": "c21b75e7-4f98-40ee-8d10-975a1163c961",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df88f139-061d-45b0-a34d-eb50ac105e6f",
            "compositeImage": {
                "id": "843a6508-29dc-43c6-a207-3544d1334e56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c21b75e7-4f98-40ee-8d10-975a1163c961",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "360bdbeb-6bf9-4bc3-bea9-dfa4884c1e23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c21b75e7-4f98-40ee-8d10-975a1163c961",
                    "LayerId": "dc3c3d81-2d00-4bee-8611-213e0f07ca3d"
                }
            ]
        },
        {
            "id": "2b181272-bd8c-437c-a662-507a9b7c79d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df88f139-061d-45b0-a34d-eb50ac105e6f",
            "compositeImage": {
                "id": "1397ea78-a487-4dca-95c7-a18ba7e6f67c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b181272-bd8c-437c-a662-507a9b7c79d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbd5b51e-5c97-4e20-9e90-17f8265569e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b181272-bd8c-437c-a662-507a9b7c79d0",
                    "LayerId": "dc3c3d81-2d00-4bee-8611-213e0f07ca3d"
                }
            ]
        },
        {
            "id": "5fbba028-5e0c-4cc3-9b07-39d7ebec66ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df88f139-061d-45b0-a34d-eb50ac105e6f",
            "compositeImage": {
                "id": "94d78656-2a83-4245-9d11-a8592d49f791",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fbba028-5e0c-4cc3-9b07-39d7ebec66ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7596717c-8797-4e76-9149-e433c69d56c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fbba028-5e0c-4cc3-9b07-39d7ebec66ca",
                    "LayerId": "dc3c3d81-2d00-4bee-8611-213e0f07ca3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dc3c3d81-2d00-4bee-8611-213e0f07ca3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df88f139-061d-45b0-a34d-eb50ac105e6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 3,
    "yorig": 6
}