{
    "id": "d54d4870-dce4-430c-adb2-a1aef13f1251",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "BO_LGreenL",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f194e7d-d151-4751-89a7-060584727ac6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d54d4870-dce4-430c-adb2-a1aef13f1251",
            "compositeImage": {
                "id": "873e8017-06c6-4012-8388-8ae4f88d1305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f194e7d-d151-4751-89a7-060584727ac6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef86fc90-b839-401d-9b65-40fe77997741",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f194e7d-d151-4751-89a7-060584727ac6",
                    "LayerId": "a71fcd37-9819-470a-aaa5-acbc5ddeb589"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "a71fcd37-9819-470a-aaa5-acbc5ddeb589",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d54d4870-dce4-430c-adb2-a1aef13f1251",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 0,
    "yorig": 0
}