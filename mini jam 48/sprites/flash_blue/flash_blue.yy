{
    "id": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flash_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c92de9f1-d917-41bd-bd38-37d0416d98fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "f4c0c0a4-6132-46b9-97c5-f64976c0c45b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c92de9f1-d917-41bd-bd38-37d0416d98fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "006306a1-2199-43ff-92df-ce049fda3b41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c92de9f1-d917-41bd-bd38-37d0416d98fb",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "eb8cb095-ce70-461a-ab4e-127a52c14cc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "3b4a0ed4-2229-4c86-bb89-508186721943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb8cb095-ce70-461a-ab4e-127a52c14cc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bac468d-18cd-46bc-b53a-54b52d7dab4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb8cb095-ce70-461a-ab4e-127a52c14cc5",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "7ee0cee7-95d8-426c-bf8a-bdcc127279cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "a3df5669-33ba-437c-9b1a-f73d6fdaa697",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ee0cee7-95d8-426c-bf8a-bdcc127279cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84ff8869-8647-49f3-9d70-cb9176e2a1cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ee0cee7-95d8-426c-bf8a-bdcc127279cc",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "629ea6c8-a677-4b8d-887a-977f4f55ddec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "66414b77-1981-4167-8354-4dbb89082d86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "629ea6c8-a677-4b8d-887a-977f4f55ddec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1abe6a7-319b-4ab7-8310-ed31063b378d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "629ea6c8-a677-4b8d-887a-977f4f55ddec",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "dd7ecb73-c5be-4ce5-8748-5e3475a43476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "2034f857-4d33-485d-a2dd-3e9955951331",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd7ecb73-c5be-4ce5-8748-5e3475a43476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41082da5-8fa0-446a-9d3f-2087e5bf0560",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd7ecb73-c5be-4ce5-8748-5e3475a43476",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "c2494700-c136-40e2-ae15-5c5a8d515c4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "26367ca0-b37d-4d61-aede-3effd1831e00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2494700-c136-40e2-ae15-5c5a8d515c4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c57cfb99-c7b0-4c4b-9956-f2b8ee4a366c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2494700-c136-40e2-ae15-5c5a8d515c4c",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "a7dcafdd-46bf-4b83-84ae-7c2b37f408a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "82f43aed-eb42-49de-b6d2-7a185c67d27c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7dcafdd-46bf-4b83-84ae-7c2b37f408a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87073754-579f-48da-8c2d-677fa8d9b58b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7dcafdd-46bf-4b83-84ae-7c2b37f408a0",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "abbd6a6e-41c2-43ad-886b-2f17b68e4636",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "dee8843f-5e88-4ce6-8a87-46faff88d3b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abbd6a6e-41c2-43ad-886b-2f17b68e4636",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2f764f9-e7c3-4f91-9497-51b75b38f20d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abbd6a6e-41c2-43ad-886b-2f17b68e4636",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "00c7bd3a-afbc-45c3-9e0b-2e398d838f5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "2c8bfde8-cfbf-4a7b-80a0-875077dfa75c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00c7bd3a-afbc-45c3-9e0b-2e398d838f5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61b8921b-fe0c-49c0-8b4f-c2752070ac06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00c7bd3a-afbc-45c3-9e0b-2e398d838f5f",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "48e1a8c6-4539-4fca-af3f-bdc613f78030",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "0c379d4a-0a7b-4169-a608-59e15df9fc5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48e1a8c6-4539-4fca-af3f-bdc613f78030",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cf25085-8f8e-4c17-b999-dbbe65637f46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48e1a8c6-4539-4fca-af3f-bdc613f78030",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "c6e0dde7-2c29-4be0-810d-0fc5195e77dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "31a8e366-72c3-4ead-ba8a-963b31bad5ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6e0dde7-2c29-4be0-810d-0fc5195e77dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26dd142b-3ba0-411c-a06c-99608d9ed75b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6e0dde7-2c29-4be0-810d-0fc5195e77dc",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "77558f85-5630-4fc2-ab9d-ebc74b5ed791",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "74142ee9-f25f-48aa-b59e-d390b53d31a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77558f85-5630-4fc2-ab9d-ebc74b5ed791",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e40dd4f-d9e9-47f4-a966-98d580ae797e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77558f85-5630-4fc2-ab9d-ebc74b5ed791",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "3e0e6197-d319-49ad-a8ef-92fe4f7af1c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "6d50e43c-4ba5-48bb-91d6-2c7a6ddc67b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e0e6197-d319-49ad-a8ef-92fe4f7af1c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ca98671-9f76-4044-a027-ab866f259c09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e0e6197-d319-49ad-a8ef-92fe4f7af1c3",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "ff637f4a-3813-4dae-9818-bcd9fac1543e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "73ed9997-ad8e-40b8-a25d-877b7fa9ec25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff637f4a-3813-4dae-9818-bcd9fac1543e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "661d8176-ca9a-4ade-87da-d247a505647d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff637f4a-3813-4dae-9818-bcd9fac1543e",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "1e6e7db9-bc6d-4227-b3e7-0fa70f773cd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "7f8215d6-2261-4338-84fa-a833be4b7962",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e6e7db9-bc6d-4227-b3e7-0fa70f773cd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4482e661-bf4b-490b-9296-e97d21c8e101",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e6e7db9-bc6d-4227-b3e7-0fa70f773cd7",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "b980e1eb-a453-4ea0-885f-de360c04bb01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "8860c304-7df8-492b-aab3-10815df01451",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b980e1eb-a453-4ea0-885f-de360c04bb01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b3967be-dfd6-4c63-9733-944ed0ea99b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b980e1eb-a453-4ea0-885f-de360c04bb01",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "71556a7a-1443-4431-8c4a-c3a8e1240bad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "4f87cf54-5dc1-4462-9122-ac0c58cb565a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71556a7a-1443-4431-8c4a-c3a8e1240bad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a591a5b7-ab43-4790-9113-9fd8e4fb8e89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71556a7a-1443-4431-8c4a-c3a8e1240bad",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "d1ea556a-3cc8-44cd-98fc-c30c08cc85e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "9d469e49-dbe0-4160-9060-b11cc2e0128e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1ea556a-3cc8-44cd-98fc-c30c08cc85e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54b88f4c-1b3c-4d91-a0fb-5c43a79e2ca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1ea556a-3cc8-44cd-98fc-c30c08cc85e6",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        },
        {
            "id": "d01424eb-832c-4ff0-b9b0-b73b0ded2c09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "compositeImage": {
                "id": "e46a517e-54ec-44ac-92ff-a8d36568a758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d01424eb-832c-4ff0-b9b0-b73b0ded2c09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c0f3fd7-54a2-4331-bde2-e7efac51c727",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d01424eb-832c-4ff0-b9b0-b73b0ded2c09",
                    "LayerId": "13f41a9f-3151-4828-9d85-618970930e23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "13f41a9f-3151-4828-9d85-618970930e23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 21,
    "yorig": 20
}