{
    "id": "6982216d-b11c-46f4-a368-9e2c5a85cb1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_up_brown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74e1641e-4606-4247-9967-76a5a43b8fc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6982216d-b11c-46f4-a368-9e2c5a85cb1b",
            "compositeImage": {
                "id": "d34dfa03-209b-4bea-82ea-4635d8cf8c43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74e1641e-4606-4247-9967-76a5a43b8fc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "461c1a09-5f23-4fd5-be17-e6e49f768ee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74e1641e-4606-4247-9967-76a5a43b8fc6",
                    "LayerId": "cc7cff4d-4c4d-4ef3-bea2-5cbc642c9e33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cc7cff4d-4c4d-4ef3-bea2-5cbc642c9e33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6982216d-b11c-46f4-a368-9e2c5a85cb1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}