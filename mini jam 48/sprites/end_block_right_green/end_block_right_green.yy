{
    "id": "8907ba4c-2a92-4ccc-b996-b50eb9aabc99",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_right_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5a014a7-8fc0-4890-8929-0f867156f3f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8907ba4c-2a92-4ccc-b996-b50eb9aabc99",
            "compositeImage": {
                "id": "de820513-b02a-49e2-bb85-5c86e63ffe8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5a014a7-8fc0-4890-8929-0f867156f3f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45997e23-55a7-4779-bef2-6ee282f8b067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5a014a7-8fc0-4890-8929-0f867156f3f8",
                    "LayerId": "53386d23-f01f-44d2-b1f4-e23b5aa3804c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "53386d23-f01f-44d2-b1f4-e23b5aa3804c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8907ba4c-2a92-4ccc-b996-b50eb9aabc99",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}