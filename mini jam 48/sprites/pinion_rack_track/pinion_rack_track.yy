{
    "id": "2d3e0451-2f05-4f82-87f5-47bab1232662",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pinion_rack_track",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7810066d-89c4-475d-ae2a-80cbe352c78d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d3e0451-2f05-4f82-87f5-47bab1232662",
            "compositeImage": {
                "id": "47b347e9-0580-46d2-a5f4-f942d108e784",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7810066d-89c4-475d-ae2a-80cbe352c78d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf672b8b-d77c-4b57-aa48-85341cf73264",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7810066d-89c4-475d-ae2a-80cbe352c78d",
                    "LayerId": "0b6fec59-70e7-4c11-a0d9-8548c60c6aaf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "0b6fec59-70e7-4c11-a0d9-8548c60c6aaf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d3e0451-2f05-4f82-87f5-47bab1232662",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}