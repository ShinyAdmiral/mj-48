{
    "id": "94fe9687-e4f0-4ac1-b4b7-83ffab500541",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_left_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd56a6d9-bc32-4e26-bcac-a4ca5f5c72eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94fe9687-e4f0-4ac1-b4b7-83ffab500541",
            "compositeImage": {
                "id": "5ce00dfa-a1d8-4a14-b45b-98d5b2b9d2b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd56a6d9-bc32-4e26-bcac-a4ca5f5c72eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b9cbe9e-be01-41cf-8aa2-96e89f16f59c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd56a6d9-bc32-4e26-bcac-a4ca5f5c72eb",
                    "LayerId": "858e3343-ad43-42a3-9fc4-4c4e90b122a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "858e3343-ad43-42a3-9fc4-4c4e90b122a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94fe9687-e4f0-4ac1-b4b7-83ffab500541",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}