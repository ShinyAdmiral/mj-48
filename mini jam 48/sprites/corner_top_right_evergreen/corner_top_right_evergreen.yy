{
    "id": "b49d1a88-980b-4e65-ab0d-c11878e15c78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_top_right_evergreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69b74533-bdb3-46b5-b070-c2123c809aa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b49d1a88-980b-4e65-ab0d-c11878e15c78",
            "compositeImage": {
                "id": "e6ccd2f4-a91c-4aa8-9f3d-2d123fec6182",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69b74533-bdb3-46b5-b070-c2123c809aa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f3028a3-d35f-4f40-80f9-cc80db58f86b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69b74533-bdb3-46b5-b070-c2123c809aa5",
                    "LayerId": "cbc1b2ac-d4eb-400b-beab-868a18f0ca2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cbc1b2ac-d4eb-400b-beab-868a18f0ca2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b49d1a88-980b-4e65-ab0d-c11878e15c78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}