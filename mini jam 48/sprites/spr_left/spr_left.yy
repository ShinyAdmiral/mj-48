{
    "id": "761dd61d-10e4-4125-beee-277790c817b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 14,
    "bbox_right": 33,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "631deff8-63ea-4b22-bb73-4fb392c89c8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "761dd61d-10e4-4125-beee-277790c817b3",
            "compositeImage": {
                "id": "b83cd823-9fed-4cfa-9d40-f9551c3dd72c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "631deff8-63ea-4b22-bb73-4fb392c89c8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ad5cf42-8f24-45f2-aa68-276344c0c44f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "631deff8-63ea-4b22-bb73-4fb392c89c8a",
                    "LayerId": "d8b804f8-52a5-436d-86f7-1d4df347e314"
                }
            ]
        },
        {
            "id": "ff8be5e6-ff8f-40e9-ba1d-c8cf0fe8d431",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "761dd61d-10e4-4125-beee-277790c817b3",
            "compositeImage": {
                "id": "caf75d25-c571-470d-937a-44d02129715a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff8be5e6-ff8f-40e9-ba1d-c8cf0fe8d431",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37f3d0c3-80b0-447d-a981-a21f706e8114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff8be5e6-ff8f-40e9-ba1d-c8cf0fe8d431",
                    "LayerId": "d8b804f8-52a5-436d-86f7-1d4df347e314"
                }
            ]
        },
        {
            "id": "892d7985-c9d9-4a7e-b483-67ee020b4c48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "761dd61d-10e4-4125-beee-277790c817b3",
            "compositeImage": {
                "id": "c110a8e1-6ac8-4346-a3d2-6e143f950812",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "892d7985-c9d9-4a7e-b483-67ee020b4c48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f0fb185-ac2d-44b7-89dc-191431b010ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "892d7985-c9d9-4a7e-b483-67ee020b4c48",
                    "LayerId": "d8b804f8-52a5-436d-86f7-1d4df347e314"
                }
            ]
        },
        {
            "id": "5ce1da9d-d5e6-4417-b1ff-30a61ec123fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "761dd61d-10e4-4125-beee-277790c817b3",
            "compositeImage": {
                "id": "12fbf2a1-a33a-4979-84bc-e0b2337f0bd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ce1da9d-d5e6-4417-b1ff-30a61ec123fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ab62d71-1f6c-4f4c-b16d-a2dc88bad937",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ce1da9d-d5e6-4417-b1ff-30a61ec123fe",
                    "LayerId": "d8b804f8-52a5-436d-86f7-1d4df347e314"
                }
            ]
        },
        {
            "id": "3e74627f-ea1d-407b-b10f-2aa9ada06065",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "761dd61d-10e4-4125-beee-277790c817b3",
            "compositeImage": {
                "id": "df11fbbe-00fc-4d0f-b6c6-78301bc9a324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e74627f-ea1d-407b-b10f-2aa9ada06065",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b74efe9-a29f-4280-9904-cada331db715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e74627f-ea1d-407b-b10f-2aa9ada06065",
                    "LayerId": "d8b804f8-52a5-436d-86f7-1d4df347e314"
                }
            ]
        },
        {
            "id": "a2809555-6b11-401d-8a0f-1be66e343a62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "761dd61d-10e4-4125-beee-277790c817b3",
            "compositeImage": {
                "id": "907dd02e-7e08-48b2-b133-85e695351a57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2809555-6b11-401d-8a0f-1be66e343a62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "209b8b2f-4568-4a3c-b112-1448dc398f82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2809555-6b11-401d-8a0f-1be66e343a62",
                    "LayerId": "d8b804f8-52a5-436d-86f7-1d4df347e314"
                }
            ]
        },
        {
            "id": "210b486e-f10e-4251-bf43-a6250bf47b72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "761dd61d-10e4-4125-beee-277790c817b3",
            "compositeImage": {
                "id": "e62671f9-a766-43cf-acec-8da7aa57ce0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "210b486e-f10e-4251-bf43-a6250bf47b72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87c222f2-db74-4d42-a064-0135dee2629f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "210b486e-f10e-4251-bf43-a6250bf47b72",
                    "LayerId": "d8b804f8-52a5-436d-86f7-1d4df347e314"
                }
            ]
        },
        {
            "id": "1549e361-1022-44a1-b1f6-fb6ab72e4a55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "761dd61d-10e4-4125-beee-277790c817b3",
            "compositeImage": {
                "id": "300b6a28-62e4-4f9d-aec4-61f10b083a75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1549e361-1022-44a1-b1f6-fb6ab72e4a55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c21cc520-1078-4508-b00a-c27cf05ef103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1549e361-1022-44a1-b1f6-fb6ab72e4a55",
                    "LayerId": "d8b804f8-52a5-436d-86f7-1d4df347e314"
                }
            ]
        },
        {
            "id": "91534778-5816-41c1-a8e4-efbfbceb99a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "761dd61d-10e4-4125-beee-277790c817b3",
            "compositeImage": {
                "id": "75f6e07b-a9b3-434d-9e2b-133324e364e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91534778-5816-41c1-a8e4-efbfbceb99a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0889a0dc-d3c9-4529-99e1-027e3400f86a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91534778-5816-41c1-a8e4-efbfbceb99a7",
                    "LayerId": "d8b804f8-52a5-436d-86f7-1d4df347e314"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "d8b804f8-52a5-436d-86f7-1d4df347e314",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "761dd61d-10e4-4125-beee-277790c817b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}