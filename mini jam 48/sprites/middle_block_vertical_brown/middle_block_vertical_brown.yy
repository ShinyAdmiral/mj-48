{
    "id": "14bb4a06-250a-4584-b48b-ade1d3d608de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "middle_block_vertical_brown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69ee2b65-f5eb-4296-b8df-4b790782fbdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14bb4a06-250a-4584-b48b-ade1d3d608de",
            "compositeImage": {
                "id": "1c704c7e-f568-4639-9245-d400b49b51ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69ee2b65-f5eb-4296-b8df-4b790782fbdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5beed309-b7d3-4fd3-9bf9-1e821818f7a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69ee2b65-f5eb-4296-b8df-4b790782fbdd",
                    "LayerId": "cd1a6c98-163c-4948-9aab-bfae6ab24039"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cd1a6c98-163c-4948-9aab-bfae6ab24039",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14bb4a06-250a-4584-b48b-ade1d3d608de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}