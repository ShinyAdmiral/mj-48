{
    "id": "dd35be04-2ba5-4329-8ebf-4ce338a8fc00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "BO_DarkT",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc163721-fbc8-41a2-9117-3b9fb4437e65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd35be04-2ba5-4329-8ebf-4ce338a8fc00",
            "compositeImage": {
                "id": "d940a44a-fde3-4f8c-892d-5e94be9c132b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc163721-fbc8-41a2-9117-3b9fb4437e65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0073ddf3-2418-46d2-900a-e245bf5a4096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc163721-fbc8-41a2-9117-3b9fb4437e65",
                    "LayerId": "cbbde807-266f-4b0e-91fb-d8ae1fe0c934"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "cbbde807-266f-4b0e-91fb-d8ae1fe0c934",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd35be04-2ba5-4329-8ebf-4ce338a8fc00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 0,
    "yorig": 0
}