{
    "id": "efdbcdd0-91f7-451d-8eb4-0da041ee3711",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "BO_BrownI",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c77c61d0-d323-4051-a43b-86ca3d55c777",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efdbcdd0-91f7-451d-8eb4-0da041ee3711",
            "compositeImage": {
                "id": "07a2a733-10e4-4120-b8b5-965ffb2bc970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c77c61d0-d323-4051-a43b-86ca3d55c777",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "919318c0-2d25-4a47-9fba-18614ef0660c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c77c61d0-d323-4051-a43b-86ca3d55c777",
                    "LayerId": "205b92f6-420b-4ef7-8aff-f9adea8e51d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "205b92f6-420b-4ef7-8aff-f9adea8e51d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efdbcdd0-91f7-451d-8eb4-0da041ee3711",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 0,
    "yorig": 0
}