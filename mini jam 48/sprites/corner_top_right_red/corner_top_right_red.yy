{
    "id": "9a81ed8d-40e8-45ae-86b6-bf9ff82823a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_top_right_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36f65dd1-20c4-4972-b484-34c33c9ff6d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a81ed8d-40e8-45ae-86b6-bf9ff82823a3",
            "compositeImage": {
                "id": "f402a1f4-08a2-4881-9508-4024f65a39b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36f65dd1-20c4-4972-b484-34c33c9ff6d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbe83d49-1982-4050-9e73-931fde369cc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36f65dd1-20c4-4972-b484-34c33c9ff6d3",
                    "LayerId": "d202990d-b45e-4a82-a2a4-4bc811c24d9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d202990d-b45e-4a82-a2a4-4bc811c24d9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a81ed8d-40e8-45ae-86b6-bf9ff82823a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}