{
    "id": "58c70811-19c3-44ee-a717-6186097d8a21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_right_brown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ccab19f-1908-47f8-a7ec-fc3c7415cb71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58c70811-19c3-44ee-a717-6186097d8a21",
            "compositeImage": {
                "id": "86d3b4dd-1bf0-44a9-909e-f18d2af171d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ccab19f-1908-47f8-a7ec-fc3c7415cb71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d1cea0b-cb56-4319-8747-7bda9bdd8668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ccab19f-1908-47f8-a7ec-fc3c7415cb71",
                    "LayerId": "6b86d6d4-b5f3-4b36-a416-cc14dfcca5f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6b86d6d4-b5f3-4b36-a416-cc14dfcca5f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58c70811-19c3-44ee-a717-6186097d8a21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}