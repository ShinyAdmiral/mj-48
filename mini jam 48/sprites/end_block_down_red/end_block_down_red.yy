{
    "id": "130ce382-2391-4016-89a2-1c117679af9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_down_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e44cfc7e-5a88-4f4e-915f-39c933918ef2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "130ce382-2391-4016-89a2-1c117679af9c",
            "compositeImage": {
                "id": "5dd07dc2-a500-4fcf-b33f-ba49e96c446b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e44cfc7e-5a88-4f4e-915f-39c933918ef2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fe52db9-9c50-461e-940b-7f57aa11bee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e44cfc7e-5a88-4f4e-915f-39c933918ef2",
                    "LayerId": "e9e57c8b-26ae-4d7f-9e24-dd6e2079c243"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e9e57c8b-26ae-4d7f-9e24-dd6e2079c243",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "130ce382-2391-4016-89a2-1c117679af9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}