{
    "id": "5a608c39-1a15-40dc-a857-0bb76237ca3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_top_left_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20623038-e78a-4d9b-9464-9c2d09e8d658",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a608c39-1a15-40dc-a857-0bb76237ca3e",
            "compositeImage": {
                "id": "b1a1be69-0b22-48ad-9000-a05f90ccd8ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20623038-e78a-4d9b-9464-9c2d09e8d658",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "595cd11a-12ba-461d-a0fc-fccdc43c9783",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20623038-e78a-4d9b-9464-9c2d09e8d658",
                    "LayerId": "56ae24b9-e845-42ae-b52a-50f7f5661a27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "56ae24b9-e845-42ae-b52a-50f7f5661a27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a608c39-1a15-40dc-a857-0bb76237ca3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}