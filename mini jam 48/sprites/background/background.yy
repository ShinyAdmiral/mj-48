{
    "id": "cb8d7c0a-a615-490d-b065-969d13fbd66a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 575,
    "bbox_left": 0,
    "bbox_right": 351,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9be21de5-a62b-4a04-a796-1643b3eaa269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb8d7c0a-a615-490d-b065-969d13fbd66a",
            "compositeImage": {
                "id": "939eb474-2ebe-4640-b6b7-70c7177d2bfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9be21de5-a62b-4a04-a796-1643b3eaa269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dfd97ea-6402-45c6-b374-2de40423bccb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9be21de5-a62b-4a04-a796-1643b3eaa269",
                    "LayerId": "a0358860-a674-4482-9210-3c65c619e2ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 576,
    "layers": [
        {
            "id": "a0358860-a674-4482-9210-3c65c619e2ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb8d7c0a-a615-490d-b065-969d13fbd66a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 352,
    "xorig": 0,
    "yorig": 0
}