{
    "id": "e8024285-1608-451d-9b14-53b9bd51fa40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_top_left_evergreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1633f008-c2d4-4033-8c5c-4d923b52240a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8024285-1608-451d-9b14-53b9bd51fa40",
            "compositeImage": {
                "id": "ad484b4e-7970-4494-91bb-c2e28c7887c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1633f008-c2d4-4033-8c5c-4d923b52240a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "508b5c5d-23f5-441c-8bfd-ac4e6fcf17a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1633f008-c2d4-4033-8c5c-4d923b52240a",
                    "LayerId": "04a84205-fb08-4d42-9fff-2ce0b2b1d5e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "04a84205-fb08-4d42-9fff-2ce0b2b1d5e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8024285-1608-451d-9b14-53b9bd51fa40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}