{
    "id": "6915be37-e4e1-4a63-bc9a-8584db653389",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "robert_discharge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 87,
    "bbox_left": 18,
    "bbox_right": 87,
    "bbox_top": 19,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "435ca59c-b833-4842-957a-8ab28d6f0bc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6915be37-e4e1-4a63-bc9a-8584db653389",
            "compositeImage": {
                "id": "3024026e-0820-44f9-b8af-cbe74ea18539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "435ca59c-b833-4842-957a-8ab28d6f0bc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db107809-cc23-4f5e-8736-a9a126edb8cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "435ca59c-b833-4842-957a-8ab28d6f0bc3",
                    "LayerId": "5a6cf84c-4708-4006-b862-7dff8fe0873e"
                }
            ]
        },
        {
            "id": "12f5d8b7-858b-4251-a736-21b4b14fc0b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6915be37-e4e1-4a63-bc9a-8584db653389",
            "compositeImage": {
                "id": "7db82e42-e07f-47c4-a086-3fc8ea220eec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12f5d8b7-858b-4251-a736-21b4b14fc0b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c706f3e-5a60-4ab3-b031-30dafaa715aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f5d8b7-858b-4251-a736-21b4b14fc0b8",
                    "LayerId": "5a6cf84c-4708-4006-b862-7dff8fe0873e"
                }
            ]
        },
        {
            "id": "b5321339-2149-4776-8404-78198264dbb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6915be37-e4e1-4a63-bc9a-8584db653389",
            "compositeImage": {
                "id": "ca61f52f-a4d5-4425-998c-90618e0981f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5321339-2149-4776-8404-78198264dbb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1d73468-5500-42cc-bb2f-dc714adb069a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5321339-2149-4776-8404-78198264dbb4",
                    "LayerId": "5a6cf84c-4708-4006-b862-7dff8fe0873e"
                }
            ]
        },
        {
            "id": "361d340d-00ad-4dd7-b9a2-63280dc99d02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6915be37-e4e1-4a63-bc9a-8584db653389",
            "compositeImage": {
                "id": "6aac9eb6-79a7-4f05-a35c-344592f283cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "361d340d-00ad-4dd7-b9a2-63280dc99d02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbffef44-2bc4-4c11-b264-780904dc6366",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "361d340d-00ad-4dd7-b9a2-63280dc99d02",
                    "LayerId": "5a6cf84c-4708-4006-b862-7dff8fe0873e"
                }
            ]
        },
        {
            "id": "f73a7972-172b-4bd3-b32a-fb59b375e922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6915be37-e4e1-4a63-bc9a-8584db653389",
            "compositeImage": {
                "id": "a553cc1f-94d9-4e2a-a75b-8e26ded7fa60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f73a7972-172b-4bd3-b32a-fb59b375e922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc25f277-672e-49d9-8ff4-31c7b8bc11a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f73a7972-172b-4bd3-b32a-fb59b375e922",
                    "LayerId": "5a6cf84c-4708-4006-b862-7dff8fe0873e"
                }
            ]
        },
        {
            "id": "09511bc4-49c1-4f1c-9aef-3bdb4db403c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6915be37-e4e1-4a63-bc9a-8584db653389",
            "compositeImage": {
                "id": "60f77930-38ec-4947-afe0-a857fc91763e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09511bc4-49c1-4f1c-9aef-3bdb4db403c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "539bde33-408c-42ab-b56e-6e299893caae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09511bc4-49c1-4f1c-9aef-3bdb4db403c1",
                    "LayerId": "5a6cf84c-4708-4006-b862-7dff8fe0873e"
                }
            ]
        },
        {
            "id": "e6c727df-c7b3-46b6-881a-96c260433e14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6915be37-e4e1-4a63-bc9a-8584db653389",
            "compositeImage": {
                "id": "6f8574ce-f510-40ef-bc93-71545dcc85d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6c727df-c7b3-46b6-881a-96c260433e14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8de1d3b3-c3a1-4fd1-9417-874d861b27f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6c727df-c7b3-46b6-881a-96c260433e14",
                    "LayerId": "5a6cf84c-4708-4006-b862-7dff8fe0873e"
                }
            ]
        },
        {
            "id": "e21f151a-25ed-4d8d-9918-b29b6fcdee57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6915be37-e4e1-4a63-bc9a-8584db653389",
            "compositeImage": {
                "id": "82f37440-1305-4410-93d9-62ba127fdede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e21f151a-25ed-4d8d-9918-b29b6fcdee57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4225afb1-2334-4ac7-8b95-4cb7eb7cc586",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e21f151a-25ed-4d8d-9918-b29b6fcdee57",
                    "LayerId": "5a6cf84c-4708-4006-b862-7dff8fe0873e"
                }
            ]
        },
        {
            "id": "1697d6ca-216d-4ef4-a066-c6224883453b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6915be37-e4e1-4a63-bc9a-8584db653389",
            "compositeImage": {
                "id": "d92a1f9e-6945-4e91-8da7-d19cb042000d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1697d6ca-216d-4ef4-a066-c6224883453b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d84b2e88-4999-4e07-9af9-c07c2b4dbe63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1697d6ca-216d-4ef4-a066-c6224883453b",
                    "LayerId": "5a6cf84c-4708-4006-b862-7dff8fe0873e"
                }
            ]
        },
        {
            "id": "ce4bba96-6556-440d-a399-b88f95af294b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6915be37-e4e1-4a63-bc9a-8584db653389",
            "compositeImage": {
                "id": "55d9c942-58b2-4776-ba87-c6ac9ce5b23a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce4bba96-6556-440d-a399-b88f95af294b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3866b86-e5d4-4b89-928c-28ea11829614",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce4bba96-6556-440d-a399-b88f95af294b",
                    "LayerId": "5a6cf84c-4708-4006-b862-7dff8fe0873e"
                }
            ]
        },
        {
            "id": "aba62247-5e51-4257-9c9e-f56ee8009922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6915be37-e4e1-4a63-bc9a-8584db653389",
            "compositeImage": {
                "id": "ff80f5d1-7365-4c0c-bcc2-d962a6eae406",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aba62247-5e51-4257-9c9e-f56ee8009922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5640037-8194-40ba-8945-56b7a1b2aef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aba62247-5e51-4257-9c9e-f56ee8009922",
                    "LayerId": "5a6cf84c-4708-4006-b862-7dff8fe0873e"
                }
            ]
        },
        {
            "id": "dcdc34b8-766a-4b28-9834-001cef18f9f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6915be37-e4e1-4a63-bc9a-8584db653389",
            "compositeImage": {
                "id": "a5ef6d61-4ee3-43fc-b894-f7efdf7f4817",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcdc34b8-766a-4b28-9834-001cef18f9f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e1bfcbf-add3-4b55-9025-91e62828f5a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcdc34b8-766a-4b28-9834-001cef18f9f9",
                    "LayerId": "5a6cf84c-4708-4006-b862-7dff8fe0873e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "5a6cf84c-4708-4006-b862-7dff8fe0873e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6915be37-e4e1-4a63-bc9a-8584db653389",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 106,
    "xorig": 53,
    "yorig": 53
}