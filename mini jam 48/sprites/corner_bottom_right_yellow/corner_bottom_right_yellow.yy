{
    "id": "0dea376d-e30b-4da7-b25f-bc6b0a899427",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_bottom_right_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "542004c2-43f3-440e-aeeb-cd65a1a7b4e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dea376d-e30b-4da7-b25f-bc6b0a899427",
            "compositeImage": {
                "id": "416b0459-03e5-4882-b8df-e2b4cc4a059f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "542004c2-43f3-440e-aeeb-cd65a1a7b4e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46920fa9-9469-4fd3-b1a9-29b38ff76ef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "542004c2-43f3-440e-aeeb-cd65a1a7b4e8",
                    "LayerId": "04b2561d-4400-49e5-b9a2-6ab578e5c71c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "04b2561d-4400-49e5-b9a2-6ab578e5c71c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0dea376d-e30b-4da7-b25f-bc6b0a899427",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}