{
    "id": "6431e5d3-304c-4979-8465-1b80e2f9668a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_up_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb85f176-1bed-4c52-92a2-a9fb97ff069a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6431e5d3-304c-4979-8465-1b80e2f9668a",
            "compositeImage": {
                "id": "b80186da-a53d-41f2-aead-b798a164b75f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb85f176-1bed-4c52-92a2-a9fb97ff069a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7924334c-192c-4fa6-b5dc-f7f4308147bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb85f176-1bed-4c52-92a2-a9fb97ff069a",
                    "LayerId": "97409d89-9b2e-4ea3-a3fd-399557fdc46c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "97409d89-9b2e-4ea3-a3fd-399557fdc46c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6431e5d3-304c-4979-8465-1b80e2f9668a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}