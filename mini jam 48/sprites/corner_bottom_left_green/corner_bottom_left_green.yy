{
    "id": "6435fdec-068a-4579-b5eb-3c73e9830fed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_bottom_left_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a48203ad-607d-4703-ac7e-3f854950d324",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6435fdec-068a-4579-b5eb-3c73e9830fed",
            "compositeImage": {
                "id": "4d6de865-e574-4328-abf0-0bcdfe32ab06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a48203ad-607d-4703-ac7e-3f854950d324",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e81738ec-48e9-4afb-946b-246110a8b675",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a48203ad-607d-4703-ac7e-3f854950d324",
                    "LayerId": "4d8f9116-97c6-4caa-8042-c71f51aba570"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4d8f9116-97c6-4caa-8042-c71f51aba570",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6435fdec-068a-4579-b5eb-3c73e9830fed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}