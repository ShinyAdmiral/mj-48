{
    "id": "864e9292-c6aa-43e6-9923-7712c4a05136",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "middle_block_vertical_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16a7aa23-0eb0-4829-a111-6997b4965329",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "864e9292-c6aa-43e6-9923-7712c4a05136",
            "compositeImage": {
                "id": "0237197d-14a2-4c62-92ac-7274feebce0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16a7aa23-0eb0-4829-a111-6997b4965329",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f82206ae-a479-4d99-ac35-675ee7d660ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16a7aa23-0eb0-4829-a111-6997b4965329",
                    "LayerId": "8a218020-4792-4ab4-8709-0c8ca9814811"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8a218020-4792-4ab4-8709-0c8ca9814811",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "864e9292-c6aa-43e6-9923-7712c4a05136",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}