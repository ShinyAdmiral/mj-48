{
    "id": "c1b12800-a4c2-4c4e-b74e-b4822acb5b14",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_bottom_right_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53dad835-8a39-4005-a780-321422c9bdcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1b12800-a4c2-4c4e-b74e-b4822acb5b14",
            "compositeImage": {
                "id": "690088a9-12ae-4a02-a8db-a7b03bd8bde3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53dad835-8a39-4005-a780-321422c9bdcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4298d6d4-8d70-470e-a64d-c58ec7d10659",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53dad835-8a39-4005-a780-321422c9bdcb",
                    "LayerId": "193e232f-f617-467c-957a-0affdd39da5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "193e232f-f617-467c-957a-0affdd39da5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1b12800-a4c2-4c4e-b74e-b4822acb5b14",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}