{
    "id": "1d6668c6-ab69-42ea-9609-acd367c9944a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "PlayButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7377d090-51fc-48ec-aacc-07cfe51d7725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d6668c6-ab69-42ea-9609-acd367c9944a",
            "compositeImage": {
                "id": "28d88464-a6df-4f3a-8b76-12990e1ed7ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7377d090-51fc-48ec-aacc-07cfe51d7725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5987b12-c383-4c6f-999b-cf1a19301fbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7377d090-51fc-48ec-aacc-07cfe51d7725",
                    "LayerId": "43f87ab3-fa24-48da-8b0d-c92fef6dbec4"
                }
            ]
        },
        {
            "id": "6aba84d7-df1d-41c7-a4fb-b539cfceee4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d6668c6-ab69-42ea-9609-acd367c9944a",
            "compositeImage": {
                "id": "6564f3aa-6794-403a-a52e-e88c899ed297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aba84d7-df1d-41c7-a4fb-b539cfceee4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b73a4079-4fb8-4c59-94be-9326496ace93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aba84d7-df1d-41c7-a4fb-b539cfceee4f",
                    "LayerId": "43f87ab3-fa24-48da-8b0d-c92fef6dbec4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "43f87ab3-fa24-48da-8b0d-c92fef6dbec4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d6668c6-ab69-42ea-9609-acd367c9944a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}