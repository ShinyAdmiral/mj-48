{
    "id": "ea3eda40-8624-4d76-ad9a-22c4216313f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ProgressBar_Right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 491,
    "bbox_left": 12,
    "bbox_right": 42,
    "bbox_top": 290,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e496bf94-f29b-4d70-9c27-84e724abb2dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea3eda40-8624-4d76-ad9a-22c4216313f7",
            "compositeImage": {
                "id": "470b2b20-e451-4b6f-95b9-be2c6e179293",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e496bf94-f29b-4d70-9c27-84e724abb2dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13930d69-99d5-4275-bb1a-613e7392bfc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e496bf94-f29b-4d70-9c27-84e724abb2dd",
                    "LayerId": "db469fbc-00e6-444b-9f17-885db4f6307a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 503,
    "layers": [
        {
            "id": "db469fbc-00e6-444b-9f17-885db4f6307a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea3eda40-8624-4d76-ad9a-22c4216313f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 94,
    "xorig": 0,
    "yorig": 0
}