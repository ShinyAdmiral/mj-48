{
    "id": "f58e3412-157f-41c6-b598-369932a28e32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "robert_dis",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 22,
    "bbox_right": 41,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10634a78-5f08-4ba9-bf42-7a488b8f5149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f58e3412-157f-41c6-b598-369932a28e32",
            "compositeImage": {
                "id": "ea0e90d5-aa54-49bd-af7b-113a34711831",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10634a78-5f08-4ba9-bf42-7a488b8f5149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed888441-cf77-42c9-8f12-42c2b62c6cbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10634a78-5f08-4ba9-bf42-7a488b8f5149",
                    "LayerId": "cfdc27a3-dadd-43e7-83df-715166c978b5"
                }
            ]
        },
        {
            "id": "e8a96852-cbc5-4fb9-b865-4317483a8244",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f58e3412-157f-41c6-b598-369932a28e32",
            "compositeImage": {
                "id": "afb37e26-6d13-4559-816c-837d4a7f8512",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8a96852-cbc5-4fb9-b865-4317483a8244",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae8e6f29-2b7b-470e-a9f3-397e8e8d4daf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8a96852-cbc5-4fb9-b865-4317483a8244",
                    "LayerId": "cfdc27a3-dadd-43e7-83df-715166c978b5"
                }
            ]
        },
        {
            "id": "9935aa33-fb0a-40dc-96c9-77044806b17e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f58e3412-157f-41c6-b598-369932a28e32",
            "compositeImage": {
                "id": "d13061fb-2c34-4765-9f91-af2d62f3736f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9935aa33-fb0a-40dc-96c9-77044806b17e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5e5ad83-d663-4822-ad82-37d745feefc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9935aa33-fb0a-40dc-96c9-77044806b17e",
                    "LayerId": "cfdc27a3-dadd-43e7-83df-715166c978b5"
                }
            ]
        },
        {
            "id": "385782b8-ae86-4f90-af41-0fe7b2e622e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f58e3412-157f-41c6-b598-369932a28e32",
            "compositeImage": {
                "id": "efb36c7b-ff8a-4533-b004-4e8546479b8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "385782b8-ae86-4f90-af41-0fe7b2e622e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aaf749e-5455-4e8f-90e6-119ec85313fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "385782b8-ae86-4f90-af41-0fe7b2e622e9",
                    "LayerId": "cfdc27a3-dadd-43e7-83df-715166c978b5"
                }
            ]
        },
        {
            "id": "c9c430e0-dc1f-4ca1-b38f-45d2a73a7502",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f58e3412-157f-41c6-b598-369932a28e32",
            "compositeImage": {
                "id": "7b3a7b57-c0ee-4749-b8bc-3e0f90730456",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9c430e0-dc1f-4ca1-b38f-45d2a73a7502",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86acde13-a77a-4888-bc05-a62436b2f9b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9c430e0-dc1f-4ca1-b38f-45d2a73a7502",
                    "LayerId": "cfdc27a3-dadd-43e7-83df-715166c978b5"
                }
            ]
        },
        {
            "id": "83fb65f8-4c97-4401-b603-6b451f00c458",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f58e3412-157f-41c6-b598-369932a28e32",
            "compositeImage": {
                "id": "a8c71fdc-40d7-4709-b95b-0758106ed5c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83fb65f8-4c97-4401-b603-6b451f00c458",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71125e2d-915a-4bc8-81b3-076d32abf848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83fb65f8-4c97-4401-b603-6b451f00c458",
                    "LayerId": "cfdc27a3-dadd-43e7-83df-715166c978b5"
                }
            ]
        },
        {
            "id": "0ea50c08-c1b4-48e4-a35e-07dfcaaa6dfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f58e3412-157f-41c6-b598-369932a28e32",
            "compositeImage": {
                "id": "fa35d5b2-705f-4a00-b88f-3e200c09b4bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea50c08-c1b4-48e4-a35e-07dfcaaa6dfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3951e8e0-ee87-49af-8ef9-cc8b3c34ea58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea50c08-c1b4-48e4-a35e-07dfcaaa6dfa",
                    "LayerId": "cfdc27a3-dadd-43e7-83df-715166c978b5"
                }
            ]
        },
        {
            "id": "809e7bbb-f111-4303-b6dc-eec3f1ef94f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f58e3412-157f-41c6-b598-369932a28e32",
            "compositeImage": {
                "id": "904e298b-2659-4b31-a664-bff77acecb08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "809e7bbb-f111-4303-b6dc-eec3f1ef94f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffeaaf48-2e6e-4abc-b146-9453dadc71d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "809e7bbb-f111-4303-b6dc-eec3f1ef94f9",
                    "LayerId": "cfdc27a3-dadd-43e7-83df-715166c978b5"
                }
            ]
        },
        {
            "id": "d3ad872f-600c-4e0d-9320-610092909075",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f58e3412-157f-41c6-b598-369932a28e32",
            "compositeImage": {
                "id": "b4a860c7-6404-4047-87dc-85cc625b9636",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3ad872f-600c-4e0d-9320-610092909075",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8055cc3b-5467-4e37-a73f-67eca4a33ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3ad872f-600c-4e0d-9320-610092909075",
                    "LayerId": "cfdc27a3-dadd-43e7-83df-715166c978b5"
                }
            ]
        },
        {
            "id": "c2e57a1f-7013-4f59-8fd7-818f1383a1b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f58e3412-157f-41c6-b598-369932a28e32",
            "compositeImage": {
                "id": "9af968a3-1a23-46eb-a4ea-1c3d636c84f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2e57a1f-7013-4f59-8fd7-818f1383a1b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "909b5f69-88c6-4950-be58-29194f99d164",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2e57a1f-7013-4f59-8fd7-818f1383a1b2",
                    "LayerId": "cfdc27a3-dadd-43e7-83df-715166c978b5"
                }
            ]
        },
        {
            "id": "f052c656-e0aa-4a96-9eec-768a542453a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f58e3412-157f-41c6-b598-369932a28e32",
            "compositeImage": {
                "id": "a2026cdc-c74f-4b8d-bf97-62c2cb70262a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f052c656-e0aa-4a96-9eec-768a542453a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97ba7f26-1ee3-4eb8-8f53-b97377d1f1af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f052c656-e0aa-4a96-9eec-768a542453a9",
                    "LayerId": "cfdc27a3-dadd-43e7-83df-715166c978b5"
                }
            ]
        },
        {
            "id": "c47ce50e-7cb6-4b4c-91c0-a89c1dac9756",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f58e3412-157f-41c6-b598-369932a28e32",
            "compositeImage": {
                "id": "972ecdeb-a1bb-4a8f-a732-55f4c1b1e473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c47ce50e-7cb6-4b4c-91c0-a89c1dac9756",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2df89f4-ee1f-4c0b-b4b9-e21e6b8c163c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c47ce50e-7cb6-4b4c-91c0-a89c1dac9756",
                    "LayerId": "cfdc27a3-dadd-43e7-83df-715166c978b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cfdc27a3-dadd-43e7-83df-715166c978b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f58e3412-157f-41c6-b598-369932a28e32",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 40
}