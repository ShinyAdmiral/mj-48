{
    "id": "6647136a-d7bd-4b12-9283-28193953d2ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "robert_landing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 2,
    "bbox_right": 59,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ae09c85-b316-4563-87c6-8ed76206f060",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "987246f7-c573-44fe-92a6-7fdb85c66634",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ae09c85-b316-4563-87c6-8ed76206f060",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85cb2284-73be-4d36-8f4a-473238d6feca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ae09c85-b316-4563-87c6-8ed76206f060",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "00659320-27a1-48bb-8c0d-8e144c1c6220",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "a68e3394-0f95-4c0e-beac-b789b86b7fd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00659320-27a1-48bb-8c0d-8e144c1c6220",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eac8d652-d3d1-4a4a-9824-519cae821deb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00659320-27a1-48bb-8c0d-8e144c1c6220",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "19818273-cdfd-44e5-ae54-adc3f8688fda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "fdd08d9e-59e6-48f3-b3a9-80206e83a85c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19818273-cdfd-44e5-ae54-adc3f8688fda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3874f8d-29f9-40a9-b4ef-a4add40c78bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19818273-cdfd-44e5-ae54-adc3f8688fda",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "2f243c55-4482-498b-bb32-9c824155b70b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "cdc055a4-8b9f-4345-a3fd-65ddc438a23b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f243c55-4482-498b-bb32-9c824155b70b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddfd3a8d-dde9-40a5-b6ba-20e8cb2fa6fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f243c55-4482-498b-bb32-9c824155b70b",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "ba0f206e-468c-440c-a7d6-0c661cc0f507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "b50a9b73-3c3e-4fd0-89b6-62fac08d402c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba0f206e-468c-440c-a7d6-0c661cc0f507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3d43620-2025-46c8-ae1d-ba27accb9411",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba0f206e-468c-440c-a7d6-0c661cc0f507",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "eb39838d-6265-4e35-b4fe-0d92c4331f56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "307418cb-eb31-420b-acd9-cc6fcd4d4aa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb39838d-6265-4e35-b4fe-0d92c4331f56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f502ac9d-a97d-450a-89ca-eab4ffac7abb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb39838d-6265-4e35-b4fe-0d92c4331f56",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "bce26b9f-ab4a-41bf-9492-614aee88b2f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "18e1186d-b9ab-44da-9caa-a603c097308f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bce26b9f-ab4a-41bf-9492-614aee88b2f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "570e1112-385c-47be-a515-13c9064affa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bce26b9f-ab4a-41bf-9492-614aee88b2f1",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "811141c4-c5dc-4921-9fdd-a0e66516ecf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "303416a6-026d-46ac-9366-f6972376768c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "811141c4-c5dc-4921-9fdd-a0e66516ecf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ce9cc54-665e-48aa-8183-3fbd839a0623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "811141c4-c5dc-4921-9fdd-a0e66516ecf8",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "7827c400-a618-403d-9f01-abab27f8443b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "9cf245ce-5532-4599-bbdb-f44f6c13101a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7827c400-a618-403d-9f01-abab27f8443b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "461f78cd-97a3-4d72-8778-116200d3e383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7827c400-a618-403d-9f01-abab27f8443b",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "dcb1d3eb-3e5c-4a77-902c-5efb5bb4d4d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "0b037a61-2acf-410a-b97a-de0cedfc3e8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcb1d3eb-3e5c-4a77-902c-5efb5bb4d4d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb484b50-b8db-45f5-9652-54cd04c9ea91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcb1d3eb-3e5c-4a77-902c-5efb5bb4d4d9",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "ca115764-1ab8-42db-8d42-f9373d91f985",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "5a44460f-732a-4c51-8c2b-4bd833785840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca115764-1ab8-42db-8d42-f9373d91f985",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5c7aa79-2796-43e6-a1de-15f977317551",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca115764-1ab8-42db-8d42-f9373d91f985",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "27fc05fa-2c72-4821-8b06-ed7fb3aa01bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "617101b9-14c9-42e2-9ef8-45d32e70b593",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27fc05fa-2c72-4821-8b06-ed7fb3aa01bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46dc2176-fce1-4da3-b8b9-6581ece20fb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27fc05fa-2c72-4821-8b06-ed7fb3aa01bf",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "4621a10b-c740-4a0f-afbd-bb292f217ef1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "6618a815-5d9e-4de1-b8bb-112c01b499d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4621a10b-c740-4a0f-afbd-bb292f217ef1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daff983b-f39c-4606-8161-3c88c825da66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4621a10b-c740-4a0f-afbd-bb292f217ef1",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "5a6aa65c-61ed-4be8-811f-8eb2de3a717c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "8d1380f8-07fc-4c70-88c6-6b4330cec1f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a6aa65c-61ed-4be8-811f-8eb2de3a717c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e31cfb9e-dc1c-4176-949b-80749d02dbb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a6aa65c-61ed-4be8-811f-8eb2de3a717c",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "a9ede9c6-a076-4d50-9155-fb13861fb6e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "177db9d8-a0c2-4782-8b4e-4acc76497aad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9ede9c6-a076-4d50-9155-fb13861fb6e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "408edf2d-7434-4c3a-bbd5-1635d850747f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9ede9c6-a076-4d50-9155-fb13861fb6e4",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "1ed9fcdb-68e1-421a-bf8a-c872ef6808f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "954d3d24-bda5-4c02-a5b4-693eb8f4071a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ed9fcdb-68e1-421a-bf8a-c872ef6808f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "342322e1-4707-4cc9-9e95-e7751418b8d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ed9fcdb-68e1-421a-bf8a-c872ef6808f5",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "119a2ddc-1bd5-4563-894c-cb58c7002a31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "89b2a53f-cad1-47c9-b560-4bf67064ed25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "119a2ddc-1bd5-4563-894c-cb58c7002a31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9bbe4f1-4be8-46f9-9314-2ee24b51d670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "119a2ddc-1bd5-4563-894c-cb58c7002a31",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "9397e159-85ea-4cb5-a84a-eaf1f7ce976d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "fa852082-a5cf-4f22-80e5-9c336aead3ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9397e159-85ea-4cb5-a84a-eaf1f7ce976d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6c47d20-e8c6-4ea0-b956-32afac0e24c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9397e159-85ea-4cb5-a84a-eaf1f7ce976d",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "7c09517b-d620-4710-a0ae-41b53696c683",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "fd14cc29-93aa-48b3-8015-9ecf9849b21f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c09517b-d620-4710-a0ae-41b53696c683",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "199543ed-7725-44b2-9a80-bb972615ab6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c09517b-d620-4710-a0ae-41b53696c683",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "b4cab69e-015d-4a85-aef2-468846efc443",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "cac5840a-9700-4a3e-9b22-97b218fed13a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4cab69e-015d-4a85-aef2-468846efc443",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3ad597c-c2ca-4d65-9435-b99f26123846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4cab69e-015d-4a85-aef2-468846efc443",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "e6b6bd0e-b4e4-4f23-9350-7184ed4f0046",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "f5debf9d-38a1-480b-b9cb-f52f7e387e1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6b6bd0e-b4e4-4f23-9350-7184ed4f0046",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a90c3e90-d526-44b7-9a08-0cabc8b79974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6b6bd0e-b4e4-4f23-9350-7184ed4f0046",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "ccd56121-e3de-4980-8a39-3aa5f62b972b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "cbb6b6ae-165d-4e63-a8dd-4f726bc347b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccd56121-e3de-4980-8a39-3aa5f62b972b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a286ca9-2aeb-4bfd-b910-5a2ffacdd21b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccd56121-e3de-4980-8a39-3aa5f62b972b",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "0955d959-5c55-45f2-a81b-24a45e14f265",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "8e390b7f-8b70-41b5-83f9-92bc954ca312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0955d959-5c55-45f2-a81b-24a45e14f265",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fe79012-0399-4573-8f33-633d3ee9ee27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0955d959-5c55-45f2-a81b-24a45e14f265",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "f2e4f542-7fc9-4a3a-aafe-50da83e2bbbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "dc45e52f-f67c-46cd-b608-a9ee54a1f0b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2e4f542-7fc9-4a3a-aafe-50da83e2bbbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2065def2-d96f-40c6-9540-fd17d379d08f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2e4f542-7fc9-4a3a-aafe-50da83e2bbbf",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "6278cdc3-92db-48f8-8904-4d4e690fd2f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "3af15b8e-19fd-499e-aa7a-1f1dda3adc8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6278cdc3-92db-48f8-8904-4d4e690fd2f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "932259e6-0ad6-46c2-9427-980fd3cf4974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6278cdc3-92db-48f8-8904-4d4e690fd2f7",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "6c97e0da-9884-4ece-b643-99a82078a0ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "7a025cc2-6554-4955-b4fd-94316a501f5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c97e0da-9884-4ece-b643-99a82078a0ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b844cdfc-9798-47bb-8fa4-4ee591f0f80d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c97e0da-9884-4ece-b643-99a82078a0ae",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "a5c74b9b-951b-4943-b8a5-970a706b63a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "70dd851e-3d3f-43ff-a47f-45bc171c3b51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5c74b9b-951b-4943-b8a5-970a706b63a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "715c9496-a85c-4203-9d05-856235ae697d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5c74b9b-951b-4943-b8a5-970a706b63a2",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "722145c2-5e52-43da-bc70-fe8bd3220d11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "cd10ad8d-52ad-41cc-8594-077aad13eacc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "722145c2-5e52-43da-bc70-fe8bd3220d11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0abf7db9-22d1-40c7-b5fd-c94124761f33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "722145c2-5e52-43da-bc70-fe8bd3220d11",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "6b3900df-f8f3-41e5-8833-d2ac8d186647",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "ebfbb867-6eb3-4253-b514-24361d5eea0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b3900df-f8f3-41e5-8833-d2ac8d186647",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3d73565-d03d-4398-9f43-6b132d38015d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b3900df-f8f3-41e5-8833-d2ac8d186647",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "f298b672-0ff9-4551-8c9b-6d457bdabac8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "04526675-dfce-44c8-8c56-2239bbdb1f37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f298b672-0ff9-4551-8c9b-6d457bdabac8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c978831e-5453-4f4f-a7e3-e901b0302906",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f298b672-0ff9-4551-8c9b-6d457bdabac8",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "c93bcab3-506d-4a3c-a672-7cf0d3932253",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "d70b838c-dcfe-4f2b-a1ae-53df433144ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c93bcab3-506d-4a3c-a672-7cf0d3932253",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a39bcf3-8756-44eb-9106-9397e4624e9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c93bcab3-506d-4a3c-a672-7cf0d3932253",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "11100aca-25f7-4efd-b66f-194c04a992ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "1c0e710a-0cf9-43ad-9d92-05a7d8b244d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11100aca-25f7-4efd-b66f-194c04a992ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f9a8823-d216-4ccc-8263-18bd94cadd3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11100aca-25f7-4efd-b66f-194c04a992ca",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        },
        {
            "id": "390af2ed-0d94-4446-a36c-3043b318ad48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "compositeImage": {
                "id": "1a748521-ea91-4daf-99ab-5ef8ba2d8e7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "390af2ed-0d94-4446-a36c-3043b318ad48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad1648fc-ccfa-4bcb-ad53-872833e4dd59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "390af2ed-0d94-4446-a36c-3043b318ad48",
                    "LayerId": "abe60db6-0a74-4bdb-a466-c57e40cb25c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "abe60db6-0a74-4bdb-a466-c57e40cb25c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6647136a-d7bd-4b12-9283-28193953d2ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}