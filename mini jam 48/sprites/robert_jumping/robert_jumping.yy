{
    "id": "0298adce-06c2-4352-8d99-677eafceb291",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "robert_jumping",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 5,
    "bbox_right": 56,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a212c884-517b-457f-aedd-b6e81954c246",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0298adce-06c2-4352-8d99-677eafceb291",
            "compositeImage": {
                "id": "b3132ab0-debd-4bb1-9eb8-7eb5ed84bdce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a212c884-517b-457f-aedd-b6e81954c246",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1e76a61-0d01-4740-a1b5-98a9bb0d59d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a212c884-517b-457f-aedd-b6e81954c246",
                    "LayerId": "7855e0b3-37db-45c4-b94e-9aca4a5ad504"
                }
            ]
        },
        {
            "id": "9321c6d3-1920-451b-9dea-ff7cad94938c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0298adce-06c2-4352-8d99-677eafceb291",
            "compositeImage": {
                "id": "25b0dd2f-95b3-4b0d-b284-80649d1122eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9321c6d3-1920-451b-9dea-ff7cad94938c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "110be829-b1fc-4ac2-89b1-b43133bb5b66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9321c6d3-1920-451b-9dea-ff7cad94938c",
                    "LayerId": "7855e0b3-37db-45c4-b94e-9aca4a5ad504"
                }
            ]
        },
        {
            "id": "31daf6c7-19ac-4adc-be0b-293b9d5e7ff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0298adce-06c2-4352-8d99-677eafceb291",
            "compositeImage": {
                "id": "f1f98466-d4ba-49d9-bc99-09c42ecb785a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31daf6c7-19ac-4adc-be0b-293b9d5e7ff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "103833e4-ef99-49e5-a1ea-20a5b53b6e98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31daf6c7-19ac-4adc-be0b-293b9d5e7ff4",
                    "LayerId": "7855e0b3-37db-45c4-b94e-9aca4a5ad504"
                }
            ]
        },
        {
            "id": "5766f0f9-42b5-4cf4-a3a8-4a4359c761dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0298adce-06c2-4352-8d99-677eafceb291",
            "compositeImage": {
                "id": "7d666cb6-6263-4a2b-80e2-45a31c0312ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5766f0f9-42b5-4cf4-a3a8-4a4359c761dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e2040fe-f251-41c3-80c0-65e726f3fadb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5766f0f9-42b5-4cf4-a3a8-4a4359c761dc",
                    "LayerId": "7855e0b3-37db-45c4-b94e-9aca4a5ad504"
                }
            ]
        },
        {
            "id": "1dfcfd47-8481-49cc-93f3-611a9396fb7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0298adce-06c2-4352-8d99-677eafceb291",
            "compositeImage": {
                "id": "7cff9bf2-69d4-405d-955b-1b9d1795df22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dfcfd47-8481-49cc-93f3-611a9396fb7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bf70a97-a36d-4ad3-b9fe-b9b05735691e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dfcfd47-8481-49cc-93f3-611a9396fb7b",
                    "LayerId": "7855e0b3-37db-45c4-b94e-9aca4a5ad504"
                }
            ]
        },
        {
            "id": "ec181a35-c274-43bd-81b0-61bb37b70f30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0298adce-06c2-4352-8d99-677eafceb291",
            "compositeImage": {
                "id": "84a97b85-1e72-4f5e-b6f8-e76dacf787f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec181a35-c274-43bd-81b0-61bb37b70f30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aaa1170-3055-432a-85b7-2a0e508c83ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec181a35-c274-43bd-81b0-61bb37b70f30",
                    "LayerId": "7855e0b3-37db-45c4-b94e-9aca4a5ad504"
                }
            ]
        },
        {
            "id": "f940df63-b842-44d6-9acf-b24639e5e8f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0298adce-06c2-4352-8d99-677eafceb291",
            "compositeImage": {
                "id": "c37c7440-9c66-4d9b-b86d-650cb5f07eed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f940df63-b842-44d6-9acf-b24639e5e8f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8333251-1d66-46c8-b6d6-7d8b411e1795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f940df63-b842-44d6-9acf-b24639e5e8f5",
                    "LayerId": "7855e0b3-37db-45c4-b94e-9aca4a5ad504"
                }
            ]
        },
        {
            "id": "397ac7a5-f198-4b44-9c07-38b7beafda39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0298adce-06c2-4352-8d99-677eafceb291",
            "compositeImage": {
                "id": "34ccc510-328f-4d20-a319-f86760bcc251",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "397ac7a5-f198-4b44-9c07-38b7beafda39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "643627eb-df19-459d-95dc-2ca3c53d34ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "397ac7a5-f198-4b44-9c07-38b7beafda39",
                    "LayerId": "7855e0b3-37db-45c4-b94e-9aca4a5ad504"
                }
            ]
        },
        {
            "id": "fb0c0c52-d8a2-455d-9e66-18ebf6755a1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0298adce-06c2-4352-8d99-677eafceb291",
            "compositeImage": {
                "id": "712acff9-f9c7-4e7d-88c8-a187b58d796b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb0c0c52-d8a2-455d-9e66-18ebf6755a1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5a6fc3c-6f5a-499b-9c13-aa59a902338a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb0c0c52-d8a2-455d-9e66-18ebf6755a1c",
                    "LayerId": "7855e0b3-37db-45c4-b94e-9aca4a5ad504"
                }
            ]
        },
        {
            "id": "aaedcce0-78a2-466a-9972-6cbafc515909",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0298adce-06c2-4352-8d99-677eafceb291",
            "compositeImage": {
                "id": "60e97792-427a-41b9-9b5c-4a4efc6535f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaedcce0-78a2-466a-9972-6cbafc515909",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6145aa5-0fdd-4c2c-a720-eb93124376eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaedcce0-78a2-466a-9972-6cbafc515909",
                    "LayerId": "7855e0b3-37db-45c4-b94e-9aca4a5ad504"
                }
            ]
        },
        {
            "id": "83b81460-2b8d-4226-9625-7f8713426bcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0298adce-06c2-4352-8d99-677eafceb291",
            "compositeImage": {
                "id": "739175cf-0bab-4fd1-8cc0-5789540443a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83b81460-2b8d-4226-9625-7f8713426bcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6ad7c45-36c3-48e3-8181-5cddad1c6fca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83b81460-2b8d-4226-9625-7f8713426bcd",
                    "LayerId": "7855e0b3-37db-45c4-b94e-9aca4a5ad504"
                }
            ]
        },
        {
            "id": "24fcc5e3-8f71-41a0-b018-a667f165c76a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0298adce-06c2-4352-8d99-677eafceb291",
            "compositeImage": {
                "id": "9b844385-973d-48f6-9bbb-49d6e55103f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24fcc5e3-8f71-41a0-b018-a667f165c76a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35c97944-fcd2-4f12-8ed6-ac8d59710265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24fcc5e3-8f71-41a0-b018-a667f165c76a",
                    "LayerId": "7855e0b3-37db-45c4-b94e-9aca4a5ad504"
                }
            ]
        },
        {
            "id": "2a65f956-2085-4c65-a545-5bd01f553036",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0298adce-06c2-4352-8d99-677eafceb291",
            "compositeImage": {
                "id": "1028a432-13ba-4d22-a071-4a26bd97e797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a65f956-2085-4c65-a545-5bd01f553036",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bdcca58-f643-4fde-ab95-749ff1802dc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a65f956-2085-4c65-a545-5bd01f553036",
                    "LayerId": "7855e0b3-37db-45c4-b94e-9aca4a5ad504"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7855e0b3-37db-45c4-b94e-9aca4a5ad504",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0298adce-06c2-4352-8d99-677eafceb291",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}