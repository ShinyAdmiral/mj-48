{
    "id": "774f00fc-f2ef-445f-9748-c5c30c966529",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_bottom_right_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e2f5662-1162-4b18-85db-07a19bc3feb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "774f00fc-f2ef-445f-9748-c5c30c966529",
            "compositeImage": {
                "id": "9173a218-c7d3-4f74-ae74-14076074e3d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e2f5662-1162-4b18-85db-07a19bc3feb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3749479-644d-41df-b262-454bb1c4e76e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e2f5662-1162-4b18-85db-07a19bc3feb1",
                    "LayerId": "718ef9a1-fae9-441a-8691-6b5a6f2e9c0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "718ef9a1-fae9-441a-8691-6b5a6f2e9c0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "774f00fc-f2ef-445f-9748-c5c30c966529",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}