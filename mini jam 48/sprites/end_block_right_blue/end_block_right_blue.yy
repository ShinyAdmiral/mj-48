{
    "id": "b2c79651-b4c1-4c66-8329-76d83aaf854b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_right_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56765ecf-ae8d-4b11-a265-ebe937ec54a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2c79651-b4c1-4c66-8329-76d83aaf854b",
            "compositeImage": {
                "id": "3cc1cdf8-0261-47b1-a1aa-107a37170b61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56765ecf-ae8d-4b11-a265-ebe937ec54a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26b11734-15a2-4f7c-b1c5-651e56b74a9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56765ecf-ae8d-4b11-a265-ebe937ec54a5",
                    "LayerId": "e9b3b482-212b-4a9a-905b-ef4e92155bb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e9b3b482-212b-4a9a-905b-ef4e92155bb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2c79651-b4c1-4c66-8329-76d83aaf854b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}