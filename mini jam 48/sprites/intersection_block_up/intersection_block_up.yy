{
    "id": "b159d8ba-59ec-416e-b6ea-0e0aad828a3a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "intersection_block_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70ffb3db-21dd-45d4-918b-65e66227556f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b159d8ba-59ec-416e-b6ea-0e0aad828a3a",
            "compositeImage": {
                "id": "92c39c1f-3510-4b72-a3ee-d43c71d1731f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70ffb3db-21dd-45d4-918b-65e66227556f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb66c124-ee0a-4f70-ad34-dd31313dd538",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70ffb3db-21dd-45d4-918b-65e66227556f",
                    "LayerId": "0018fe97-1cf7-4bc0-8845-f7302cb03fb9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0018fe97-1cf7-4bc0-8845-f7302cb03fb9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b159d8ba-59ec-416e-b6ea-0e0aad828a3a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}