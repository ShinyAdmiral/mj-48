{
    "id": "61796dc4-c5aa-404d-b62f-71a8c81a51a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "MissionFailed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 15,
    "bbox_right": 304,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "748fa49d-7dcc-45fd-bd57-903b5623513f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61796dc4-c5aa-404d-b62f-71a8c81a51a1",
            "compositeImage": {
                "id": "06001602-9e70-45f5-8b35-e4bd6961fba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "748fa49d-7dcc-45fd-bd57-903b5623513f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d98e32e-8dde-4394-899d-ab9dce1be94c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "748fa49d-7dcc-45fd-bd57-903b5623513f",
                    "LayerId": "9c4e4298-59cc-417d-ac24-9eb13a216ce7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "9c4e4298-59cc-417d-ac24-9eb13a216ce7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61796dc4-c5aa-404d-b62f-71a8c81a51a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 75
}