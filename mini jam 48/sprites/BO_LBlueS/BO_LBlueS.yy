{
    "id": "9f4dedc2-b4a4-4db8-8349-bd3d3a46287a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "BO_LBlueS",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f57c1f9-e65c-4dff-aa82-d4155b12ea41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f4dedc2-b4a4-4db8-8349-bd3d3a46287a",
            "compositeImage": {
                "id": "648ffd75-1464-461a-b670-c0ac56fbc3f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f57c1f9-e65c-4dff-aa82-d4155b12ea41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9619ad3b-2896-4a7d-a402-74fdc82aac54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f57c1f9-e65c-4dff-aa82-d4155b12ea41",
                    "LayerId": "2cf5a40d-dd33-4775-8a2c-5b00be9b5a0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "2cf5a40d-dd33-4775-8a2c-5b00be9b5a0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f4dedc2-b4a4-4db8-8349-bd3d3a46287a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 0,
    "yorig": 0
}