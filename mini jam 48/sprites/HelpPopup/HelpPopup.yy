{
    "id": "263b8180-f73e-4f47-8677-bf16e45fbad7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "HelpPopup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 287,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b92cf785-0a9c-4cb7-84ca-76b5db449251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263b8180-f73e-4f47-8677-bf16e45fbad7",
            "compositeImage": {
                "id": "9899c3b3-4529-43f6-a3ea-8e855fdd1cf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b92cf785-0a9c-4cb7-84ca-76b5db449251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f7f9b75-bab9-4150-b519-15103a4cc2b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b92cf785-0a9c-4cb7-84ca-76b5db449251",
                    "LayerId": "fa72eb86-1360-4a9d-a705-0daf1b040b3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "fa72eb86-1360-4a9d-a705-0daf1b040b3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "263b8180-f73e-4f47-8677-bf16e45fbad7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 288,
    "xorig": 144,
    "yorig": 112
}