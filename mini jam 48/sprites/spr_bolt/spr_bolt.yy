{
    "id": "31474b22-9f6e-4ad1-967e-a57aa3289c8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bolt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 7,
    "bbox_right": 23,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e46c6650-7957-448e-86ef-06dcc2b28b54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31474b22-9f6e-4ad1-967e-a57aa3289c8a",
            "compositeImage": {
                "id": "b7c361f8-4ac3-40ee-acb7-7e0a54dd6b54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e46c6650-7957-448e-86ef-06dcc2b28b54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eea520a5-e2b6-452c-b7e1-5f064131094c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e46c6650-7957-448e-86ef-06dcc2b28b54",
                    "LayerId": "d5fa96d4-a769-4809-b224-5f37441beb2e"
                }
            ]
        },
        {
            "id": "a1d2ce40-cb64-48e5-9c25-dbc97418d3d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31474b22-9f6e-4ad1-967e-a57aa3289c8a",
            "compositeImage": {
                "id": "7649b037-9c82-4674-95dd-13cfacbd9a2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1d2ce40-cb64-48e5-9c25-dbc97418d3d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80045084-71dd-4594-8707-4213bd458fdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1d2ce40-cb64-48e5-9c25-dbc97418d3d0",
                    "LayerId": "d5fa96d4-a769-4809-b224-5f37441beb2e"
                }
            ]
        },
        {
            "id": "6f140e6d-999e-40d4-9ef5-5dd2c75c945c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31474b22-9f6e-4ad1-967e-a57aa3289c8a",
            "compositeImage": {
                "id": "bd899ab4-5cac-4c22-8ae1-8948708b410a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f140e6d-999e-40d4-9ef5-5dd2c75c945c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "105454dc-7bb0-4cdd-a565-52abf944726d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f140e6d-999e-40d4-9ef5-5dd2c75c945c",
                    "LayerId": "d5fa96d4-a769-4809-b224-5f37441beb2e"
                }
            ]
        },
        {
            "id": "68fd5a37-5cf3-4558-98a4-f82ba2850fe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31474b22-9f6e-4ad1-967e-a57aa3289c8a",
            "compositeImage": {
                "id": "4a79c2a2-fd9b-46ef-a5fe-30f6c61567cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68fd5a37-5cf3-4558-98a4-f82ba2850fe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beebdbf3-b35f-42a0-9025-7aed4974d7c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68fd5a37-5cf3-4558-98a4-f82ba2850fe5",
                    "LayerId": "d5fa96d4-a769-4809-b224-5f37441beb2e"
                }
            ]
        },
        {
            "id": "ae1a4cb0-c9aa-4813-bb4e-09a33ca0d6e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31474b22-9f6e-4ad1-967e-a57aa3289c8a",
            "compositeImage": {
                "id": "9d2c5614-4aa1-43d8-84f4-7b080f77fbaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae1a4cb0-c9aa-4813-bb4e-09a33ca0d6e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac0df01e-2e72-4be0-8605-4679e9eac373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae1a4cb0-c9aa-4813-bb4e-09a33ca0d6e4",
                    "LayerId": "d5fa96d4-a769-4809-b224-5f37441beb2e"
                }
            ]
        },
        {
            "id": "afe953b5-0e97-4db1-be77-c59dc16e4445",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31474b22-9f6e-4ad1-967e-a57aa3289c8a",
            "compositeImage": {
                "id": "2533894b-755e-43c7-9fb1-ebe8ceb102d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afe953b5-0e97-4db1-be77-c59dc16e4445",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e21d8de-285c-400e-9275-5a922a29f6ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afe953b5-0e97-4db1-be77-c59dc16e4445",
                    "LayerId": "d5fa96d4-a769-4809-b224-5f37441beb2e"
                }
            ]
        },
        {
            "id": "f7174a32-26d6-4204-8e80-2cd5c9ad4816",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31474b22-9f6e-4ad1-967e-a57aa3289c8a",
            "compositeImage": {
                "id": "11bb7201-208a-4ffa-9e56-f3e69ac00f3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7174a32-26d6-4204-8e80-2cd5c9ad4816",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77162791-4d59-4e16-8b86-5f5f07b7085c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7174a32-26d6-4204-8e80-2cd5c9ad4816",
                    "LayerId": "d5fa96d4-a769-4809-b224-5f37441beb2e"
                }
            ]
        },
        {
            "id": "2f0bf3ee-eeac-4560-af59-fa8ca7475fb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31474b22-9f6e-4ad1-967e-a57aa3289c8a",
            "compositeImage": {
                "id": "3c40a423-e6de-41d9-8011-f98e6098cf27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f0bf3ee-eeac-4560-af59-fa8ca7475fb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f09306e-0d14-46a0-85fa-20551cf514ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f0bf3ee-eeac-4560-af59-fa8ca7475fb9",
                    "LayerId": "d5fa96d4-a769-4809-b224-5f37441beb2e"
                }
            ]
        },
        {
            "id": "9fb3fc18-73b3-42a1-87db-e32c25163311",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31474b22-9f6e-4ad1-967e-a57aa3289c8a",
            "compositeImage": {
                "id": "2d7cec3b-f452-4a28-91f2-4b3ba62444f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fb3fc18-73b3-42a1-87db-e32c25163311",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02e60f94-607b-4c13-b641-2e919caaf5e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fb3fc18-73b3-42a1-87db-e32c25163311",
                    "LayerId": "d5fa96d4-a769-4809-b224-5f37441beb2e"
                }
            ]
        },
        {
            "id": "0bac5d72-492d-46c2-8b67-1dad49289f8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31474b22-9f6e-4ad1-967e-a57aa3289c8a",
            "compositeImage": {
                "id": "3d9ebec5-f7db-4470-bc99-442fe222c1b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bac5d72-492d-46c2-8b67-1dad49289f8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ff56df5-7f6d-47f6-a9c6-ca3abd525529",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bac5d72-492d-46c2-8b67-1dad49289f8e",
                    "LayerId": "d5fa96d4-a769-4809-b224-5f37441beb2e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "d5fa96d4-a769-4809-b224-5f37441beb2e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31474b22-9f6e-4ad1-967e-a57aa3289c8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 12
}