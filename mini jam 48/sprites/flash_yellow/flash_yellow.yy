{
    "id": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flash_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3de1cafb-3297-4ead-8d04-d3e11106fda5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "compositeImage": {
                "id": "04517f83-ffba-4cb9-8a5e-9e8a29306496",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3de1cafb-3297-4ead-8d04-d3e11106fda5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c39ca8d0-d352-4ea1-b57c-1099e5ea5012",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3de1cafb-3297-4ead-8d04-d3e11106fda5",
                    "LayerId": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811"
                }
            ]
        },
        {
            "id": "c12ac3ea-e83b-4e89-9a36-955a154864f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "compositeImage": {
                "id": "a948b1e5-dfe7-489d-9eae-de5b890ddb8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c12ac3ea-e83b-4e89-9a36-955a154864f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1fe39ac-2594-4e5a-980c-a82f89121083",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c12ac3ea-e83b-4e89-9a36-955a154864f1",
                    "LayerId": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811"
                }
            ]
        },
        {
            "id": "5ad381e9-d13c-40db-bf1f-4a47127584d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "compositeImage": {
                "id": "ad59b395-74d0-4042-aa0f-f75ada96cd25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ad381e9-d13c-40db-bf1f-4a47127584d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba7b1ebf-d3ed-4af9-bf90-47edbbb3a372",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ad381e9-d13c-40db-bf1f-4a47127584d1",
                    "LayerId": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811"
                }
            ]
        },
        {
            "id": "cd63c097-9ee3-4e38-bec1-d1012858c558",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "compositeImage": {
                "id": "0efd419c-bdf6-4028-b93d-df7269cbfd7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd63c097-9ee3-4e38-bec1-d1012858c558",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f02e70d4-be39-473b-97c9-ed3b67c01fd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd63c097-9ee3-4e38-bec1-d1012858c558",
                    "LayerId": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811"
                }
            ]
        },
        {
            "id": "2f7a7338-02a7-4592-b47e-607a3088ff5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "compositeImage": {
                "id": "a07c6bc4-cefd-4f24-a41e-a12eca0b75bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f7a7338-02a7-4592-b47e-607a3088ff5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6ef5e93-0fae-4d64-9b73-544136a2cc6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f7a7338-02a7-4592-b47e-607a3088ff5c",
                    "LayerId": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811"
                }
            ]
        },
        {
            "id": "4e846698-afa6-4fa5-a0c8-a95f7cc3a675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "compositeImage": {
                "id": "2b939f5b-930b-48a9-9b2c-54cc9048c59b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e846698-afa6-4fa5-a0c8-a95f7cc3a675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faca78c1-a898-423e-8d5f-5dcb5a165390",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e846698-afa6-4fa5-a0c8-a95f7cc3a675",
                    "LayerId": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811"
                }
            ]
        },
        {
            "id": "bd14f7ec-8a2f-4fa1-9a0d-fb1e309f51cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "compositeImage": {
                "id": "080efa03-cd1f-42bc-b6fa-8dcc4fa83959",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd14f7ec-8a2f-4fa1-9a0d-fb1e309f51cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19a2383f-11a5-487e-b057-ccb13c9a9bdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd14f7ec-8a2f-4fa1-9a0d-fb1e309f51cb",
                    "LayerId": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811"
                }
            ]
        },
        {
            "id": "6dcaf08d-6886-41d3-b5a5-2faa3d841c9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "compositeImage": {
                "id": "cb336615-6b91-4eea-bd90-89b3af7adadd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dcaf08d-6886-41d3-b5a5-2faa3d841c9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a4c93a0-e568-46fa-989f-698e6da14426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dcaf08d-6886-41d3-b5a5-2faa3d841c9a",
                    "LayerId": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811"
                }
            ]
        },
        {
            "id": "2322100f-7523-4faa-bce3-86d9e8e57f20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "compositeImage": {
                "id": "ba83e987-2273-4e39-98c7-0ccd889da675",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2322100f-7523-4faa-bce3-86d9e8e57f20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "960ff27f-1b78-44bc-820c-888899d31ac9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2322100f-7523-4faa-bce3-86d9e8e57f20",
                    "LayerId": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811"
                }
            ]
        },
        {
            "id": "a1793fbb-3242-4822-b8ae-e00ef0e2988e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "compositeImage": {
                "id": "083b5480-a270-4b87-ac90-f992e8deca00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1793fbb-3242-4822-b8ae-e00ef0e2988e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aefcd5c4-e136-4660-884a-6246e81f5fd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1793fbb-3242-4822-b8ae-e00ef0e2988e",
                    "LayerId": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811"
                }
            ]
        },
        {
            "id": "f407eaf1-a2c6-42bd-84d4-140a2217a68c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "compositeImage": {
                "id": "f409fb42-eba5-4bdd-a0de-64e535fb4d0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f407eaf1-a2c6-42bd-84d4-140a2217a68c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fff9bd86-39ee-4e31-933b-48c2b8f24f27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f407eaf1-a2c6-42bd-84d4-140a2217a68c",
                    "LayerId": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811"
                }
            ]
        },
        {
            "id": "a27509b1-64d7-4d57-a747-5e378d7aef7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "compositeImage": {
                "id": "77542d15-7b58-4ad1-a960-c3aee6225578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a27509b1-64d7-4d57-a747-5e378d7aef7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f05c676-fbe0-4b68-890e-91a686a87fcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a27509b1-64d7-4d57-a747-5e378d7aef7b",
                    "LayerId": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811"
                }
            ]
        },
        {
            "id": "0c8d81bc-dbcc-4775-a8e2-7c71a9cfe0ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "compositeImage": {
                "id": "6e839979-9289-4460-a50f-ef60cfb4ffb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c8d81bc-dbcc-4775-a8e2-7c71a9cfe0ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee9aff49-1fac-461e-924b-ed06f6df0c71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c8d81bc-dbcc-4775-a8e2-7c71a9cfe0ae",
                    "LayerId": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811"
                }
            ]
        },
        {
            "id": "1813fa55-0457-4c25-84e0-9a8ce84b13d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "compositeImage": {
                "id": "e9ce6e8e-4d70-4989-9e06-0de74a41cad4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1813fa55-0457-4c25-84e0-9a8ce84b13d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdf04857-8f4e-4d98-8930-a59dbd180666",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1813fa55-0457-4c25-84e0-9a8ce84b13d1",
                    "LayerId": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "b5c4e559-ee8b-4120-9b0b-33ec3b6ca811",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 21,
    "yorig": 20
}