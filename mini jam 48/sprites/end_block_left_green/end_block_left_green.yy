{
    "id": "5ca03106-a3b1-4e48-8a94-7dd5131a960f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_left_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac27a000-b87d-403c-81d0-052344d31efc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ca03106-a3b1-4e48-8a94-7dd5131a960f",
            "compositeImage": {
                "id": "57b1e2e8-9d53-4fb8-b830-8447bba7fbc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac27a000-b87d-403c-81d0-052344d31efc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe54c2d3-ce77-49cb-b61b-6396011d7313",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac27a000-b87d-403c-81d0-052344d31efc",
                    "LayerId": "c6feec0c-7b03-4218-b116-3a8db003a699"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c6feec0c-7b03-4218-b116-3a8db003a699",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ca03106-a3b1-4e48-8a94-7dd5131a960f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}