{
    "id": "7ac6b604-89ed-4951-bcaa-f39c0bd4b6ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_up_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e71c85d-b91f-4e97-84c1-d0de4aef8a26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ac6b604-89ed-4951-bcaa-f39c0bd4b6ad",
            "compositeImage": {
                "id": "27426170-c6ce-487e-976c-121e83cd4005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e71c85d-b91f-4e97-84c1-d0de4aef8a26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1628b69-f22c-4791-8101-f4900c635acd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e71c85d-b91f-4e97-84c1-d0de4aef8a26",
                    "LayerId": "b35f4bc7-922d-4e7e-a1fd-6a1d410fa2de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b35f4bc7-922d-4e7e-a1fd-6a1d410fa2de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ac6b604-89ed-4951-bcaa-f39c0bd4b6ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}