{
    "id": "63e3fc69-8b1e-4ae2-8171-47f2307679cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "intersection_block_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85e5c180-5dab-4d1c-91d8-61e87c05efd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63e3fc69-8b1e-4ae2-8171-47f2307679cf",
            "compositeImage": {
                "id": "eac84de6-e2da-407e-bccb-ceacb8573634",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85e5c180-5dab-4d1c-91d8-61e87c05efd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8290e815-17cb-44a8-80d4-1866f4c9cc5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85e5c180-5dab-4d1c-91d8-61e87c05efd7",
                    "LayerId": "92b88d98-8742-4ffd-a6d5-f2d7535a809b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "92b88d98-8742-4ffd-a6d5-f2d7535a809b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63e3fc69-8b1e-4ae2-8171-47f2307679cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}