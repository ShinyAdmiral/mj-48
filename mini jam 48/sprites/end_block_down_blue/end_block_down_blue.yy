{
    "id": "50ee2952-dc31-42b4-a4ae-39e1980ba83f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_down_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a574dad-7365-4e9f-b4a7-ebc0cb495db7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50ee2952-dc31-42b4-a4ae-39e1980ba83f",
            "compositeImage": {
                "id": "1c6d9560-e70a-4e12-b4a1-98020fcf5195",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a574dad-7365-4e9f-b4a7-ebc0cb495db7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "533c66ee-2013-43eb-bdf1-4fb903fa74e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a574dad-7365-4e9f-b4a7-ebc0cb495db7",
                    "LayerId": "9eef6591-57ea-495f-8845-2ebde85a1cac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9eef6591-57ea-495f-8845-2ebde85a1cac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50ee2952-dc31-42b4-a4ae-39e1980ba83f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}