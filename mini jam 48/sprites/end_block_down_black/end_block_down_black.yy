{
    "id": "ee3f7549-4632-4680-baa8-a90d286e1eaa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_down_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "926565bd-d936-4333-b8f3-53323f2de071",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee3f7549-4632-4680-baa8-a90d286e1eaa",
            "compositeImage": {
                "id": "47a4f892-3740-4504-af9e-bfebd3352d1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "926565bd-d936-4333-b8f3-53323f2de071",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b50b86ee-818b-456f-abf5-ea7cbc6ee531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "926565bd-d936-4333-b8f3-53323f2de071",
                    "LayerId": "ba4b74ca-22ba-4cc8-9d68-1be469c695cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ba4b74ca-22ba-4cc8-9d68-1be469c695cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee3f7549-4632-4680-baa8-a90d286e1eaa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}