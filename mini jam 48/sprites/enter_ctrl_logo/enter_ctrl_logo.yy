{
    "id": "3ea6a681-233a-4e6c-afd5-8812cd423e0f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enter_ctrl_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 289,
    "bbox_left": 0,
    "bbox_right": 339,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b85b009-253e-4904-8585-424e5a0dac9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ea6a681-233a-4e6c-afd5-8812cd423e0f",
            "compositeImage": {
                "id": "af9cf1ff-5102-40cd-8b12-2c1ae7dd9ad7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b85b009-253e-4904-8585-424e5a0dac9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30fc3ff4-d08c-4c63-8737-40a5f42c993c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b85b009-253e-4904-8585-424e5a0dac9a",
                    "LayerId": "e4243a26-7896-44e1-bb5a-3a05e8632b04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 290,
    "layers": [
        {
            "id": "e4243a26-7896-44e1-bb5a-3a05e8632b04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ea6a681-233a-4e6c-afd5-8812cd423e0f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 340,
    "xorig": 0,
    "yorig": 0
}