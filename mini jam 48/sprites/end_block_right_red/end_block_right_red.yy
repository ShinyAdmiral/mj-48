{
    "id": "e540df36-b3ef-4db4-a0db-bf8727d7948c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_right_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b20dbc0-d678-450b-8231-f1bf362f68e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e540df36-b3ef-4db4-a0db-bf8727d7948c",
            "compositeImage": {
                "id": "6234021f-8346-492d-bde2-c8d27046945e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b20dbc0-d678-450b-8231-f1bf362f68e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f17eacd0-f90f-414a-bde7-101d9100bc54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b20dbc0-d678-450b-8231-f1bf362f68e1",
                    "LayerId": "b76ba364-8f69-4880-ad3a-b8836785b956"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b76ba364-8f69-4880-ad3a-b8836785b956",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e540df36-b3ef-4db4-a0db-bf8727d7948c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}