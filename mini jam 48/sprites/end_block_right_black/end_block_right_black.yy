{
    "id": "f5f8095b-244e-4202-9f0b-a9514b0fc4c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_right_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea4f2295-afbb-4402-87ba-6ad622a7b18c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5f8095b-244e-4202-9f0b-a9514b0fc4c9",
            "compositeImage": {
                "id": "7ff4b1de-ea82-4412-b567-b6b07b8f52dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea4f2295-afbb-4402-87ba-6ad622a7b18c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2b0fa33-fe2f-4d96-a673-59cdd95095a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea4f2295-afbb-4402-87ba-6ad622a7b18c",
                    "LayerId": "57cb527a-5575-4537-9a18-b40250082bf2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "57cb527a-5575-4537-9a18-b40250082bf2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5f8095b-244e-4202-9f0b-a9514b0fc4c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}