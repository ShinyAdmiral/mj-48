{
    "id": "93d1e136-5668-4a4a-9b86-724ea32d88bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "UIBlocks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 501,
    "bbox_left": 1,
    "bbox_right": 92,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a87d792e-e980-41ac-9a39-89cbf6195884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93d1e136-5668-4a4a-9b86-724ea32d88bb",
            "compositeImage": {
                "id": "a8f11515-9bf6-4f3b-9ef7-3001e17f7e90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a87d792e-e980-41ac-9a39-89cbf6195884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ced3a94e-90da-467b-a6b8-2d868411adda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a87d792e-e980-41ac-9a39-89cbf6195884",
                    "LayerId": "143a9f46-8fa7-47b8-8044-ef34d7184bf8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 503,
    "layers": [
        {
            "id": "143a9f46-8fa7-47b8-8044-ef34d7184bf8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93d1e136-5668-4a4a-9b86-724ea32d88bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 94,
    "xorig": 0,
    "yorig": 0
}