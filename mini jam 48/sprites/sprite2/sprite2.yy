{
    "id": "204330e0-10eb-4c9f-bd85-812eb94ff3a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9672de6c-f3e3-4eb7-bb95-1b21b7be7124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "204330e0-10eb-4c9f-bd85-812eb94ff3a3",
            "compositeImage": {
                "id": "815f814f-d4c2-4d69-8159-b3102b8e7bd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9672de6c-f3e3-4eb7-bb95-1b21b7be7124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef17543c-35fc-459b-a1c8-995c5f1783fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9672de6c-f3e3-4eb7-bb95-1b21b7be7124",
                    "LayerId": "f32bfbbc-d99a-444b-a3b2-adc099037ce3"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 32,
    "layers": [
        {
            "id": "f32bfbbc-d99a-444b-a3b2-adc099037ce3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "204330e0-10eb-4c9f-bd85-812eb94ff3a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}