{
    "id": "b6274efb-3128-4143-8c35-7b63ed3c9a4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85f3dde5-705a-4917-ab41-cdef5c15e6bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6274efb-3128-4143-8c35-7b63ed3c9a4d",
            "compositeImage": {
                "id": "ffb2177a-ae47-4fcd-894e-21e225ce076e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85f3dde5-705a-4917-ab41-cdef5c15e6bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "665b7144-db8c-4551-897b-5e82bfbf6577",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85f3dde5-705a-4917-ab41-cdef5c15e6bb",
                    "LayerId": "9d7bb8f9-145f-45f5-8095-1eefb791b204"
                },
                {
                    "id": "ad166670-9915-4339-9976-5bd8520618f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85f3dde5-705a-4917-ab41-cdef5c15e6bb",
                    "LayerId": "447322d8-c7b1-4749-bba1-24ff33021c6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "447322d8-c7b1-4749-bba1-24ff33021c6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6274efb-3128-4143-8c35-7b63ed3c9a4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "9d7bb8f9-145f-45f5-8095-1eefb791b204",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6274efb-3128-4143-8c35-7b63ed3c9a4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 8,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 319,
    "yorig": 511
}