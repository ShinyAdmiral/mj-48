{
    "id": "f45f71d6-9e9d-4298-a27d-0f924b1e6629",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "nebula_profile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 74,
    "bbox_left": 0,
    "bbox_right": 81,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ee943db-61e2-4310-a342-116eb870687d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f45f71d6-9e9d-4298-a27d-0f924b1e6629",
            "compositeImage": {
                "id": "1add5f0a-e0a6-405f-9306-2e66801341a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ee943db-61e2-4310-a342-116eb870687d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86890599-092c-4b89-9876-d8a0cfcaf69e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ee943db-61e2-4310-a342-116eb870687d",
                    "LayerId": "48ba5bf5-00a0-4962-a575-56986736e4d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 75,
    "layers": [
        {
            "id": "48ba5bf5-00a0-4962-a575-56986736e4d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f45f71d6-9e9d-4298-a27d-0f924b1e6629",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 82,
    "xorig": 0,
    "yorig": 0
}