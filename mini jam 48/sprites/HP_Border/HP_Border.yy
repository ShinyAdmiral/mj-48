{
    "id": "f084849f-12ab-4519-9d73-834e7a798bf6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "HP_Border",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 501,
    "bbox_left": 1,
    "bbox_right": 92,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fc5d1df-4f9c-4013-b3c5-504b8dedd549",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f084849f-12ab-4519-9d73-834e7a798bf6",
            "compositeImage": {
                "id": "b3673b54-806e-49d6-957e-7477d9fe88d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fc5d1df-4f9c-4013-b3c5-504b8dedd549",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c84809ec-652c-44a4-8165-707d5ca269be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fc5d1df-4f9c-4013-b3c5-504b8dedd549",
                    "LayerId": "6f2ebcab-6671-4a6b-a34a-1e04e1071f8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 503,
    "layers": [
        {
            "id": "6f2ebcab-6671-4a6b-a34a-1e04e1071f8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f084849f-12ab-4519-9d73-834e7a798bf6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 94,
    "xorig": 0,
    "yorig": 0
}