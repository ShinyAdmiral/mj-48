{
    "id": "7b7342cf-f615-423f-901d-19cdb7c0361a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_left_brown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9ec8920-ca7c-4429-b732-c71525a810ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b7342cf-f615-423f-901d-19cdb7c0361a",
            "compositeImage": {
                "id": "f180ee4c-70d4-4466-afec-d509e46d9116",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9ec8920-ca7c-4429-b732-c71525a810ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "515ee046-2937-4cb1-9e57-8d26dfeef653",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9ec8920-ca7c-4429-b732-c71525a810ac",
                    "LayerId": "4bd0f9c4-0949-47aa-818b-db8d1f22966e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4bd0f9c4-0949-47aa-818b-db8d1f22966e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b7342cf-f615-423f-901d-19cdb7c0361a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}