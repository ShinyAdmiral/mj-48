{
    "id": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flash_brown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49da9876-848c-4f76-96f6-325f30c4f71c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "2b9c0160-c1ee-4131-affe-823f2728887f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49da9876-848c-4f76-96f6-325f30c4f71c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10aa79cb-69a1-4c55-8cb1-50b7b9da1a35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49da9876-848c-4f76-96f6-325f30c4f71c",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "26e1d999-33a3-4d3f-ac6c-70069209b4f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "76981c30-bff3-443d-8dc4-38d2827a88fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26e1d999-33a3-4d3f-ac6c-70069209b4f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0511aa9c-2a47-4e37-aefa-66ec2ae98c37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26e1d999-33a3-4d3f-ac6c-70069209b4f6",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "b78c11f9-6265-43ef-8abe-fe23ef623f59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "0eb3b7d1-2781-4107-a3d8-601229400a18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b78c11f9-6265-43ef-8abe-fe23ef623f59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bc1708d-59c4-4e51-abe8-22d3f0e71043",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b78c11f9-6265-43ef-8abe-fe23ef623f59",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "fc91c080-26cd-4e2f-a7c1-523ffecb408c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "18cd7bf8-2ad6-42d0-8ff9-0a404064eb7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc91c080-26cd-4e2f-a7c1-523ffecb408c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb9a2687-e093-4809-a6f1-46ce2cd44564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc91c080-26cd-4e2f-a7c1-523ffecb408c",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "5d416476-6576-49d8-8a2b-cadb14e89348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "8f64adde-1333-4112-bcaa-028a2108f27a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d416476-6576-49d8-8a2b-cadb14e89348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35aa874a-4a54-4255-b6c7-0ebfb44c4b6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d416476-6576-49d8-8a2b-cadb14e89348",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "e63bf0e5-729f-435e-bf3a-5e47cb78b519",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "6cecc7fb-bc07-4727-a6ab-2b64554b16d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e63bf0e5-729f-435e-bf3a-5e47cb78b519",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c371f7c2-82eb-454f-81e4-b7f570a830e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e63bf0e5-729f-435e-bf3a-5e47cb78b519",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "005146ba-f2df-4106-bf8d-628331291b4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "9f4dad38-754f-45ae-8b10-201438d59dbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "005146ba-f2df-4106-bf8d-628331291b4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8891223-aede-4bb6-a748-4cf2f361a13d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "005146ba-f2df-4106-bf8d-628331291b4d",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "0e98cc6b-395a-4df2-8cb1-5d65ed1e0e95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "fb55c133-fc21-4338-a810-0ca70128b404",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e98cc6b-395a-4df2-8cb1-5d65ed1e0e95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be4cee03-dc34-4057-b682-9ff09c403048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e98cc6b-395a-4df2-8cb1-5d65ed1e0e95",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "3f587652-6c18-4951-a541-c077d3ab1074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "af1ff12a-6ad9-4945-a6a8-f5baa6378d2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f587652-6c18-4951-a541-c077d3ab1074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f73f4116-d387-4f88-9fea-6c010681dff9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f587652-6c18-4951-a541-c077d3ab1074",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "5b30b11a-3ff9-4e7b-a68e-bd9724638179",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "a3d84a79-88c8-4a23-949e-1825c842423b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b30b11a-3ff9-4e7b-a68e-bd9724638179",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50219e8f-4b29-4ce4-afbe-db04058c7b73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b30b11a-3ff9-4e7b-a68e-bd9724638179",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "88065fc0-c975-4e25-b65b-02250c21aa40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "3b9fe0b9-8a56-4c8f-8afb-938b5b3cf540",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88065fc0-c975-4e25-b65b-02250c21aa40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0561508b-1c01-4bd6-83c2-aec1ee1269de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88065fc0-c975-4e25-b65b-02250c21aa40",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "22a2cdbd-039f-4168-87f6-98986ed967e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "d28f8129-6a03-42e8-a589-fc6af1d8c568",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22a2cdbd-039f-4168-87f6-98986ed967e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0046a21c-a0df-41b9-92a3-b9234a4ec22c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22a2cdbd-039f-4168-87f6-98986ed967e1",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "25841f11-4b24-4604-8fe0-d47f7ba4b90b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "79f3828e-3ac2-46c1-adab-238fab31e0d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25841f11-4b24-4604-8fe0-d47f7ba4b90b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "379ae081-b933-425c-9661-61f19d7da361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25841f11-4b24-4604-8fe0-d47f7ba4b90b",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "a69aa87d-1a61-433e-a5cf-334dccc8bb4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "95c6293d-6c53-4371-8377-0b0319548984",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a69aa87d-1a61-433e-a5cf-334dccc8bb4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8490836e-1b4e-4e97-b79a-77d3d79b9022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a69aa87d-1a61-433e-a5cf-334dccc8bb4a",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "70b249ce-9f75-40e9-a300-9ad80c62431d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "de31e364-4b83-4ab1-af9b-597b2c5e2a5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70b249ce-9f75-40e9-a300-9ad80c62431d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a50516f0-5756-4e48-8c39-f12c5727dd73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70b249ce-9f75-40e9-a300-9ad80c62431d",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "acb28a50-a50a-43db-9b12-ba3234c227fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "8c3039bf-dfe3-4e1a-84a6-4f1285cfca5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acb28a50-a50a-43db-9b12-ba3234c227fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92d688a9-adad-4fd8-878f-7b835830677d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acb28a50-a50a-43db-9b12-ba3234c227fb",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "1d225593-dbd7-48c6-86c1-ec80a5f26be0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "75323a14-4356-4fb9-9e2c-c5c4c17d5edf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d225593-dbd7-48c6-86c1-ec80a5f26be0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdb06afd-8261-4229-a26a-bb20626862db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d225593-dbd7-48c6-86c1-ec80a5f26be0",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        },
        {
            "id": "0c71e0c8-971e-4bb1-a12b-c57830a8811d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "compositeImage": {
                "id": "8e189dda-77b7-42ed-b085-923ea1bfae07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c71e0c8-971e-4bb1-a12b-c57830a8811d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5917648f-b430-4c0f-bd05-851460da5b05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c71e0c8-971e-4bb1-a12b-c57830a8811d",
                    "LayerId": "08c5a247-9c2c-4994-8053-31f4dd762f36"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "08c5a247-9c2c-4994-8053-31f4dd762f36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 21,
    "yorig": 20
}