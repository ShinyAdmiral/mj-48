{
    "id": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "robert_dies",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 111,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d562afb-12f0-4599-aa0a-1405dbda8336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "ab21abbd-936d-4a34-8cf2-74ecf70032db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d562afb-12f0-4599-aa0a-1405dbda8336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f7919c7-7b5f-47c0-8dfe-adc35b01b2b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d562afb-12f0-4599-aa0a-1405dbda8336",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "d21616f0-a9c9-4c69-88b6-ab52c8460788",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "6b8f5d81-8eee-4700-99b8-731046bbb1c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d21616f0-a9c9-4c69-88b6-ab52c8460788",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0f579fb-d2ce-4e17-897a-0b96f6ff342f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d21616f0-a9c9-4c69-88b6-ab52c8460788",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "04c6641a-8097-4bb2-875d-e71b21ef640e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "4ae82ad9-1d9c-4fec-a63b-4e814f62bcfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04c6641a-8097-4bb2-875d-e71b21ef640e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e397b739-687c-4481-83d9-86c6e24d5039",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04c6641a-8097-4bb2-875d-e71b21ef640e",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "8c6e0d67-0c3e-4f6f-a1b5-c79438e2de75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "0be5f42a-93d2-4cd6-b58b-443b06c12649",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c6e0d67-0c3e-4f6f-a1b5-c79438e2de75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73963048-d65f-46b5-8b59-5f0c46886a58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c6e0d67-0c3e-4f6f-a1b5-c79438e2de75",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "c9ee29a9-87e5-41a9-ba4f-28901dc89b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "0978726a-52dc-4274-b343-231470c5c2b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9ee29a9-87e5-41a9-ba4f-28901dc89b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e559436-888f-452c-9585-33c8a3ba8a15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9ee29a9-87e5-41a9-ba4f-28901dc89b5c",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "fd1fa484-cb9e-4e81-8c5d-bd1979c978a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "c3c9d6a8-cee9-4ee6-988a-7fbce4149352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd1fa484-cb9e-4e81-8c5d-bd1979c978a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28d1c37e-6ad1-40b8-914c-bd9ff3a7cd0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd1fa484-cb9e-4e81-8c5d-bd1979c978a9",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "9affedec-5232-4802-a426-87f57b166c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "0e5aaa17-2e48-4b8b-ae6a-92dc1b2da7e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9affedec-5232-4802-a426-87f57b166c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc2692d9-e5c6-41f1-9a12-4add9a4979ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9affedec-5232-4802-a426-87f57b166c56",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "24b57f77-cb35-4316-bf32-86e442a2cc18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "d23e7c38-4a42-406b-a5c2-a9c1bd4ea11f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24b57f77-cb35-4316-bf32-86e442a2cc18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66c07f4e-1270-4f2a-89ad-d1a460e6810c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24b57f77-cb35-4316-bf32-86e442a2cc18",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "e6a6eade-7a26-4fa5-9d18-a7bbd42862a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "6d3e9da1-136f-4708-9abf-2cda0e99de48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6a6eade-7a26-4fa5-9d18-a7bbd42862a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e37fca1a-ad2a-4851-9df4-d15f80bf484e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6a6eade-7a26-4fa5-9d18-a7bbd42862a9",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "32555a80-010e-4848-baed-5785dcdaa238",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "1e61870c-e11a-4243-83db-6a2660903b5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32555a80-010e-4848-baed-5785dcdaa238",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9592348d-35f0-4bff-ae26-c0e5bbfb6931",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32555a80-010e-4848-baed-5785dcdaa238",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "276ddb37-5a63-4a6f-8895-bff87887d870",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "448a1874-19cb-4f59-afa9-b37825489037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "276ddb37-5a63-4a6f-8895-bff87887d870",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98fd94e3-207d-447e-94d3-86d78523a8b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "276ddb37-5a63-4a6f-8895-bff87887d870",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "1714a615-efd1-4d70-a2d1-228ba5a94144",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "c43b6707-f131-4181-a271-95b00d2eff8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1714a615-efd1-4d70-a2d1-228ba5a94144",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de5aff84-ad76-487f-94f7-7e76699ac382",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1714a615-efd1-4d70-a2d1-228ba5a94144",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "c73b9017-3263-4f57-81f9-5b7d53fac206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "c6da5f8b-2450-4b10-9d10-bddb6cc95abb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c73b9017-3263-4f57-81f9-5b7d53fac206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60253fa2-7639-4fc7-9a22-7c44bdf03ba6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c73b9017-3263-4f57-81f9-5b7d53fac206",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "131aa12d-dd47-4e1d-a789-077afa09ea06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "5147a1d5-3f13-48c8-98ca-37c905ccbd28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "131aa12d-dd47-4e1d-a789-077afa09ea06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a3693cf-14db-47b1-a187-0d1eb9e53ef6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "131aa12d-dd47-4e1d-a789-077afa09ea06",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "d4d08b62-7d47-44d6-8122-482b5857a698",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "6de42abc-eaa7-4c8e-8227-268a02fc2238",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4d08b62-7d47-44d6-8122-482b5857a698",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd48f8ab-3344-478e-91fa-aacd2778c250",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4d08b62-7d47-44d6-8122-482b5857a698",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "628817da-d7f9-40fd-a554-2ccbda9873d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "3a7df5e3-f6e4-40ac-a888-0681302a060f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "628817da-d7f9-40fd-a554-2ccbda9873d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ff3a65d-a9c3-4c58-b91c-c189058acc53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "628817da-d7f9-40fd-a554-2ccbda9873d4",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "ef07ca9f-ecc1-41d8-b56c-1fe2c8c459bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "7483558a-a10e-40e1-a8f6-217d41b80c00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef07ca9f-ecc1-41d8-b56c-1fe2c8c459bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1153510-5283-4c96-9973-9e9c8cb38475",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef07ca9f-ecc1-41d8-b56c-1fe2c8c459bd",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "755afbe7-1c4b-4c24-8f9f-c6e6f6d27f92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "b3f424f5-6176-4617-9d31-c4ae4bb35da8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "755afbe7-1c4b-4c24-8f9f-c6e6f6d27f92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c42a89a0-ceb4-4909-8820-36b817382199",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "755afbe7-1c4b-4c24-8f9f-c6e6f6d27f92",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "8667b21b-12ea-4c0f-a211-9a3bc5d69f5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "cd0ff2d3-82e7-4556-b6f5-b68945596fd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8667b21b-12ea-4c0f-a211-9a3bc5d69f5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c63db8ba-c441-408f-90e7-81d20bbde986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8667b21b-12ea-4c0f-a211-9a3bc5d69f5a",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "bcca11bc-d19f-4bd6-91cb-4fe97315c27c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "0c4242e8-9067-498f-83db-f607d34b44f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcca11bc-d19f-4bd6-91cb-4fe97315c27c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9ecf6e5-68d0-47b3-9de4-a95336728215",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcca11bc-d19f-4bd6-91cb-4fe97315c27c",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "7b864b7b-6bad-41ad-a985-4de8d15554a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "3854ee88-344e-40e9-9a2c-1ceeea3c3147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b864b7b-6bad-41ad-a985-4de8d15554a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d3df745-47da-4997-a5c1-a319b0eca76d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b864b7b-6bad-41ad-a985-4de8d15554a9",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "65bf327e-3fe1-48e8-8b72-3ff95e6eae6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "68f50e52-fb09-4025-afe2-16ea9fe06d73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65bf327e-3fe1-48e8-8b72-3ff95e6eae6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e845b444-60e9-4118-9495-a72010baf1eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65bf327e-3fe1-48e8-8b72-3ff95e6eae6c",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "d53c841a-eec1-4ea5-a3b8-576d396c8293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "02cc3ef1-137d-4e03-a7e8-1fd5d6674d04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d53c841a-eec1-4ea5-a3b8-576d396c8293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d5c8cc3-1fac-48df-873b-616da0ba797f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d53c841a-eec1-4ea5-a3b8-576d396c8293",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "5c72616d-4bf9-4f80-9335-9b7b396a589c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "00b731e7-e530-4222-bffb-c24fe7d507c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c72616d-4bf9-4f80-9335-9b7b396a589c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a82d4953-69dc-40b6-8bbd-9ce79675fe6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c72616d-4bf9-4f80-9335-9b7b396a589c",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "54df120f-b648-4756-a5eb-1c43cf11c9bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "c2541b7a-897e-498d-887d-3a4d1e9925bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54df120f-b648-4756-a5eb-1c43cf11c9bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9759f153-10ff-4157-8960-6c34e91b5b86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54df120f-b648-4756-a5eb-1c43cf11c9bb",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "cb1a9b28-ea54-4eaa-bfbd-7f569bb77093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "ed9fa947-f880-4d07-be27-b38f0ec62050",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb1a9b28-ea54-4eaa-bfbd-7f569bb77093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "678f38d8-4382-432e-bf00-0f9230ae01d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1a9b28-ea54-4eaa-bfbd-7f569bb77093",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "be3b5b7e-bc73-41a5-addc-821e7ae45563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "72adf6e5-1769-49df-9635-eeb9d67d7fbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be3b5b7e-bc73-41a5-addc-821e7ae45563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2345ddc3-8986-4acb-b6da-86a923191b0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be3b5b7e-bc73-41a5-addc-821e7ae45563",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "c094a118-5bed-43a3-b395-64bdb6f7b39f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "0042550a-68e9-449d-92b2-0a2fd920b07b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c094a118-5bed-43a3-b395-64bdb6f7b39f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14c2b0df-8e03-46f0-9d4d-fd804136d0f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c094a118-5bed-43a3-b395-64bdb6f7b39f",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "6660fe5c-ded0-4ae1-87d9-d33e54d32d2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "3c5b813a-6d27-4887-a2c7-13769722835a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6660fe5c-ded0-4ae1-87d9-d33e54d32d2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e1317b9-c28a-4796-aeb2-99247db6347c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6660fe5c-ded0-4ae1-87d9-d33e54d32d2d",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "733d56fb-343b-4c56-a25e-93c41204b0f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "8c7797bd-bba4-4db8-8e1e-0578804a582a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "733d56fb-343b-4c56-a25e-93c41204b0f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45eaed81-0e30-427e-a17a-54f805898a47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "733d56fb-343b-4c56-a25e-93c41204b0f1",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "af7665d4-3838-4325-8988-81a65c5a4932",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "04f42a04-30de-4cef-91ce-873b2fc26102",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af7665d4-3838-4325-8988-81a65c5a4932",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99f01766-6647-4ad9-8c65-d681ebd10de8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af7665d4-3838-4325-8988-81a65c5a4932",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        },
        {
            "id": "78f622ec-2032-4981-b6b2-e74bc40c942b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "compositeImage": {
                "id": "59476665-8efe-455d-9d72-c18b5eaf9a66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78f622ec-2032-4981-b6b2-e74bc40c942b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c0ccc70-6f78-44f5-b946-c19bed2c8a60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78f622ec-2032-4981-b6b2-e74bc40c942b",
                    "LayerId": "28a93c26-43bb-4d28-8564-40bdb7b25be5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "28a93c26-43bb-4d28-8564-40bdb7b25be5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 34,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 112,
    "xorig": 55,
    "yorig": 24
}