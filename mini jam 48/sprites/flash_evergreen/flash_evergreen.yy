{
    "id": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flash_evergreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65806b33-9d4a-43e7-8af4-6b6365d6f18d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "1c1849bd-179c-47be-9290-d8d316449778",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65806b33-9d4a-43e7-8af4-6b6365d6f18d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "192ecf62-b943-4878-81a9-8ed40113d756",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65806b33-9d4a-43e7-8af4-6b6365d6f18d",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "2bbead92-4aca-4c0b-9944-3668bf808e6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "2fc7d925-fe29-45ac-9f69-7c094bafdd8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bbead92-4aca-4c0b-9944-3668bf808e6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d155fa9f-68b3-4f7b-a217-e9efa33cdb3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bbead92-4aca-4c0b-9944-3668bf808e6c",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "054deb76-429e-4170-9acb-3e273cc28504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "50c3c9b2-28d1-4405-8674-6e1cf407bbe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "054deb76-429e-4170-9acb-3e273cc28504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0413f988-096e-4b59-8f9e-adfdd7e4e65b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "054deb76-429e-4170-9acb-3e273cc28504",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "7f1a0f6c-969e-48c5-8415-f9f8017299a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "1d773ece-c593-4f28-bba2-d29d46485c43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f1a0f6c-969e-48c5-8415-f9f8017299a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2afa22c-11f0-46fd-80c1-5df69200f675",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f1a0f6c-969e-48c5-8415-f9f8017299a0",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "81eaee41-5728-4385-823c-aeb19b284046",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "c02b07cc-c4c5-40df-a936-5042f834380f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81eaee41-5728-4385-823c-aeb19b284046",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "700709b7-649f-490e-a98c-a9206cd9f34c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81eaee41-5728-4385-823c-aeb19b284046",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "dc54d0b8-d6e5-4e9f-9919-9b961cb44cee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "d815bf7c-5595-43c2-bb3c-60b1fe30131b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc54d0b8-d6e5-4e9f-9919-9b961cb44cee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a15349c0-01fc-4511-97b1-0148c4b310b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc54d0b8-d6e5-4e9f-9919-9b961cb44cee",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "c31e7c60-8260-4077-bd4a-db82bad52f7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "0a8dc274-fcc4-4213-9d2e-1cc0f80208c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c31e7c60-8260-4077-bd4a-db82bad52f7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0366d2b-4d53-4c02-b4ba-37ed6a21feb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c31e7c60-8260-4077-bd4a-db82bad52f7a",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "0d094af8-3c68-4a27-b7d3-61e6da644355",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "a38ce065-9927-4eab-84f1-7fde7f81ddae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d094af8-3c68-4a27-b7d3-61e6da644355",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cf17930-8f8d-4855-a7c9-6cec86fbe631",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d094af8-3c68-4a27-b7d3-61e6da644355",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "e9128d36-1658-4a3f-8f19-ae9b6e695b34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "d4c154b0-28ff-4865-b110-16457afc88b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9128d36-1658-4a3f-8f19-ae9b6e695b34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e193146-90c0-4f0f-8e18-f372608eb7de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9128d36-1658-4a3f-8f19-ae9b6e695b34",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "f9b86159-c12a-4d91-aeda-ae07f5a0d55d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "4d01285a-161f-4c51-bce0-4bc39b414bb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9b86159-c12a-4d91-aeda-ae07f5a0d55d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb66f703-4d87-4534-9e8f-ea1f5b6efaec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9b86159-c12a-4d91-aeda-ae07f5a0d55d",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "f2d2a706-b161-44f7-b718-b455a31601f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "5b3f9069-6523-463f-9bde-a3ef0b96333f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2d2a706-b161-44f7-b718-b455a31601f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97d62f5c-1018-4cec-af85-9f63d430ff00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2d2a706-b161-44f7-b718-b455a31601f4",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "d6c23c01-1663-47a1-8267-e79b8dd0ae6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "9fe06b40-3c61-4cbe-9c22-e26c6fc3c303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6c23c01-1663-47a1-8267-e79b8dd0ae6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a573f34-14d8-43ad-a6e5-d9d18e036fd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6c23c01-1663-47a1-8267-e79b8dd0ae6c",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "1e35b079-b72e-4e11-bbde-8f9a0a253049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "6a21041b-c7f7-4dd0-86f8-98d788ad9941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e35b079-b72e-4e11-bbde-8f9a0a253049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba35df22-da10-41a0-b7a2-0b30429ce9df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e35b079-b72e-4e11-bbde-8f9a0a253049",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "a413beae-6913-4fed-9bed-36619fb9782b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "8ad7e0e9-689d-4794-b081-11250f997a52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a413beae-6913-4fed-9bed-36619fb9782b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8525f941-1d85-4e21-9d15-07d2376391c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a413beae-6913-4fed-9bed-36619fb9782b",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "bdfd948a-cd4d-452a-93be-af0749e2c60e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "78ed09fb-f59d-4113-87fd-f96951822425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdfd948a-cd4d-452a-93be-af0749e2c60e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25b66463-1eea-491c-b759-a350a2718fac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdfd948a-cd4d-452a-93be-af0749e2c60e",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "677a212f-99d0-4bf1-890e-25a48a95920d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "2d79a81f-ba68-4f53-95ce-801a2805faa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "677a212f-99d0-4bf1-890e-25a48a95920d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8664f720-e27e-419d-ab5f-5aac7b8bef2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "677a212f-99d0-4bf1-890e-25a48a95920d",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "435cdd67-59f5-4b12-b592-827997c9de8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "ac8df011-24e2-43b0-acf4-3ec4cdb1b0eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "435cdd67-59f5-4b12-b592-827997c9de8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "799b973c-e365-47a1-9d02-f81742b8b78d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "435cdd67-59f5-4b12-b592-827997c9de8c",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        },
        {
            "id": "77b4a7b1-93cc-4adb-8776-10ec1b3f7f57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "compositeImage": {
                "id": "01388a20-fbb1-4190-9f5f-dc5361ab3687",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77b4a7b1-93cc-4adb-8776-10ec1b3f7f57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27b59b40-ef8a-4e2c-b005-e6924d6b76b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77b4a7b1-93cc-4adb-8776-10ec1b3f7f57",
                    "LayerId": "35cb5735-dc6f-47bd-aa33-23b6408c0714"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "35cb5735-dc6f-47bd-aa33-23b6408c0714",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 21,
    "yorig": 20
}