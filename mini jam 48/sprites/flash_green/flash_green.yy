{
    "id": "eb70b244-11ea-4715-8875-aa4d087c832f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flash_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f6bc94e-26b4-4d96-a3c6-fbe65c000c8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "bde2bc21-d0ad-4508-bf45-3c3f5499748f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f6bc94e-26b4-4d96-a3c6-fbe65c000c8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5d91f6f-e2fe-4d66-8798-14d752719cb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f6bc94e-26b4-4d96-a3c6-fbe65c000c8c",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "bdeefb28-bc0e-40b8-8366-f43a908bb8c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "e9c3b661-0544-41f6-80ec-e2abc91ecff8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdeefb28-bc0e-40b8-8366-f43a908bb8c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3c56774-2caa-4550-9f43-2a5a34db95ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdeefb28-bc0e-40b8-8366-f43a908bb8c4",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "2baf8121-c6f0-4692-a56c-782acb4691ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "c46f19d6-79ff-4ef2-8ca6-03e454cb13c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2baf8121-c6f0-4692-a56c-782acb4691ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dc7ef4f-7b8c-48b9-8390-8933907d8cf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2baf8121-c6f0-4692-a56c-782acb4691ed",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "2f7da1d8-8f1f-48cb-8574-6a92b35ba913",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "92d55bdb-931e-40bc-9cf8-06e52ec77b56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f7da1d8-8f1f-48cb-8574-6a92b35ba913",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7795583-ab55-4293-9c79-02784145eeec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f7da1d8-8f1f-48cb-8574-6a92b35ba913",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "41d34918-82fb-4bd0-b7c7-c70d6ab7c419",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "4507ce38-e363-4eb1-875c-86129989a8bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41d34918-82fb-4bd0-b7c7-c70d6ab7c419",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d3a4c3e-70eb-4c69-9073-b9271b624624",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41d34918-82fb-4bd0-b7c7-c70d6ab7c419",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "5c3fcea3-2f03-480a-b754-c70d840b983d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "d4aad1f3-dd4d-4fb5-89a1-4a0db57e3fd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c3fcea3-2f03-480a-b754-c70d840b983d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db7c89a4-c1b1-41e3-8646-2cbcbe8cdd12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c3fcea3-2f03-480a-b754-c70d840b983d",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "006664c2-b1aa-42c7-8ff6-d9b628f89359",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "97740e60-a95c-40f6-841c-45a86ec806d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "006664c2-b1aa-42c7-8ff6-d9b628f89359",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e1b0371-30cc-4f9f-8593-7ba098f5d067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "006664c2-b1aa-42c7-8ff6-d9b628f89359",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "e6e54dd0-7bdd-40a1-a6d4-8e0786f338bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "8b5ac80a-e21f-4b80-8b75-73126e656539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6e54dd0-7bdd-40a1-a6d4-8e0786f338bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c9c59a0-0d2f-47c7-ae60-984c9271124c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6e54dd0-7bdd-40a1-a6d4-8e0786f338bd",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "94b52b89-61e2-47ce-935d-ac3d3991a42d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "ebe80ee2-e4f9-4d6b-8f8d-ff70abb7acd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94b52b89-61e2-47ce-935d-ac3d3991a42d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d20a5f0-cec9-4787-9fb7-fa8783800139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94b52b89-61e2-47ce-935d-ac3d3991a42d",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "195a1f22-0660-4929-9de3-f820475a5a97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "54146fa0-28f9-4343-a3fc-1704e0c362d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "195a1f22-0660-4929-9de3-f820475a5a97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef69691f-ba6a-49db-b64c-b6741aec4c29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "195a1f22-0660-4929-9de3-f820475a5a97",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "18726e2d-8281-449b-ab7a-bc2aebd7f8d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "fd21e919-1f65-443a-9e3e-ddc48d4aa1fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18726e2d-8281-449b-ab7a-bc2aebd7f8d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b34420f9-630d-43ad-9dda-c014eba07f2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18726e2d-8281-449b-ab7a-bc2aebd7f8d5",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "49f5f084-c63e-40c2-85ed-29b05e456804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "8340d231-0224-4289-b658-74e4c7613c24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49f5f084-c63e-40c2-85ed-29b05e456804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a841a65-fe45-442c-8156-40e566b5befa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49f5f084-c63e-40c2-85ed-29b05e456804",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "3fd722ad-af00-40e4-a746-dbb8800d0cd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "6357f7e3-454b-4480-95d3-600d1050a4c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fd722ad-af00-40e4-a746-dbb8800d0cd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e21aca3d-735d-44a5-ba08-af9b6ade5539",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fd722ad-af00-40e4-a746-dbb8800d0cd4",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "db006bf9-4845-4bf0-9834-9732d86d273e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "a23b3e50-f59b-4bd2-a230-64c7f164371e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db006bf9-4845-4bf0-9834-9732d86d273e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcf87b97-aaa4-4b21-9962-4a5b9f3a184f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db006bf9-4845-4bf0-9834-9732d86d273e",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "83d68fdf-7358-4f58-b1fa-c011d43304a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "1a4c0d21-8fd2-4232-90b0-37b5b3ba6952",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83d68fdf-7358-4f58-b1fa-c011d43304a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb14284-566b-4cd5-b70a-96f10a28fd87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83d68fdf-7358-4f58-b1fa-c011d43304a8",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "d499e1ae-4f32-4df6-a03a-9489312df375",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "6c69d94b-3f76-431c-8835-44cebb84b5db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d499e1ae-4f32-4df6-a03a-9489312df375",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b64629ca-f112-4ef6-ae86-89b26031ab79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d499e1ae-4f32-4df6-a03a-9489312df375",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "9325ea55-59f1-4ebd-a3a9-048c64feb5cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "f1560414-c121-46fe-8ff9-16f90ba01952",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9325ea55-59f1-4ebd-a3a9-048c64feb5cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c086792d-bdf4-41a5-95be-c389b370dd67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9325ea55-59f1-4ebd-a3a9-048c64feb5cf",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        },
        {
            "id": "fd95db5d-f3ab-4ba2-ada9-3bccc247a4b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "compositeImage": {
                "id": "bab21366-4dea-49b4-bb3f-1d0eec97e238",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd95db5d-f3ab-4ba2-ada9-3bccc247a4b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "299b459c-9b3b-4278-93f3-7cbb8d289793",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd95db5d-f3ab-4ba2-ada9-3bccc247a4b0",
                    "LayerId": "636004d4-c2e8-4f56-992c-1e39a72e7de3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "636004d4-c2e8-4f56-992c-1e39a72e7de3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 21,
    "yorig": 20
}