{
    "id": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "HP__ProgressBar_Animated",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 491,
    "bbox_left": 0,
    "bbox_right": 81,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74f521f2-227a-4c98-9734-b2fd450aa78a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "10233d3a-6991-4b67-ac83-f6d2decd4b0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74f521f2-227a-4c98-9734-b2fd450aa78a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71a1d502-c916-4fc2-9480-29febcb398f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74f521f2-227a-4c98-9734-b2fd450aa78a",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "b3d977f7-5fcd-4333-ac61-bd714167c5be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "32a244dc-2bdc-4248-bac7-1fed92cd658b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3d977f7-5fcd-4333-ac61-bd714167c5be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbe04535-8afa-44e5-b1cc-65454ecfe2fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d977f7-5fcd-4333-ac61-bd714167c5be",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "a08bc4fe-936b-4c5d-86bc-e51c2ec4e71c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "47e448f8-1a32-4239-834c-a4ba5202298b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a08bc4fe-936b-4c5d-86bc-e51c2ec4e71c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e19ca41d-0ace-4ca2-97aa-7ab88281fd27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a08bc4fe-936b-4c5d-86bc-e51c2ec4e71c",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "587f608e-2471-4004-9f19-78ad3f91b32f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "0b4ed8da-d7ba-4d0a-9c25-9b302b36e45d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "587f608e-2471-4004-9f19-78ad3f91b32f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99b3ff8c-0b21-486a-a7b3-ea64ff803f6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "587f608e-2471-4004-9f19-78ad3f91b32f",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "4af9fbdd-3dd9-4b1f-b3ab-513fd7647eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "010d0eca-795a-4e64-a45c-c30e01054503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4af9fbdd-3dd9-4b1f-b3ab-513fd7647eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d72775f6-427b-4c09-8db5-9e835594ab2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4af9fbdd-3dd9-4b1f-b3ab-513fd7647eba",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "1c9fbac1-eccb-461c-b976-6d32e86d8c0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "62670418-bc97-477a-837f-98e260098475",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c9fbac1-eccb-461c-b976-6d32e86d8c0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e400cf7c-8709-4ed0-8e87-3c4df7e491f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c9fbac1-eccb-461c-b976-6d32e86d8c0f",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "25e8bfff-53ae-4fc5-b65e-3066082c6ae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "ff433465-85cb-4871-b202-ef7aa1eb4135",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25e8bfff-53ae-4fc5-b65e-3066082c6ae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60606621-aa58-4b41-a64a-b2c3ff033cc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25e8bfff-53ae-4fc5-b65e-3066082c6ae7",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "edffe58d-4c9c-4883-b197-924176d5a56c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "b246a0ba-ebe3-4121-8d50-62f0adfa7e30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edffe58d-4c9c-4883-b197-924176d5a56c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57b701a0-7052-4cd5-8d42-2e831465083c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edffe58d-4c9c-4883-b197-924176d5a56c",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "ec4d1fb1-499d-4af1-ac1c-31f869e2e5cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "d997fbc9-f216-4c09-81f0-8fc264932274",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec4d1fb1-499d-4af1-ac1c-31f869e2e5cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52bf674f-4dd2-4ed9-a9d3-12ecdade6e67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec4d1fb1-499d-4af1-ac1c-31f869e2e5cc",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "fbbdb0a6-1c3b-4b54-b06d-6ddf5b7de362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "75031113-c14b-4849-a9a9-ab3e673dd86a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbbdb0a6-1c3b-4b54-b06d-6ddf5b7de362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cba8c9f-8a0d-40dd-bd11-f031cdd28a04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbbdb0a6-1c3b-4b54-b06d-6ddf5b7de362",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "c5edfb52-607d-4871-835c-91fa6735acf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "77669475-9d27-4017-8015-51d516e5fde6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5edfb52-607d-4871-835c-91fa6735acf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98ef57d3-e92b-4b02-9482-fd81e00a6ff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5edfb52-607d-4871-835c-91fa6735acf3",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "1e883755-10c3-4fee-995b-3b8ab118f8cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "c0b3dc6a-9b4e-44b0-8464-06b72a709147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e883755-10c3-4fee-995b-3b8ab118f8cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d07d32bb-cfe2-4e1e-9b4d-0480602b436d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e883755-10c3-4fee-995b-3b8ab118f8cf",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "c6f2c249-35d1-41c1-b0c0-d31193a98431",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "3d3b1977-827d-40d8-ac74-133e784ce596",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6f2c249-35d1-41c1-b0c0-d31193a98431",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a20ad9ee-134d-448a-8f89-d2d77e247350",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6f2c249-35d1-41c1-b0c0-d31193a98431",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "ecdbaaa9-048f-4dcd-9093-7cb061d42d0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "9f092659-1d88-4851-a90f-fc6755ae308f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecdbaaa9-048f-4dcd-9093-7cb061d42d0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39644a49-b7f4-44bc-8224-411b9930f70c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecdbaaa9-048f-4dcd-9093-7cb061d42d0f",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "355ca1d3-a926-4c01-ae0e-9cc90dfc01cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "adaa4a94-494b-490a-ba6b-4728ea72e6d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "355ca1d3-a926-4c01-ae0e-9cc90dfc01cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b61c0b4f-1441-41d8-9ea8-0208d8da76bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "355ca1d3-a926-4c01-ae0e-9cc90dfc01cf",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "a30b3365-3487-48e9-a6eb-df7cad434c16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "3812e928-1eef-4b09-aeaa-32ba4c90ea30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a30b3365-3487-48e9-a6eb-df7cad434c16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "613b326d-f528-430b-987f-366190d0916b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a30b3365-3487-48e9-a6eb-df7cad434c16",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "61b097cb-a922-4e03-8c50-1c7fe85daf6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "38ea4805-8825-47c0-a9c4-c48cb2fb6e13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61b097cb-a922-4e03-8c50-1c7fe85daf6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e58df13-a40b-415b-aaf4-f8a5d7e3dc10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61b097cb-a922-4e03-8c50-1c7fe85daf6c",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "6946def9-49af-4141-b356-9370c0f5af5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "a4980c78-233b-4dd6-bab2-7f640a0a3dec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6946def9-49af-4141-b356-9370c0f5af5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c78fbefd-5fe7-4d80-9ea6-50ff74d4184c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6946def9-49af-4141-b356-9370c0f5af5b",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "3a057aaf-ff65-4a6d-8ae9-43e51747389f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "c571d2fc-2a54-4f85-81f0-5c814b500260",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a057aaf-ff65-4a6d-8ae9-43e51747389f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4925cea1-a72f-43fe-adbe-9b6a7d020cf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a057aaf-ff65-4a6d-8ae9-43e51747389f",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "5fce8b0f-096c-4255-9939-e2caf0f0b6cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "c90976b4-fb8c-48e2-a52b-1771bf8dee1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fce8b0f-096c-4255-9939-e2caf0f0b6cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c36603fe-c07e-42c3-a82b-df3b0313087a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fce8b0f-096c-4255-9939-e2caf0f0b6cf",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "9be05955-0a15-408c-9a88-6bd5ccf71ec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "9b06a641-5029-43f1-b4a7-75ee5993e2a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9be05955-0a15-408c-9a88-6bd5ccf71ec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5a633bb-a89a-4276-b845-95e4eca51d93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9be05955-0a15-408c-9a88-6bd5ccf71ec1",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "b45a58f5-7da6-4ba8-bd64-e117edcb38cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "5f3a58d6-0c91-4829-8b83-c1f8e85b3238",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b45a58f5-7da6-4ba8-bd64-e117edcb38cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fc82439-2cdd-473a-8338-c477d028745f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b45a58f5-7da6-4ba8-bd64-e117edcb38cb",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "fad0518e-a940-4814-b048-8e36c616a477",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "6c39a50c-4853-4b03-82f1-c37f8d196539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fad0518e-a940-4814-b048-8e36c616a477",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8c437ae-b04b-4fc5-b3e2-4c2e9da96009",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fad0518e-a940-4814-b048-8e36c616a477",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "8ed64e9c-43b8-4224-950f-2e4c006ea7c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "4b9ae037-05cd-4359-b199-f85c143f08e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ed64e9c-43b8-4224-950f-2e4c006ea7c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c55cb1c5-2d36-44e6-9b4f-7b1c3ea189e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ed64e9c-43b8-4224-950f-2e4c006ea7c7",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "106374dd-931d-463b-bf1d-5aec94d10e20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "72b6bcb6-23ff-4c2d-b03c-63551b26f55a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "106374dd-931d-463b-bf1d-5aec94d10e20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d15cdb2-7131-4100-aa7e-6a58e1974d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "106374dd-931d-463b-bf1d-5aec94d10e20",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "55ae4a95-109a-4caf-a653-1063520438bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "07f0a981-cad8-4e35-8290-b97b0f0eed8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55ae4a95-109a-4caf-a653-1063520438bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89562cf7-27ae-41ee-930d-6e92ac87dfce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55ae4a95-109a-4caf-a653-1063520438bd",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "f7aeb70c-d873-4d87-840e-dfe8e97dc4d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "96df798c-012b-460e-9703-406c24c4961a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7aeb70c-d873-4d87-840e-dfe8e97dc4d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "214aad88-d489-490b-8248-d65a6be521b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7aeb70c-d873-4d87-840e-dfe8e97dc4d1",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "da08b09b-b387-429f-b80b-99e165c93fff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "138aa0be-6369-46c0-b6ea-15ffcb743b2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da08b09b-b387-429f-b80b-99e165c93fff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eae00f02-e8f6-4cff-9a43-d7526babd939",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da08b09b-b387-429f-b80b-99e165c93fff",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "bd756c87-60f1-4c77-8092-aab3bdbdd329",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "387e016b-da4f-4a4b-9f1c-79dc3574274f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd756c87-60f1-4c77-8092-aab3bdbdd329",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "168e7ab5-a0cc-40f5-837f-84cb15c63400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd756c87-60f1-4c77-8092-aab3bdbdd329",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "8efbf32f-46da-4c93-b276-ccd7338d79ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "111ec14a-54b5-4016-b683-cadd2c852c22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8efbf32f-46da-4c93-b276-ccd7338d79ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "489d1739-5901-4efd-ad9f-0b8a5dd7b768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8efbf32f-46da-4c93-b276-ccd7338d79ea",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "3b05efa4-6a13-49bf-b118-42519040584f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "c2958211-c23f-4710-84b6-02727d52d06d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b05efa4-6a13-49bf-b118-42519040584f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6de8eb46-623c-4af1-84aa-1b24249d6949",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b05efa4-6a13-49bf-b118-42519040584f",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "3ea08d61-b1f8-464b-8103-238c58db5bbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "2dab6a56-89ab-4126-8f17-b5c83e42c991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ea08d61-b1f8-464b-8103-238c58db5bbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "493531a9-c157-49be-a9b4-97993e5f2875",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ea08d61-b1f8-464b-8103-238c58db5bbc",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "96042f43-b172-483a-b719-d6045ce56ad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "92dbac16-e202-4301-be67-a2e009d718ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96042f43-b172-483a-b719-d6045ce56ad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a45033b4-2643-49fc-9b95-635834d415e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96042f43-b172-483a-b719-d6045ce56ad6",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "764b2b64-4b13-4daa-89ac-aa6eb47681ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "3a6532ae-1111-45fd-b347-2a1db24ea372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "764b2b64-4b13-4daa-89ac-aa6eb47681ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0acaa1a3-0167-4fb6-92ae-92e78b622a67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "764b2b64-4b13-4daa-89ac-aa6eb47681ff",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "162fab6c-1f59-4b7d-a25d-ea08302a2cb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "e053290c-4f9f-422c-81bf-3c537d8f8217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "162fab6c-1f59-4b7d-a25d-ea08302a2cb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4879d9eb-3201-4054-a336-061d3523c0e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "162fab6c-1f59-4b7d-a25d-ea08302a2cb9",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "e3fcbf35-f783-4c19-b496-26c5f0fa1383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "b7595f0e-b442-477d-a954-0d60b61d2768",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3fcbf35-f783-4c19-b496-26c5f0fa1383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4186d9c-feb1-4420-8986-a0ef7fab6f57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3fcbf35-f783-4c19-b496-26c5f0fa1383",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "a89194e0-9d29-4158-9f06-f563303e482c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "1ffd501a-fa3c-472a-b1d1-9181b9915291",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a89194e0-9d29-4158-9f06-f563303e482c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3661a1f0-b2c5-47ab-be5a-928de93d7b6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a89194e0-9d29-4158-9f06-f563303e482c",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "0a2a2cc0-fa3e-4397-b373-74844a3c91c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "c45a2a91-65ef-455b-b69b-348b962caaf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a2a2cc0-fa3e-4397-b373-74844a3c91c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "523d50c3-58e1-4165-b15b-6935d1131f84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a2a2cc0-fa3e-4397-b373-74844a3c91c7",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "160a5e32-b0af-4b20-88b8-08042474bef9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "d951ab6c-225c-40ab-aff6-40345846c49e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "160a5e32-b0af-4b20-88b8-08042474bef9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57884e62-404c-43b7-80a9-aee1155cbeed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "160a5e32-b0af-4b20-88b8-08042474bef9",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "f674710b-1a48-4ca0-8e1a-8191dbe7f099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "19773ec5-6410-4ea8-bcd0-995c8f19c6cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f674710b-1a48-4ca0-8e1a-8191dbe7f099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79d6a969-2205-46ca-86e9-f2991b16cafb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f674710b-1a48-4ca0-8e1a-8191dbe7f099",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "40cf2a4a-3fea-4dd0-b681-0305f24e34ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "be90eab2-a486-47d2-b9de-0786a8c976d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40cf2a4a-3fea-4dd0-b681-0305f24e34ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a20ed289-d0ac-4cc0-a5cf-b1b697dbc61a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40cf2a4a-3fea-4dd0-b681-0305f24e34ea",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "f7c04301-17cd-43e2-bc07-d55cd045d97c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "88fa5482-67d7-4c9b-943d-e00b16978ecf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7c04301-17cd-43e2-bc07-d55cd045d97c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "988aec9a-bf43-47a2-ae55-d929d51b6a27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7c04301-17cd-43e2-bc07-d55cd045d97c",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "654fb1a4-5cc3-4a20-ae02-dea7e12e48f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "6f85f587-eb31-4466-a9f4-a91e6122f0c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "654fb1a4-5cc3-4a20-ae02-dea7e12e48f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae8fe10b-4a50-41b4-a33b-8eb1f12dcca2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "654fb1a4-5cc3-4a20-ae02-dea7e12e48f9",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "3548e33d-00f9-4be4-b451-a41be7567282",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "27f5483f-6de1-46f2-813c-8a6076d2ddff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3548e33d-00f9-4be4-b451-a41be7567282",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e654b870-f7d4-4fbf-aef6-a14201662154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3548e33d-00f9-4be4-b451-a41be7567282",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "3f0309ec-ce06-41e3-95e3-9db648a4661d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "b2ff5176-6ff9-479c-b4ac-c57a3e075af9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f0309ec-ce06-41e3-95e3-9db648a4661d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5ff9ed2-24bc-49ea-a6c1-fd4efe3de629",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f0309ec-ce06-41e3-95e3-9db648a4661d",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "88490bfe-bab1-4b22-84db-db6ad54417e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "c3738d37-bb2b-49fd-9bd1-34e745008b14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88490bfe-bab1-4b22-84db-db6ad54417e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09cc4b10-1ad1-4300-8c5a-4f3b63239c43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88490bfe-bab1-4b22-84db-db6ad54417e5",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "ac0a78b9-03f2-415e-a133-a8e1596381b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "d4740d53-50e1-4f3e-93a9-60bb6ca40a2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac0a78b9-03f2-415e-a133-a8e1596381b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e37c2b47-be97-4905-bd74-7fd50710ccc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac0a78b9-03f2-415e-a133-a8e1596381b4",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "e8a2ae6f-5dfc-4da0-8194-8931011f1a88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "76cb6696-ec2f-4195-821e-0d8433a7d562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8a2ae6f-5dfc-4da0-8194-8931011f1a88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d24863e-4046-41f4-b2ca-4f289caaea4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8a2ae6f-5dfc-4da0-8194-8931011f1a88",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        },
        {
            "id": "5eb914ad-8d1c-4d64-baf1-e2c9ad0ecc42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "compositeImage": {
                "id": "cc30bd80-7b78-482b-ad6c-0c2037ef9e0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eb914ad-8d1c-4d64-baf1-e2c9ad0ecc42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf139dd6-4b65-4701-9a4a-ad23e0c64ffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eb914ad-8d1c-4d64-baf1-e2c9ad0ecc42",
                    "LayerId": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 503,
    "layers": [
        {
            "id": "383740f6-d3ae-4b5b-b9ce-6059c56cb62f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 94,
    "xorig": 0,
    "yorig": 0
}