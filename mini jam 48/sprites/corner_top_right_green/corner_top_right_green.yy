{
    "id": "7a776060-5662-4857-aca1-b3b50f9b2e3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_top_right_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8eea45e0-0ddf-4309-aff0-ebaed004757e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a776060-5662-4857-aca1-b3b50f9b2e3e",
            "compositeImage": {
                "id": "cc2bb05f-ae27-4159-b815-fc3c32e46cf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eea45e0-0ddf-4309-aff0-ebaed004757e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96ef66e1-ad82-4eb5-87b4-fc561e330bc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eea45e0-0ddf-4309-aff0-ebaed004757e",
                    "LayerId": "77fd2404-f480-409b-832e-50fbc4530a19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "77fd2404-f480-409b-832e-50fbc4530a19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a776060-5662-4857-aca1-b3b50f9b2e3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}