{
    "id": "4d1440e8-93ec-465d-9fe8-7a16b79d29fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_down_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e20da1d-c851-4222-b17d-109d0eba454b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d1440e8-93ec-465d-9fe8-7a16b79d29fd",
            "compositeImage": {
                "id": "9a748767-5f24-4c4c-b803-bbfe107ef244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e20da1d-c851-4222-b17d-109d0eba454b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a055027b-ae88-4267-bf83-5d785a1aaa66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e20da1d-c851-4222-b17d-109d0eba454b",
                    "LayerId": "9d54e4cb-e5bd-455b-bd3d-d7a34a80fc22"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9d54e4cb-e5bd-455b-bd3d-d7a34a80fc22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d1440e8-93ec-465d-9fe8-7a16b79d29fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}