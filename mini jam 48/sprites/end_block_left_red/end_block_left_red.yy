{
    "id": "35165546-3784-4dab-afee-418df1000e4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_left_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccff3d02-f6bd-4977-96d0-7d4a83d647b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35165546-3784-4dab-afee-418df1000e4c",
            "compositeImage": {
                "id": "7844691a-fdb4-4172-bbad-b1c37c3efa0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccff3d02-f6bd-4977-96d0-7d4a83d647b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec905f83-94ec-46f7-bb58-797c23ec4db5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccff3d02-f6bd-4977-96d0-7d4a83d647b8",
                    "LayerId": "1adb83af-f333-4354-b3e4-572b5ceb6e10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1adb83af-f333-4354-b3e4-572b5ceb6e10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35165546-3784-4dab-afee-418df1000e4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}