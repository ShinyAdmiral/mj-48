{
    "id": "6d3c0857-a173-4c9b-826b-336f060a50d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "BO_DGreenO",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f73276d-8c22-454d-9aec-c92a9257a626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d3c0857-a173-4c9b-826b-336f060a50d5",
            "compositeImage": {
                "id": "444717ac-4b20-44c5-8c46-8b215920038e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f73276d-8c22-454d-9aec-c92a9257a626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "218edb7b-2117-4dbd-a972-f96da7b914ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f73276d-8c22-454d-9aec-c92a9257a626",
                    "LayerId": "9e2acfbb-75da-416c-a08f-953b6ae10d8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "9e2acfbb-75da-416c-a08f-953b6ae10d8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d3c0857-a173-4c9b-826b-336f060a50d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 0,
    "yorig": 0
}