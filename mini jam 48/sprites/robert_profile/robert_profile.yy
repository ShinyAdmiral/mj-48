{
    "id": "813649aa-29af-46e1-b647-31636b15ea9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "robert_profile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 138,
    "bbox_left": 63,
    "bbox_right": 144,
    "bbox_top": 64,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2baa2e26-1b7b-40f5-89f4-8635e887751d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "813649aa-29af-46e1-b647-31636b15ea9b",
            "compositeImage": {
                "id": "23d1144e-d65b-4bf1-9a39-0cecde283d78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2baa2e26-1b7b-40f5-89f4-8635e887751d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaaef313-38e3-43b8-9b01-0724554abeac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2baa2e26-1b7b-40f5-89f4-8635e887751d",
                    "LayerId": "0631ff61-1486-4126-90d4-ad60eb741d05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "0631ff61-1486-4126-90d4-ad60eb741d05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "813649aa-29af-46e1-b647-31636b15ea9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 208,
    "xorig": 104,
    "yorig": 96
}