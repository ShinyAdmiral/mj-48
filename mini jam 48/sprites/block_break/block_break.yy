{
    "id": "71e1c995-e8ce-4d46-92e9-d8acf4279131",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "block_break",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73443c49-e1d7-46fb-bfeb-2c8f00ef65a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e1c995-e8ce-4d46-92e9-d8acf4279131",
            "compositeImage": {
                "id": "52c27f16-2559-464b-9b93-4ee245278004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73443c49-e1d7-46fb-bfeb-2c8f00ef65a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87786bdd-025b-49e0-a71e-c13603fd9a7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73443c49-e1d7-46fb-bfeb-2c8f00ef65a2",
                    "LayerId": "7c974fc1-3f76-49e6-9126-b47029d1e58a"
                }
            ]
        },
        {
            "id": "0a279367-58a7-4c54-81db-5673e12e7f0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e1c995-e8ce-4d46-92e9-d8acf4279131",
            "compositeImage": {
                "id": "a2b18b88-06b3-458c-acc7-c3fcb183dd64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a279367-58a7-4c54-81db-5673e12e7f0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3bbf32d-0269-44f7-b9ad-2b8de69fa79c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a279367-58a7-4c54-81db-5673e12e7f0b",
                    "LayerId": "7c974fc1-3f76-49e6-9126-b47029d1e58a"
                }
            ]
        },
        {
            "id": "e6b27a41-7ee9-4d29-bdb5-b03657ff3d75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e1c995-e8ce-4d46-92e9-d8acf4279131",
            "compositeImage": {
                "id": "7266ae12-7c39-43ac-92ed-09ba74a2361b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6b27a41-7ee9-4d29-bdb5-b03657ff3d75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2fb2f11-f63a-4d61-879c-408d09f2b849",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6b27a41-7ee9-4d29-bdb5-b03657ff3d75",
                    "LayerId": "7c974fc1-3f76-49e6-9126-b47029d1e58a"
                }
            ]
        },
        {
            "id": "8931cc71-f8f2-40d1-9ed2-1a7e4690a21f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e1c995-e8ce-4d46-92e9-d8acf4279131",
            "compositeImage": {
                "id": "93b0d3f8-922f-4710-9453-d7bbe0c2ea1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8931cc71-f8f2-40d1-9ed2-1a7e4690a21f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9678805-641f-4381-b0e6-edf936474bdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8931cc71-f8f2-40d1-9ed2-1a7e4690a21f",
                    "LayerId": "7c974fc1-3f76-49e6-9126-b47029d1e58a"
                }
            ]
        },
        {
            "id": "79406686-944a-43f8-a2be-69293be20cf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e1c995-e8ce-4d46-92e9-d8acf4279131",
            "compositeImage": {
                "id": "63f96173-61bc-419d-97aa-757dd6a9a382",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79406686-944a-43f8-a2be-69293be20cf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65dc25f2-782a-4899-8f42-d9121d5cda5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79406686-944a-43f8-a2be-69293be20cf8",
                    "LayerId": "7c974fc1-3f76-49e6-9126-b47029d1e58a"
                }
            ]
        },
        {
            "id": "38204c30-0809-44bd-9cfe-8abca70eccf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e1c995-e8ce-4d46-92e9-d8acf4279131",
            "compositeImage": {
                "id": "de2a1f47-2c40-4fb2-b227-09b83135eb6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38204c30-0809-44bd-9cfe-8abca70eccf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19547b52-c7fe-4158-9435-fe6340f877ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38204c30-0809-44bd-9cfe-8abca70eccf0",
                    "LayerId": "7c974fc1-3f76-49e6-9126-b47029d1e58a"
                }
            ]
        },
        {
            "id": "d59953c0-e5a6-41ad-a517-6d68952d80cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e1c995-e8ce-4d46-92e9-d8acf4279131",
            "compositeImage": {
                "id": "e173c29a-e423-41a3-870d-e792cece97d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d59953c0-e5a6-41ad-a517-6d68952d80cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cab08ace-c7d1-4a96-9ebb-d8ba2e762f87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d59953c0-e5a6-41ad-a517-6d68952d80cb",
                    "LayerId": "7c974fc1-3f76-49e6-9126-b47029d1e58a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7c974fc1-3f76-49e6-9126-b47029d1e58a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71e1c995-e8ce-4d46-92e9-d8acf4279131",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}