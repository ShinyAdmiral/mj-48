{
    "id": "37eba367-ba88-4082-9e19-49f700b7b048",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "starting_platform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "109ab2cd-e0dc-4cb6-ad56-d9e149f76b97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37eba367-ba88-4082-9e19-49f700b7b048",
            "compositeImage": {
                "id": "13cf47e9-f86f-414b-adc9-6bf41623a3da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "109ab2cd-e0dc-4cb6-ad56-d9e149f76b97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc9ec11-e57a-4964-83ab-f5f1092e6e3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "109ab2cd-e0dc-4cb6-ad56-d9e149f76b97",
                    "LayerId": "8bb7561e-c02b-4f19-9b48-6c7ad148ecc0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "8bb7561e-c02b-4f19-9b48-6c7ad148ecc0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37eba367-ba88-4082-9e19-49f700b7b048",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}