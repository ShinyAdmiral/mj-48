{
    "id": "14566e4b-f553-4b9e-a7a2-ed228d547517",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "BO_RedJ",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61ba2327-656d-4e84-846a-fd2eb3c1a931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14566e4b-f553-4b9e-a7a2-ed228d547517",
            "compositeImage": {
                "id": "1a40f511-2e0a-4e59-a360-82f168ada92b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61ba2327-656d-4e84-846a-fd2eb3c1a931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fbd5f9f-f788-4f0a-ae0d-ff5820729ed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61ba2327-656d-4e84-846a-fd2eb3c1a931",
                    "LayerId": "77cb35b8-e7cf-425c-85b1-4dfd2f56e8d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "77cb35b8-e7cf-425c-85b1-4dfd2f56e8d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14566e4b-f553-4b9e-a7a2-ed228d547517",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 0,
    "yorig": 0
}