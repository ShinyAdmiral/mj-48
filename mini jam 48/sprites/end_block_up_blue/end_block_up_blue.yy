{
    "id": "7403df67-f00c-43db-b61e-6b28cc298f24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_up_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96676571-9dec-42b9-a981-dfd175c8363a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7403df67-f00c-43db-b61e-6b28cc298f24",
            "compositeImage": {
                "id": "45599617-1c56-48d1-acaa-dfdb3dec2f88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96676571-9dec-42b9-a981-dfd175c8363a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0ecc0ea-71fa-4b09-93df-a70ea6ca9ecc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96676571-9dec-42b9-a981-dfd175c8363a",
                    "LayerId": "f9619431-d4d4-42af-9764-1f2841486dd1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f9619431-d4d4-42af-9764-1f2841486dd1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7403df67-f00c-43db-b61e-6b28cc298f24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}