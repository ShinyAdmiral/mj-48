{
    "id": "e3840f04-749e-4271-bbc2-37089fefa877",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "middle_block_horizontal_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4a257f1-d971-4b94-8641-33778f58dc89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3840f04-749e-4271-bbc2-37089fefa877",
            "compositeImage": {
                "id": "ef01397d-595b-48a9-a050-9eba24cb5745",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4a257f1-d971-4b94-8641-33778f58dc89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c02762a3-ca70-4305-8617-fc2d7e27ec33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a257f1-d971-4b94-8641-33778f58dc89",
                    "LayerId": "5caa01af-c526-4406-8f07-3872d6478e77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5caa01af-c526-4406-8f07-3872d6478e77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3840f04-749e-4271-bbc2-37089fefa877",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}