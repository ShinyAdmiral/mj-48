{
    "id": "09d66430-09db-483b-9faa-beea1681bded",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "BO_YellowZ",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbb50946-382e-4f9e-938f-954b4b5e002e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09d66430-09db-483b-9faa-beea1681bded",
            "compositeImage": {
                "id": "0c721f4d-efb4-4736-9724-1094c1b0e6d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbb50946-382e-4f9e-938f-954b4b5e002e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78e06923-88bc-4a9a-a329-0d86e9e2bc93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbb50946-382e-4f9e-938f-954b4b5e002e",
                    "LayerId": "2f8b51c7-1826-4dab-8347-cb402eccd666"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "2f8b51c7-1826-4dab-8347-cb402eccd666",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09d66430-09db-483b-9faa-beea1681bded",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 0,
    "yorig": 0
}