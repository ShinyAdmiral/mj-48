{
    "id": "637a2191-771e-4035-8df6-5c83c0375f10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_down_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9bb3c02-163b-468a-842b-044eba3195c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "637a2191-771e-4035-8df6-5c83c0375f10",
            "compositeImage": {
                "id": "c764a4a4-5d41-4117-8c50-8df32788ffd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9bb3c02-163b-468a-842b-044eba3195c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0b2014e-d8e0-4859-8b38-a96c89ecf500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9bb3c02-163b-468a-842b-044eba3195c5",
                    "LayerId": "095b936e-3733-440a-8a73-131ce55e64b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "095b936e-3733-440a-8a73-131ce55e64b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "637a2191-771e-4035-8df6-5c83c0375f10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}