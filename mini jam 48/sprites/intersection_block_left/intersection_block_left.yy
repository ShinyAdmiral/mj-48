{
    "id": "97a4d739-3029-46e9-9996-8780e18bf376",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "intersection_block_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a49429d8-6a5a-48fb-b1b6-51ee1f924698",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97a4d739-3029-46e9-9996-8780e18bf376",
            "compositeImage": {
                "id": "13bfa12f-198e-408a-a840-2e5769dfc087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a49429d8-6a5a-48fb-b1b6-51ee1f924698",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ebd91cb-f5a9-4664-ac9b-3901846d97a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a49429d8-6a5a-48fb-b1b6-51ee1f924698",
                    "LayerId": "c905fc37-37cd-4fd1-a38e-2f126a512b61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c905fc37-37cd-4fd1-a38e-2f126a512b61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97a4d739-3029-46e9-9996-8780e18bf376",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}