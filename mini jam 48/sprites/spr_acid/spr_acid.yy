{
    "id": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_acid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 959,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a705c9c-c022-4624-9e63-840d813f32b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "compositeImage": {
                "id": "413b096f-1add-4f03-806c-f98de8e08725",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a705c9c-c022-4624-9e63-840d813f32b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26db9c27-9483-4f06-8ed3-dc312b8c6f11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a705c9c-c022-4624-9e63-840d813f32b2",
                    "LayerId": "30a6e8e0-c969-40c6-a372-0709ee7740fc"
                }
            ]
        },
        {
            "id": "6f109b87-84b8-4bb4-afa9-1d611166413e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "compositeImage": {
                "id": "673c251b-0b39-41c6-a932-294b4c8321e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f109b87-84b8-4bb4-afa9-1d611166413e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b0c5484-17a7-4f01-9f96-49ba97a8bc8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f109b87-84b8-4bb4-afa9-1d611166413e",
                    "LayerId": "30a6e8e0-c969-40c6-a372-0709ee7740fc"
                }
            ]
        },
        {
            "id": "d8b068f2-8aa0-4c24-ae3b-f90900c27ca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "compositeImage": {
                "id": "328d7531-9d24-4434-a72f-3b2154b269e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8b068f2-8aa0-4c24-ae3b-f90900c27ca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b4aed36-c5fc-4261-a42a-cd46ce4d9a10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8b068f2-8aa0-4c24-ae3b-f90900c27ca9",
                    "LayerId": "30a6e8e0-c969-40c6-a372-0709ee7740fc"
                }
            ]
        },
        {
            "id": "1a4d11c2-dc36-4a8f-b953-be4a9800163b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "compositeImage": {
                "id": "d566c64d-e16a-4eba-82dd-64da2953c473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a4d11c2-dc36-4a8f-b953-be4a9800163b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84e0ddb4-4b43-49af-b234-ab92b2e5bad0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a4d11c2-dc36-4a8f-b953-be4a9800163b",
                    "LayerId": "30a6e8e0-c969-40c6-a372-0709ee7740fc"
                }
            ]
        },
        {
            "id": "201419cf-6548-4f68-9434-41544b9a255e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "compositeImage": {
                "id": "7d4970e8-ccd0-47b0-9585-df4f1e9dc316",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "201419cf-6548-4f68-9434-41544b9a255e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f9570d5-c001-4874-afda-d14acd1ffcd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "201419cf-6548-4f68-9434-41544b9a255e",
                    "LayerId": "30a6e8e0-c969-40c6-a372-0709ee7740fc"
                }
            ]
        },
        {
            "id": "3eef09ff-7b5d-48a5-81ba-1a5fb9ad6a47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "compositeImage": {
                "id": "cd754fa0-b8ce-4773-99ee-f260ab62e59c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eef09ff-7b5d-48a5-81ba-1a5fb9ad6a47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f0bb836-715f-4d37-aa92-32159f158ccc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eef09ff-7b5d-48a5-81ba-1a5fb9ad6a47",
                    "LayerId": "30a6e8e0-c969-40c6-a372-0709ee7740fc"
                }
            ]
        },
        {
            "id": "d02ad365-2c62-4866-acbc-522d253f51d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "compositeImage": {
                "id": "25533959-7527-4a7a-aebd-215b08b73d1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d02ad365-2c62-4866-acbc-522d253f51d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8ff473e-d7b9-4067-ba95-7c0123e9e63f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d02ad365-2c62-4866-acbc-522d253f51d8",
                    "LayerId": "30a6e8e0-c969-40c6-a372-0709ee7740fc"
                }
            ]
        },
        {
            "id": "e21f3d77-6746-461b-9847-d62f63960582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "compositeImage": {
                "id": "7f72eba8-e74b-4807-b2fa-9e5bb1ddc6bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e21f3d77-6746-461b-9847-d62f63960582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a34373e5-c501-4648-aab3-f618f99a6ee1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e21f3d77-6746-461b-9847-d62f63960582",
                    "LayerId": "30a6e8e0-c969-40c6-a372-0709ee7740fc"
                }
            ]
        },
        {
            "id": "93c8365e-9b19-4fa7-aa1c-b910c7a595c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "compositeImage": {
                "id": "7c40a478-f0a2-42dd-800e-d01ce9add828",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93c8365e-9b19-4fa7-aa1c-b910c7a595c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9055ea8-fab4-41ad-8f78-0512ed423dbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93c8365e-9b19-4fa7-aa1c-b910c7a595c6",
                    "LayerId": "30a6e8e0-c969-40c6-a372-0709ee7740fc"
                }
            ]
        },
        {
            "id": "7389c860-37ac-4c1f-86b3-d6248886d744",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "compositeImage": {
                "id": "aa249edc-3b0f-46fb-a0bc-2debe058baf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7389c860-37ac-4c1f-86b3-d6248886d744",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f0155ad-1077-47e2-9c76-a6e1aa6f624d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7389c860-37ac-4c1f-86b3-d6248886d744",
                    "LayerId": "30a6e8e0-c969-40c6-a372-0709ee7740fc"
                }
            ]
        },
        {
            "id": "f0c1cb0f-857c-46a9-b444-bf1b217e5969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "compositeImage": {
                "id": "a88c3ef6-45ed-48c6-b09b-d06be43db201",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0c1cb0f-857c-46a9-b444-bf1b217e5969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33c9ebbc-bd7a-454c-91e3-72110784682f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c1cb0f-857c-46a9-b444-bf1b217e5969",
                    "LayerId": "30a6e8e0-c969-40c6-a372-0709ee7740fc"
                }
            ]
        },
        {
            "id": "731d0896-827d-4a15-9cb2-fbc7a6a34715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "compositeImage": {
                "id": "065d2bb5-1d94-4380-9244-6ff0ab874131",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "731d0896-827d-4a15-9cb2-fbc7a6a34715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "675c9b94-1bc8-4dbf-9542-77211daf9829",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "731d0896-827d-4a15-9cb2-fbc7a6a34715",
                    "LayerId": "30a6e8e0-c969-40c6-a372-0709ee7740fc"
                }
            ]
        },
        {
            "id": "f7405568-b6ce-46ff-9621-7927f8c0368d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "compositeImage": {
                "id": "1f57b562-b250-43aa-8dd5-e73196792564",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7405568-b6ce-46ff-9621-7927f8c0368d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e481aa23-7008-4449-8d20-ce1d2df30c4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7405568-b6ce-46ff-9621-7927f8c0368d",
                    "LayerId": "30a6e8e0-c969-40c6-a372-0709ee7740fc"
                }
            ]
        },
        {
            "id": "7163d9eb-00a7-4c52-82fb-50802e5c1389",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "compositeImage": {
                "id": "439be58f-88ca-4db1-946c-5622b92ba1f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7163d9eb-00a7-4c52-82fb-50802e5c1389",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3213429e-bd09-4873-8450-5ae2ceef86eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7163d9eb-00a7-4c52-82fb-50802e5c1389",
                    "LayerId": "30a6e8e0-c969-40c6-a372-0709ee7740fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 960,
    "layers": [
        {
            "id": "30a6e8e0-c969-40c6-a372-0709ee7740fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}