{
    "id": "d9744c00-a53d-4f37-b0e3-7ebcd5a86828",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01ee3fa2-ee17-4426-b6bb-1a2322009fc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9744c00-a53d-4f37-b0e3-7ebcd5a86828",
            "compositeImage": {
                "id": "fd402df2-10ec-4f2b-8822-a12e02647adc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01ee3fa2-ee17-4426-b6bb-1a2322009fc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9eaca19-6d8d-4eb6-8f8a-71120ac32403",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01ee3fa2-ee17-4426-b6bb-1a2322009fc2",
                    "LayerId": "af50ebaa-e722-4076-9002-6c82d1077a88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "af50ebaa-e722-4076-9002-6c82d1077a88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9744c00-a53d-4f37-b0e3-7ebcd5a86828",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 16,
    "yorig": 28
}