{
    "id": "050f2f17-a791-4227-9372-578461f8d3cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "middle_block_horizontal_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5d844ad-7c9f-4936-bf44-b12a5543c40f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050f2f17-a791-4227-9372-578461f8d3cb",
            "compositeImage": {
                "id": "47d469ae-2ae0-46e9-ac97-90a8d3559cac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5d844ad-7c9f-4936-bf44-b12a5543c40f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61546554-bcc2-4019-bb72-b47a82e55796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5d844ad-7c9f-4936-bf44-b12a5543c40f",
                    "LayerId": "8aa18b63-cd87-4321-8048-adf9ffea9be3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8aa18b63-cd87-4321-8048-adf9ffea9be3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "050f2f17-a791-4227-9372-578461f8d3cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}