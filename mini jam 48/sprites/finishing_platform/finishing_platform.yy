{
    "id": "9ea29323-9c92-4def-97dd-343e79863354",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "finishing_platform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a62b63ae-c0e3-4420-a78a-f1daaef41cab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ea29323-9c92-4def-97dd-343e79863354",
            "compositeImage": {
                "id": "8bd1a7aa-c964-4343-a9f6-82bc721a179b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a62b63ae-c0e3-4420-a78a-f1daaef41cab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "578bb9ee-fc6e-4bca-b5db-f084f4e95683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a62b63ae-c0e3-4420-a78a-f1daaef41cab",
                    "LayerId": "86464050-fb5b-4b08-a24a-a90c3c7939dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "86464050-fb5b-4b08-a24a-a90c3c7939dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ea29323-9c92-4def-97dd-343e79863354",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 32
}