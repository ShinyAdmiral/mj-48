{
    "id": "4720c6d2-9a1e-4dd8-bc64-9166953b8a9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_down_brown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b30cd144-9679-4d31-ac4f-42108e5237d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4720c6d2-9a1e-4dd8-bc64-9166953b8a9f",
            "compositeImage": {
                "id": "e1e97224-8349-4969-839b-f543a4114d79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b30cd144-9679-4d31-ac4f-42108e5237d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "552d8ebc-b9ca-42d2-a7b9-995e99491481",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b30cd144-9679-4d31-ac4f-42108e5237d4",
                    "LayerId": "99c89be4-fd74-430f-b4fa-bfba20e35e1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "99c89be4-fd74-430f-b4fa-bfba20e35e1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4720c6d2-9a1e-4dd8-bc64-9166953b8a9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}