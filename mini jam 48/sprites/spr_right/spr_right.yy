{
    "id": "6df2a2ce-158f-4ec7-aad1-c700c51e8b0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 14,
    "bbox_right": 33,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6e978b1-42c1-47fc-a110-08575a28ff91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df2a2ce-158f-4ec7-aad1-c700c51e8b0a",
            "compositeImage": {
                "id": "5e99c9c5-6280-4f04-9c9d-d6acf86a57ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6e978b1-42c1-47fc-a110-08575a28ff91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c739a155-e621-4e6c-9242-02b2d583b901",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6e978b1-42c1-47fc-a110-08575a28ff91",
                    "LayerId": "37e26442-c75c-4366-9905-34eca12c42e3"
                }
            ]
        },
        {
            "id": "4856a2d2-1e58-4282-b288-84230e64a7ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df2a2ce-158f-4ec7-aad1-c700c51e8b0a",
            "compositeImage": {
                "id": "61045201-277b-4753-a1e2-afe049992563",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4856a2d2-1e58-4282-b288-84230e64a7ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "337803c9-fabb-4a96-bf3a-dd17d3776eff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4856a2d2-1e58-4282-b288-84230e64a7ce",
                    "LayerId": "37e26442-c75c-4366-9905-34eca12c42e3"
                }
            ]
        },
        {
            "id": "2b9e24ec-a31f-41b8-949a-c182d1b7700f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df2a2ce-158f-4ec7-aad1-c700c51e8b0a",
            "compositeImage": {
                "id": "654763c2-6ca7-416f-8062-c41961dca69d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b9e24ec-a31f-41b8-949a-c182d1b7700f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "debb5301-cf54-4243-8c5f-75607a1c4681",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b9e24ec-a31f-41b8-949a-c182d1b7700f",
                    "LayerId": "37e26442-c75c-4366-9905-34eca12c42e3"
                }
            ]
        },
        {
            "id": "e9f8bb45-3e4f-4940-b34c-b90296f7191a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df2a2ce-158f-4ec7-aad1-c700c51e8b0a",
            "compositeImage": {
                "id": "f280d486-9ea4-4b3e-af23-8cc345dbd586",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9f8bb45-3e4f-4940-b34c-b90296f7191a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63100e8c-bc2c-4a71-9a8e-2b8ce7595437",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f8bb45-3e4f-4940-b34c-b90296f7191a",
                    "LayerId": "37e26442-c75c-4366-9905-34eca12c42e3"
                }
            ]
        },
        {
            "id": "13ac19d2-b1c6-4411-957f-ddad757276af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df2a2ce-158f-4ec7-aad1-c700c51e8b0a",
            "compositeImage": {
                "id": "b8dfbac6-c9b9-4f87-92a2-a2a0c02b3c73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13ac19d2-b1c6-4411-957f-ddad757276af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11f0c590-551c-4827-a7ad-0945baa75c91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13ac19d2-b1c6-4411-957f-ddad757276af",
                    "LayerId": "37e26442-c75c-4366-9905-34eca12c42e3"
                }
            ]
        },
        {
            "id": "64873226-6f62-4fd7-bdea-dee4fbb31dc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df2a2ce-158f-4ec7-aad1-c700c51e8b0a",
            "compositeImage": {
                "id": "f98250d0-ba7b-4245-a134-aaa164596aba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64873226-6f62-4fd7-bdea-dee4fbb31dc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7129ff76-688d-4778-a4f7-0904273f6815",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64873226-6f62-4fd7-bdea-dee4fbb31dc0",
                    "LayerId": "37e26442-c75c-4366-9905-34eca12c42e3"
                }
            ]
        },
        {
            "id": "3925dac3-2528-4213-834e-8f6551a5ee80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df2a2ce-158f-4ec7-aad1-c700c51e8b0a",
            "compositeImage": {
                "id": "86510751-fb29-4e63-90a5-d101694f68ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3925dac3-2528-4213-834e-8f6551a5ee80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5806eb5-c512-4d60-a088-96d6581ff494",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3925dac3-2528-4213-834e-8f6551a5ee80",
                    "LayerId": "37e26442-c75c-4366-9905-34eca12c42e3"
                }
            ]
        },
        {
            "id": "fbb79023-d96b-4455-aa9f-8c4c76bf6fff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df2a2ce-158f-4ec7-aad1-c700c51e8b0a",
            "compositeImage": {
                "id": "a3d1850e-5369-445f-bb41-17d86731abab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbb79023-d96b-4455-aa9f-8c4c76bf6fff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f91ba13f-90fc-4ea7-a70f-4ce5503c1e30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbb79023-d96b-4455-aa9f-8c4c76bf6fff",
                    "LayerId": "37e26442-c75c-4366-9905-34eca12c42e3"
                }
            ]
        },
        {
            "id": "7edc7cb3-1576-4eb1-a1e4-de32bf12f030",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df2a2ce-158f-4ec7-aad1-c700c51e8b0a",
            "compositeImage": {
                "id": "4d312f22-e998-4394-a1d5-212da8c60b2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7edc7cb3-1576-4eb1-a1e4-de32bf12f030",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3dfc295-ed72-412d-888b-4de2c63ab6b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7edc7cb3-1576-4eb1-a1e4-de32bf12f030",
                    "LayerId": "37e26442-c75c-4366-9905-34eca12c42e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "37e26442-c75c-4366-9905-34eca12c42e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6df2a2ce-158f-4ec7-aad1-c700c51e8b0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}