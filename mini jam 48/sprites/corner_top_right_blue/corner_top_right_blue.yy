{
    "id": "5b514c70-2230-45c5-9e93-77b12eb21deb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_top_right_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e79d1f48-b780-4521-815e-d8b7a9a50701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b514c70-2230-45c5-9e93-77b12eb21deb",
            "compositeImage": {
                "id": "12fefc3c-aec8-47a7-ae3f-560fb07f7bd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e79d1f48-b780-4521-815e-d8b7a9a50701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2027e64-aa9b-48dd-aab2-4ab5bd1f8ef5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e79d1f48-b780-4521-815e-d8b7a9a50701",
                    "LayerId": "76f2cd13-cc2d-4f3f-85ac-d7ed73dfc65c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "76f2cd13-cc2d-4f3f-85ac-d7ed73dfc65c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b514c70-2230-45c5-9e93-77b12eb21deb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}