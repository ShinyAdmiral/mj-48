{
    "id": "7ab8c55b-79fb-4696-bce4-8eddf3ce6348",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ui_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 105,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "acc91041-cfda-4890-8cf6-65d5b8d25c73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ab8c55b-79fb-4696-bce4-8eddf3ce6348",
            "compositeImage": {
                "id": "79b75234-1494-44a5-ad09-950f72427cef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acc91041-cfda-4890-8cf6-65d5b8d25c73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2fde5b9-24f1-4d31-8d78-b59b2333012e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acc91041-cfda-4890-8cf6-65d5b8d25c73",
                    "LayerId": "e598dbf5-41be-43f6-85d0-c41996a20781"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "e598dbf5-41be-43f6-85d0-c41996a20781",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ab8c55b-79fb-4696-bce4-8eddf3ce6348",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 106,
    "xorig": 0,
    "yorig": 0
}