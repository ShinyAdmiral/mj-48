{
    "id": "6a0beffb-d423-4431-8575-7e07bcbbf58b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_bottom_right_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9db9da19-da28-4514-b640-ef7ba99fd144",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a0beffb-d423-4431-8575-7e07bcbbf58b",
            "compositeImage": {
                "id": "49bc83de-0747-47cc-8c76-18350b18a738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9db9da19-da28-4514-b640-ef7ba99fd144",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76e68dbf-9782-43da-976a-e9708a8caa2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9db9da19-da28-4514-b640-ef7ba99fd144",
                    "LayerId": "e0359701-6bc4-49b5-8fba-674541d90653"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e0359701-6bc4-49b5-8fba-674541d90653",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a0beffb-d423-4431-8575-7e07bcbbf58b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}