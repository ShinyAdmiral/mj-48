{
    "id": "add03e69-ec2a-4178-beb1-08d350ba4d24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "HelpButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab9b2692-f8cd-424d-9b72-087b505c03b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "add03e69-ec2a-4178-beb1-08d350ba4d24",
            "compositeImage": {
                "id": "852e201b-c679-4a30-a03c-e6ab0649e793",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab9b2692-f8cd-424d-9b72-087b505c03b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80eb6316-cd39-471d-a46b-b174b1736dbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab9b2692-f8cd-424d-9b72-087b505c03b9",
                    "LayerId": "46237dd4-d30b-4bbd-a7ab-d17e31cb6c01"
                }
            ]
        },
        {
            "id": "978f2f50-b9b4-420c-a546-17ff548cec4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "add03e69-ec2a-4178-beb1-08d350ba4d24",
            "compositeImage": {
                "id": "f06b3245-5010-4ff1-945a-b8146131d7b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "978f2f50-b9b4-420c-a546-17ff548cec4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "570121ed-6ab1-4d14-a3f3-79ca38c8e267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "978f2f50-b9b4-420c-a546-17ff548cec4e",
                    "LayerId": "46237dd4-d30b-4bbd-a7ab-d17e31cb6c01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "46237dd4-d30b-4bbd-a7ab-d17e31cb6c01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "add03e69-ec2a-4178-beb1-08d350ba4d24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}