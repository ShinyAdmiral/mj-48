{
    "id": "d8fd31b7-35f6-4076-9cf2-86b5c5c6ce93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "damage_particle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 66,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "758ae8b9-956b-4f93-bb33-0f98e23bdeec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8fd31b7-35f6-4076-9cf2-86b5c5c6ce93",
            "compositeImage": {
                "id": "920ea49f-7736-4454-8bc9-2ce18f1f82fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "758ae8b9-956b-4f93-bb33-0f98e23bdeec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "221e21c3-c69f-44be-b16c-a1ada67153fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "758ae8b9-956b-4f93-bb33-0f98e23bdeec",
                    "LayerId": "be1aa108-90b6-4e9b-a4f0-a96cdb618bc9"
                }
            ]
        },
        {
            "id": "ec534ea2-c38b-46c8-9de1-6bcf885495ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8fd31b7-35f6-4076-9cf2-86b5c5c6ce93",
            "compositeImage": {
                "id": "e5aa9193-a1bc-42eb-a225-72e3a3e0db76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec534ea2-c38b-46c8-9de1-6bcf885495ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95ecbd0e-6695-4b7d-a2d0-a58b5cbd9821",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec534ea2-c38b-46c8-9de1-6bcf885495ee",
                    "LayerId": "be1aa108-90b6-4e9b-a4f0-a96cdb618bc9"
                }
            ]
        },
        {
            "id": "080325d6-8a21-4668-9afa-f6945860f650",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8fd31b7-35f6-4076-9cf2-86b5c5c6ce93",
            "compositeImage": {
                "id": "8ff407a0-c3d9-4850-8f59-cb98b5d97393",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "080325d6-8a21-4668-9afa-f6945860f650",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70c70482-1e3a-4591-a47e-5c85428a9673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "080325d6-8a21-4668-9afa-f6945860f650",
                    "LayerId": "be1aa108-90b6-4e9b-a4f0-a96cdb618bc9"
                }
            ]
        },
        {
            "id": "fb8711a1-2ddd-4aeb-b437-8c8109166d9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8fd31b7-35f6-4076-9cf2-86b5c5c6ce93",
            "compositeImage": {
                "id": "00b65972-87a2-465a-b375-77eb61495781",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb8711a1-2ddd-4aeb-b437-8c8109166d9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe9322b7-7d26-4b7c-b1f9-5fb9ed19aaa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb8711a1-2ddd-4aeb-b437-8c8109166d9d",
                    "LayerId": "be1aa108-90b6-4e9b-a4f0-a96cdb618bc9"
                }
            ]
        },
        {
            "id": "3ff52067-6fb8-4f7b-ad64-f18af5930878",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8fd31b7-35f6-4076-9cf2-86b5c5c6ce93",
            "compositeImage": {
                "id": "fad00383-6431-461f-b407-9f3bc996488a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ff52067-6fb8-4f7b-ad64-f18af5930878",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2dded06-b061-4f15-9890-7fb8b073aa9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ff52067-6fb8-4f7b-ad64-f18af5930878",
                    "LayerId": "be1aa108-90b6-4e9b-a4f0-a96cdb618bc9"
                }
            ]
        },
        {
            "id": "9df68fbe-df35-4233-8ffc-fdcac255cdf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8fd31b7-35f6-4076-9cf2-86b5c5c6ce93",
            "compositeImage": {
                "id": "0ee4a647-b788-4ccd-8376-47ffbd8fa763",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9df68fbe-df35-4233-8ffc-fdcac255cdf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "038c4865-bb6e-40dd-b401-90233ec0921b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9df68fbe-df35-4233-8ffc-fdcac255cdf3",
                    "LayerId": "be1aa108-90b6-4e9b-a4f0-a96cdb618bc9"
                }
            ]
        },
        {
            "id": "3b3a8587-bb35-402d-a467-2d843f0b4750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8fd31b7-35f6-4076-9cf2-86b5c5c6ce93",
            "compositeImage": {
                "id": "5f5f544f-ade9-46b0-a77e-9ca2bc0f8911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b3a8587-bb35-402d-a467-2d843f0b4750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a29ad016-4544-4e04-9b42-cf4c8e468b37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b3a8587-bb35-402d-a467-2d843f0b4750",
                    "LayerId": "be1aa108-90b6-4e9b-a4f0-a96cdb618bc9"
                }
            ]
        },
        {
            "id": "1f42661c-99b2-4eb6-a529-76ab10848186",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8fd31b7-35f6-4076-9cf2-86b5c5c6ce93",
            "compositeImage": {
                "id": "053ba0cc-fa42-43e6-8288-f769402b66ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f42661c-99b2-4eb6-a529-76ab10848186",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d66e597b-2e6c-4000-b962-5943b561d019",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f42661c-99b2-4eb6-a529-76ab10848186",
                    "LayerId": "be1aa108-90b6-4e9b-a4f0-a96cdb618bc9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "be1aa108-90b6-4e9b-a4f0-a96cdb618bc9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8fd31b7-35f6-4076-9cf2-86b5c5c6ce93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 67,
    "xorig": 33,
    "yorig": 24
}