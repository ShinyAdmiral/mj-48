{
    "id": "31b01702-d333-43b9-ae7b-ee2377e7dd05",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_bottom_left_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26cef973-e9fc-4f03-815d-8660cc0ed0dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31b01702-d333-43b9-ae7b-ee2377e7dd05",
            "compositeImage": {
                "id": "bb350200-6fdc-4de3-b376-c7ba50a14ee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26cef973-e9fc-4f03-815d-8660cc0ed0dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db057c58-3380-43d3-9cc5-90aab6f2f8a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26cef973-e9fc-4f03-815d-8660cc0ed0dc",
                    "LayerId": "aee36a1f-e6f0-46e0-8a54-58562e816e9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "aee36a1f-e6f0-46e0-8a54-58562e816e9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31b01702-d333-43b9-ae7b-ee2377e7dd05",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}