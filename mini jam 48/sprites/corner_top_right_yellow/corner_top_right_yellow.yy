{
    "id": "bc84668b-4153-4225-b4a3-cec86209561b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_top_right_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ca5ae85-4806-4ef7-b052-6fe9079199d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc84668b-4153-4225-b4a3-cec86209561b",
            "compositeImage": {
                "id": "ae5d35bc-06ce-44bb-8b06-a9e24989097a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ca5ae85-4806-4ef7-b052-6fe9079199d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "689d4c85-c17c-4738-9ebd-0aa7f2688646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ca5ae85-4806-4ef7-b052-6fe9079199d9",
                    "LayerId": "9a458574-8ae3-4abd-a2ae-ccce5257f353"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9a458574-8ae3-4abd-a2ae-ccce5257f353",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc84668b-4153-4225-b4a3-cec86209561b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}