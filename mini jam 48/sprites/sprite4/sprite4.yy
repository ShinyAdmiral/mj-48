{
    "id": "13f9e085-2950-4de3-b4e9-469a410c583d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 74,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "583c0598-e060-4be6-b3f1-ee8b9b5022fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f9e085-2950-4de3-b4e9-469a410c583d",
            "compositeImage": {
                "id": "321b6b2b-85d7-4970-a3ee-ca807de51d22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "583c0598-e060-4be6-b3f1-ee8b9b5022fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0514ae6-d56e-44ed-8084-d7dd5fec1180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "583c0598-e060-4be6-b3f1-ee8b9b5022fa",
                    "LayerId": "dacaf58c-ac50-4960-9598-dae5e46aa291"
                }
            ]
        },
        {
            "id": "0c7dcc73-23d2-4a9a-b478-9ea4c029fba8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f9e085-2950-4de3-b4e9-469a410c583d",
            "compositeImage": {
                "id": "4a6724eb-d81f-47a0-bb45-e4829169c44a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c7dcc73-23d2-4a9a-b478-9ea4c029fba8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0532f17-b34c-4965-81be-f3bcdfcbca34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c7dcc73-23d2-4a9a-b478-9ea4c029fba8",
                    "LayerId": "dacaf58c-ac50-4960-9598-dae5e46aa291"
                }
            ]
        },
        {
            "id": "ace673a3-1ae5-49f4-8da2-b7a7dda2ec5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f9e085-2950-4de3-b4e9-469a410c583d",
            "compositeImage": {
                "id": "5d0d98c7-7d1d-467f-aece-5c5852e655b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ace673a3-1ae5-49f4-8da2-b7a7dda2ec5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b4c62a9-c67d-478a-940a-34bb85b97197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ace673a3-1ae5-49f4-8da2-b7a7dda2ec5b",
                    "LayerId": "dacaf58c-ac50-4960-9598-dae5e46aa291"
                }
            ]
        },
        {
            "id": "3fe76584-319a-4ce5-ae9b-b4e50f393da4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f9e085-2950-4de3-b4e9-469a410c583d",
            "compositeImage": {
                "id": "eb011223-af52-4ff8-a615-bd2be3d4cda4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fe76584-319a-4ce5-ae9b-b4e50f393da4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37ecd357-e1ef-4d3e-8385-0aed99f6fd13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fe76584-319a-4ce5-ae9b-b4e50f393da4",
                    "LayerId": "dacaf58c-ac50-4960-9598-dae5e46aa291"
                }
            ]
        },
        {
            "id": "21fdf4a0-7734-4eff-b06c-c00347a0f1ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f9e085-2950-4de3-b4e9-469a410c583d",
            "compositeImage": {
                "id": "54b3b34c-06e3-418d-8481-1f42c80c3cfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21fdf4a0-7734-4eff-b06c-c00347a0f1ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "844ab1ae-e0f8-49c9-846f-fbc4f70708f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21fdf4a0-7734-4eff-b06c-c00347a0f1ae",
                    "LayerId": "dacaf58c-ac50-4960-9598-dae5e46aa291"
                }
            ]
        },
        {
            "id": "75be54ce-84e5-4a56-a41d-530c18418476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f9e085-2950-4de3-b4e9-469a410c583d",
            "compositeImage": {
                "id": "b86bdb72-556f-4d9f-8382-b747619ca0ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75be54ce-84e5-4a56-a41d-530c18418476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d478fcd-f85d-4cc1-8634-71729fd20e47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75be54ce-84e5-4a56-a41d-530c18418476",
                    "LayerId": "dacaf58c-ac50-4960-9598-dae5e46aa291"
                }
            ]
        },
        {
            "id": "7750689c-8f18-46f2-8363-e638e329f340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f9e085-2950-4de3-b4e9-469a410c583d",
            "compositeImage": {
                "id": "0e750499-6170-46c3-b63f-9cd5058c5816",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7750689c-8f18-46f2-8363-e638e329f340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df9a89f9-0be4-4ba3-b968-c5c7b9a838e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7750689c-8f18-46f2-8363-e638e329f340",
                    "LayerId": "dacaf58c-ac50-4960-9598-dae5e46aa291"
                }
            ]
        },
        {
            "id": "eb05c889-6358-4b16-a461-4deb409bf582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f9e085-2950-4de3-b4e9-469a410c583d",
            "compositeImage": {
                "id": "d77bf00a-99be-4836-8577-33fef45e1112",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb05c889-6358-4b16-a461-4deb409bf582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eb5d582-11af-47de-9e5d-996d767a371b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb05c889-6358-4b16-a461-4deb409bf582",
                    "LayerId": "dacaf58c-ac50-4960-9598-dae5e46aa291"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 72,
    "layers": [
        {
            "id": "dacaf58c-ac50-4960-9598-dae5e46aa291",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13f9e085-2950-4de3-b4e9-469a410c583d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 36
}