{
    "id": "3cb4aaeb-f413-4967-a0f4-87993949d242",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2df89d9-a591-432d-a5d5-0471cc24b5d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cb4aaeb-f413-4967-a0f4-87993949d242",
            "compositeImage": {
                "id": "41704eaa-f4b7-490c-a71a-b0ec7275870f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2df89d9-a591-432d-a5d5-0471cc24b5d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fba5ce58-ad0a-4b05-a3cd-41adba5d1d7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2df89d9-a591-432d-a5d5-0471cc24b5d1",
                    "LayerId": "d06cde14-cce2-41da-8010-49c61bfa3b31"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 32,
    "layers": [
        {
            "id": "d06cde14-cce2-41da-8010-49c61bfa3b31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3cb4aaeb-f413-4967-a0f4-87993949d242",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}