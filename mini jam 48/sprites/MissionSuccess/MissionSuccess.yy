{
    "id": "ffd29366-8958-41e2-b55b-6d771deb1aa8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "MissionSuccess",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 4,
    "bbox_right": 315,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19147118-d09a-49e4-8bbf-29f1c0b55ac6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffd29366-8958-41e2-b55b-6d771deb1aa8",
            "compositeImage": {
                "id": "100a95f0-65bb-452c-9c63-781bb128241f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19147118-d09a-49e4-8bbf-29f1c0b55ac6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "689172cc-452d-40db-aa25-9effde621760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19147118-d09a-49e4-8bbf-29f1c0b55ac6",
                    "LayerId": "0151e9ab-ffea-47ef-bce1-aa9133937cfd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "0151e9ab-ffea-47ef-bce1-aa9133937cfd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffd29366-8958-41e2-b55b-6d771deb1aa8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 165,
    "yorig": 74
}