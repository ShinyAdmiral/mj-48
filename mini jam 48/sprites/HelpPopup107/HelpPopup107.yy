{
    "id": "ee08f5cd-eb23-45ac-8bf6-ca0c82df4e89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "HelpPopup107",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 287,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62534fb8-1ccb-43fc-8a34-5b4bc2971a0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee08f5cd-eb23-45ac-8bf6-ca0c82df4e89",
            "compositeImage": {
                "id": "2210ced7-a78f-4e72-a185-151f29e45a36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62534fb8-1ccb-43fc-8a34-5b4bc2971a0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aae86f8-29b2-4ee9-90b9-f2f96ca6eb6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62534fb8-1ccb-43fc-8a34-5b4bc2971a0e",
                    "LayerId": "77967d0c-d8b1-4b18-b856-d25d25430ed1"
                }
            ]
        },
        {
            "id": "719217cc-9ac8-4cc8-89a2-8545235fc283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee08f5cd-eb23-45ac-8bf6-ca0c82df4e89",
            "compositeImage": {
                "id": "e071b5a3-22a3-499c-b915-814b363a59a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "719217cc-9ac8-4cc8-89a2-8545235fc283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "033d9d44-6005-42a6-8144-a2dc5116e9a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "719217cc-9ac8-4cc8-89a2-8545235fc283",
                    "LayerId": "77967d0c-d8b1-4b18-b856-d25d25430ed1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "77967d0c-d8b1-4b18-b856-d25d25430ed1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee08f5cd-eb23-45ac-8bf6-ca0c82df4e89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 288,
    "xorig": 144,
    "yorig": 112
}