{
    "id": "7e777575-02b7-4a07-95fb-50eeb2a6aaf1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "MenuLogo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 11,
    "bbox_right": 308,
    "bbox_top": 51,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3a44d0a-dd7e-4602-9f89-f4c54fdee7cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e777575-02b7-4a07-95fb-50eeb2a6aaf1",
            "compositeImage": {
                "id": "4265af4a-f66b-45a0-a51d-5c82507d3c46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3a44d0a-dd7e-4602-9f89-f4c54fdee7cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e121d999-e984-443b-97db-9d6d2315a016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3a44d0a-dd7e-4602-9f89-f4c54fdee7cf",
                    "LayerId": "10794dac-584e-4776-a13f-8f9098221225"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "10794dac-584e-4776-a13f-8f9098221225",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e777575-02b7-4a07-95fb-50eeb2a6aaf1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 75
}