{
    "id": "b9602c35-7e95-4fa1-ad20-d2d441a4b465",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "HP__ProgressBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 452,
    "bbox_left": 8,
    "bbox_right": 33,
    "bbox_top": 217,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e5d166d-fd21-4688-8cbf-c16cd066a04c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9602c35-7e95-4fa1-ad20-d2d441a4b465",
            "compositeImage": {
                "id": "75114321-526f-4803-bef2-03f442f156bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e5d166d-fd21-4688-8cbf-c16cd066a04c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49ce9279-46c9-4134-b02b-951024b0299c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e5d166d-fd21-4688-8cbf-c16cd066a04c",
                    "LayerId": "7f9f3caf-e82a-4c58-91a3-29049cd82b66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 503,
    "layers": [
        {
            "id": "7f9f3caf-e82a-4c58-91a3-29049cd82b66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9602c35-7e95-4fa1-ad20-d2d441a4b465",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 94,
    "xorig": 0,
    "yorig": 0
}