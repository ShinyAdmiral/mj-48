{
    "id": "762154c6-0465-4f6d-8ea6-3c9d8c16d0bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "end_block_left_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b06d1ff3-9c5f-497f-98c8-0b87bfb7eb3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "762154c6-0465-4f6d-8ea6-3c9d8c16d0bb",
            "compositeImage": {
                "id": "47f68faf-eba1-4d00-9dc8-0652f901392b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b06d1ff3-9c5f-497f-98c8-0b87bfb7eb3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1d43f23-1db7-41cb-8563-91dd3165b8c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b06d1ff3-9c5f-497f-98c8-0b87bfb7eb3a",
                    "LayerId": "f745a2c9-0637-4021-927a-fbfa5c0c6b85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f745a2c9-0637-4021-927a-fbfa5c0c6b85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "762154c6-0465-4f6d-8ea6-3c9d8c16d0bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}