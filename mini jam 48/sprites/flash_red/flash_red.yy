{
    "id": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flash_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ff1f643-0d01-4b35-90cd-7cc5b2e2e720",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "3895d1c4-123e-49cd-81a7-83d65dd18030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ff1f643-0d01-4b35-90cd-7cc5b2e2e720",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39bb508c-5977-4489-852e-7f76aba5f788",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ff1f643-0d01-4b35-90cd-7cc5b2e2e720",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "eb239a58-b15a-41e3-81a9-703ccf17c7cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "20459785-7b04-46ba-bf67-4ffa9fdb38c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb239a58-b15a-41e3-81a9-703ccf17c7cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7514f04-5f4d-4784-9bd7-a9ea282aed01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb239a58-b15a-41e3-81a9-703ccf17c7cb",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "9eca6714-4d88-4395-b76c-30c56d24f678",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "5ace29ff-3607-47c9-85a5-4188f50f1783",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eca6714-4d88-4395-b76c-30c56d24f678",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd9142fe-1716-4e18-8cca-c5e2b446bcca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eca6714-4d88-4395-b76c-30c56d24f678",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "25e91c2d-ce77-4271-b0f7-0d966ea80a8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "0d5182b9-f38e-4078-8361-e68265d70b5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25e91c2d-ce77-4271-b0f7-0d966ea80a8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "123d66ca-3fc8-42a6-b7f4-ec2d1a6ca4db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25e91c2d-ce77-4271-b0f7-0d966ea80a8f",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "d6209f3a-10a7-4377-8998-f872e8edde7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "2f65d7c7-b1c3-467f-843a-0aa3b8b741dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6209f3a-10a7-4377-8998-f872e8edde7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a4727d4-2111-4a09-b1fd-68f854fbd0a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6209f3a-10a7-4377-8998-f872e8edde7e",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "7d801ec9-919b-4f22-9b6b-ad140b6cfb0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "02ea9ed8-08f9-457b-80f8-a5c0d9b79e10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d801ec9-919b-4f22-9b6b-ad140b6cfb0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5242eba7-22a5-44d3-9dca-af2440d899fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d801ec9-919b-4f22-9b6b-ad140b6cfb0f",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "c99e2168-2fea-451e-9f1d-082b4d885cfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "fe7f48ed-5d0d-4a7f-b3c9-931d3c23c01c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c99e2168-2fea-451e-9f1d-082b4d885cfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bf9248a-c111-4424-908a-8edc9c2c2743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c99e2168-2fea-451e-9f1d-082b4d885cfa",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "6179ba95-d891-4fff-b042-722aa41d1071",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "78e07a05-3ba1-4841-986b-8f0a767890d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6179ba95-d891-4fff-b042-722aa41d1071",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e759175-39b8-49a8-8968-b38b1d1de18e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6179ba95-d891-4fff-b042-722aa41d1071",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "a1dfb889-7f61-4ef6-b1b1-1450f14d24a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "484c15e2-e5ad-4953-9625-e116ec26e615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1dfb889-7f61-4ef6-b1b1-1450f14d24a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0737921-8d91-463f-84e3-d6eee0c745ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1dfb889-7f61-4ef6-b1b1-1450f14d24a9",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "391ee048-ba32-4300-84ae-52fc249b8212",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "100d1bbb-6f52-4a2b-b2c2-72f8539d88e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "391ee048-ba32-4300-84ae-52fc249b8212",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e66da2a-e636-4ee4-a45f-6c92ee875cba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "391ee048-ba32-4300-84ae-52fc249b8212",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "3da85d03-904b-4170-9bf8-12cdf0167488",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "a64c4f48-1b7f-4833-bc75-240a3362d8b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3da85d03-904b-4170-9bf8-12cdf0167488",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71d81add-7549-4d6a-a9db-d446922449dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3da85d03-904b-4170-9bf8-12cdf0167488",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "997818f8-fa03-4be6-8def-6107da79003f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "988fd35d-cb78-4059-9b04-5a0694f8895d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "997818f8-fa03-4be6-8def-6107da79003f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2004533d-70a1-4523-83f2-e7d8aa0af017",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "997818f8-fa03-4be6-8def-6107da79003f",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "6f218218-10bc-456b-81bb-b626c5a28026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "0bb594b9-626b-4b0c-a6bc-5f865610664d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f218218-10bc-456b-81bb-b626c5a28026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fdb7e3c-cb02-4113-bfcf-41b186a02c88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f218218-10bc-456b-81bb-b626c5a28026",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "775008b4-d6ef-43d6-ad95-4c3d41dbb03f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "8ee46dad-147a-49da-8fed-83866dab37f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "775008b4-d6ef-43d6-ad95-4c3d41dbb03f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "903b91ea-6f70-425c-ae2f-8009241d488f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "775008b4-d6ef-43d6-ad95-4c3d41dbb03f",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "937f559a-f847-471e-b725-d7fe9f98d4a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "a4b5542b-730e-4e54-9aef-0da39af805ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "937f559a-f847-471e-b725-d7fe9f98d4a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1748d69-e2c5-4eaa-8653-44532264d3eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "937f559a-f847-471e-b725-d7fe9f98d4a6",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "fcb1071b-674c-4f57-9a79-9d14e37aa13b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "635cf24f-898b-4456-b195-1842ba3bdf49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcb1071b-674c-4f57-9a79-9d14e37aa13b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0db7fcf0-bc06-49fd-8082-ce722231f0e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcb1071b-674c-4f57-9a79-9d14e37aa13b",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "44562b58-8a82-497a-893b-42c4abc79084",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "1408a373-3e57-4971-a2fb-1257cc279fe2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44562b58-8a82-497a-893b-42c4abc79084",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30f28eba-c973-4a61-81f9-9129383e84a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44562b58-8a82-497a-893b-42c4abc79084",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        },
        {
            "id": "883cb8ff-ea51-410d-b436-603eea4d4fc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "compositeImage": {
                "id": "45f17702-47c8-4699-86a4-61c78f385b26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "883cb8ff-ea51-410d-b436-603eea4d4fc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6db503c9-de0e-4530-91e6-f3118b6ab98e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "883cb8ff-ea51-410d-b436-603eea4d4fc7",
                    "LayerId": "0826918c-81da-43ee-af0f-86394a82f708"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "0826918c-81da-43ee-af0f-86394a82f708",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 21,
    "yorig": 20
}