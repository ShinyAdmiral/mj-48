{
    "id": "1f089b2e-63bc-434f-9820-77edbd2781dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "corner_bottom_left_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93423ae1-e98d-4ad6-8c0e-7e126534acac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f089b2e-63bc-434f-9820-77edbd2781dd",
            "compositeImage": {
                "id": "38650450-a7d8-4199-8b95-de49983d573d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93423ae1-e98d-4ad6-8c0e-7e126534acac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36031296-1ac3-4311-9b0d-8313444390a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93423ae1-e98d-4ad6-8c0e-7e126534acac",
                    "LayerId": "dc3af2f5-0251-4f59-8300-9a50fa27d95c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dc3af2f5-0251-4f59-8300-9a50fa27d95c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f089b2e-63bc-434f-9820-77edbd2781dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}