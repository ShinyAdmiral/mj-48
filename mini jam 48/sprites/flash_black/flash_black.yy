{
    "id": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flash_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65c09d0f-83c0-43a2-837e-eaf724169152",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "697dcd32-840c-4b73-82f7-a160feaf3e6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65c09d0f-83c0-43a2-837e-eaf724169152",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4a00f54-0990-4e1b-bbf8-f9bb7491da87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65c09d0f-83c0-43a2-837e-eaf724169152",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "6dd5f5be-b5bc-4d98-b214-f8606ed0076d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "baed329e-d8c5-4f28-a610-7bc635e9d322",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dd5f5be-b5bc-4d98-b214-f8606ed0076d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d17371c3-910a-4c5c-83a6-47ad787f25a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dd5f5be-b5bc-4d98-b214-f8606ed0076d",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "17370050-435f-4c1d-bee1-8d118567b8e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "492a0908-9898-4270-8779-b406171b2f16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17370050-435f-4c1d-bee1-8d118567b8e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "223b447c-b9fd-4012-b86a-4795f460bfa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17370050-435f-4c1d-bee1-8d118567b8e9",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "f5033b5f-e8b4-4a78-bdb1-b52fc80f9114",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "3d16baf2-4d9d-4375-8651-a696e738362a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5033b5f-e8b4-4a78-bdb1-b52fc80f9114",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dd3496a-aae5-4c5b-b879-d532ef5d2f95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5033b5f-e8b4-4a78-bdb1-b52fc80f9114",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "84cc44a6-5677-4925-8eef-26cfaf3fc474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "3e058094-2098-4cf1-9547-bdbed0ce37e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84cc44a6-5677-4925-8eef-26cfaf3fc474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1918140d-d646-4c72-96b8-f31b457b2a91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84cc44a6-5677-4925-8eef-26cfaf3fc474",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "39bc8f71-907a-4a9b-9c58-3d4e8a88f61a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "3e85c410-76ec-4c15-bbb9-9bce9e929f8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39bc8f71-907a-4a9b-9c58-3d4e8a88f61a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3269f4d5-af3c-4710-85cc-26a0c543d26d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39bc8f71-907a-4a9b-9c58-3d4e8a88f61a",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "4ebd39e2-8087-4aea-a1d6-9e9fbc274846",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "b6eae83e-6813-4ac9-8186-20e0ef96c18e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ebd39e2-8087-4aea-a1d6-9e9fbc274846",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a140a22-3e85-4d45-b76f-9990d57ce2a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ebd39e2-8087-4aea-a1d6-9e9fbc274846",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "3592e1ab-b264-448c-ac0a-ef7a9d965034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "4668438f-c0cd-47da-977a-41e3f7b81cc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3592e1ab-b264-448c-ac0a-ef7a9d965034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8426e194-b691-40c7-aff7-00a8838daa54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3592e1ab-b264-448c-ac0a-ef7a9d965034",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "6572834d-f045-43fc-a175-0e71b26aa063",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "0748112b-b33b-4641-920f-88323042da4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6572834d-f045-43fc-a175-0e71b26aa063",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9f6b82d-9201-428c-bde2-ece08aca2d75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6572834d-f045-43fc-a175-0e71b26aa063",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "6ae8aa5e-c23d-46bb-9391-28d6d8409964",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "37c5282c-f2a3-455a-afe6-f0e552180b2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ae8aa5e-c23d-46bb-9391-28d6d8409964",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e67bff5-0d6c-4cfc-89f4-3a6259f24e6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ae8aa5e-c23d-46bb-9391-28d6d8409964",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "9054a596-06dc-4a6d-a028-02e65a7252b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "461be4f7-ae27-4228-b7d8-b1dd3f449c69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9054a596-06dc-4a6d-a028-02e65a7252b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "168af232-e9ee-404c-a213-9bc54c950c9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9054a596-06dc-4a6d-a028-02e65a7252b5",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "7667f0af-2771-4819-825b-23694cd694a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "7db931ee-5788-4d3b-9ccf-b33fc5887070",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7667f0af-2771-4819-825b-23694cd694a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f35f91b4-0519-4f77-a930-813c2154fdcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7667f0af-2771-4819-825b-23694cd694a8",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "29f74a7d-2323-49fa-8fb4-d425c69b5ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "955156c6-a5cd-45b6-91f2-93881580872a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29f74a7d-2323-49fa-8fb4-d425c69b5ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a73ff12a-8fca-467e-b865-ed577d2caa41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29f74a7d-2323-49fa-8fb4-d425c69b5ce3",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "8d307995-715b-4900-8336-80f6632e5a8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "8bf1500c-dac1-40d6-9b71-0bd5862ba478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d307995-715b-4900-8336-80f6632e5a8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b1186eb-6354-46e3-995f-a8da4bc5b5f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d307995-715b-4900-8336-80f6632e5a8b",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "01afdd59-4f06-4703-b013-125c987d4d80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "6b38b815-5c1b-4159-a2d5-050090d7f498",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01afdd59-4f06-4703-b013-125c987d4d80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a76fd6f-b532-48bb-a56a-85996d754b7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01afdd59-4f06-4703-b013-125c987d4d80",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "6fa75984-09df-4499-a3e0-b33dc771d3b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "d03ebfc7-0fba-4dce-a428-94f132190d9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fa75984-09df-4499-a3e0-b33dc771d3b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ece4ae99-3c58-4213-81cb-d7f884d3ff45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fa75984-09df-4499-a3e0-b33dc771d3b2",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "21b0a52b-c7ec-4f12-85a1-c5c505dae280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "e4ac92cd-da11-4e4a-ae4e-445bc61962b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21b0a52b-c7ec-4f12-85a1-c5c505dae280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e5bc66a-955d-48f9-bf75-54d327ae0a42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21b0a52b-c7ec-4f12-85a1-c5c505dae280",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        },
        {
            "id": "ad7c01e5-d170-42c8-89d8-f3d581aae9d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "compositeImage": {
                "id": "55c3ad7b-e6db-4869-85bc-ade2d080e1fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad7c01e5-d170-42c8-89d8-f3d581aae9d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82134b65-5f1b-4e2a-85b7-54aec15f6647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad7c01e5-d170-42c8-89d8-f3d581aae9d6",
                    "LayerId": "0473c93c-ea29-472b-9b21-9e41c286635e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "0473c93c-ea29-472b-9b21-9e41c286635e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 21,
    "yorig": 20
}