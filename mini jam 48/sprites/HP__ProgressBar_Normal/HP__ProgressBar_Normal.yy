{
    "id": "36faf1d6-c802-4682-a675-f9c1c184a635",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "HP__ProgressBar_Normal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 501,
    "bbox_left": 1,
    "bbox_right": 92,
    "bbox_top": 118,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd9fa100-4712-4c2b-aef0-b0a7f65a7712",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "c00d73d5-73d1-4162-92c5-24214eba4ed7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd9fa100-4712-4c2b-aef0-b0a7f65a7712",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d009cab1-10d4-4bb7-910f-57fbc9b88745",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd9fa100-4712-4c2b-aef0-b0a7f65a7712",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "5edffce0-e9af-4e7f-b8fc-04dfc2276916",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "9a1fef66-657a-440f-b895-8f512d64dd56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5edffce0-e9af-4e7f-b8fc-04dfc2276916",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7abdb4c1-f774-41c1-b654-897318ac4ccd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5edffce0-e9af-4e7f-b8fc-04dfc2276916",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "076fd79e-7aa4-45af-98e3-ca791a3090b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "9ef3944d-d271-4592-8715-c307efc75ee5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "076fd79e-7aa4-45af-98e3-ca791a3090b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d18a1b0-9f74-4ba8-8bf5-5f760de71d96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "076fd79e-7aa4-45af-98e3-ca791a3090b4",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "6b5da86e-4470-4400-855e-6a5c6a8666d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "6be9f241-3ef0-47f5-b422-a82c6fca5f4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b5da86e-4470-4400-855e-6a5c6a8666d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb42aa3d-5a47-4065-a026-6be899af7ba1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b5da86e-4470-4400-855e-6a5c6a8666d2",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "740694b9-52b0-40d2-aaee-a9ed1ae63535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "d406eb70-b909-4104-80c2-f11915345640",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "740694b9-52b0-40d2-aaee-a9ed1ae63535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae438b32-4b97-4b98-b73a-068bb84b6550",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "740694b9-52b0-40d2-aaee-a9ed1ae63535",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "305618a5-8504-49f9-8677-707d6c82d407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "0d954665-6f0d-4da9-a604-94b2c179f238",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "305618a5-8504-49f9-8677-707d6c82d407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3e538a2-605a-4022-bc97-ccddb2de5052",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "305618a5-8504-49f9-8677-707d6c82d407",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "5635c98f-b7fd-40aa-a53a-ade3cf418b30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "1c5784e1-57ac-41a8-a665-fcdfa977fd5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5635c98f-b7fd-40aa-a53a-ade3cf418b30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff63fc65-67f6-49e1-a99c-6bb2487bc434",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5635c98f-b7fd-40aa-a53a-ade3cf418b30",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "ef339724-cff6-4644-a64d-def4da0e72dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "9f51c7f1-cc8c-406a-9196-3315ef2c38a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef339724-cff6-4644-a64d-def4da0e72dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29296b90-3f74-40ae-ac12-01210104bd9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef339724-cff6-4644-a64d-def4da0e72dd",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "21966310-ca9c-40e1-a938-862aa6f63fee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "3f452754-9e75-476b-bedd-cbad85086bbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21966310-ca9c-40e1-a938-862aa6f63fee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b6eea7c-b582-4c0f-a0b7-865aae079f2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21966310-ca9c-40e1-a938-862aa6f63fee",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "c7c1eb4b-7406-4926-8f0b-24f55ea8efd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "538de095-26d6-4be6-8b99-09f346370030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7c1eb4b-7406-4926-8f0b-24f55ea8efd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a8c2f2c-ebe0-4d34-9b58-0ab80bd65f48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7c1eb4b-7406-4926-8f0b-24f55ea8efd0",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "056da8b2-4589-4769-a781-8786e43c5e77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "3b7a3f16-704c-4241-9cbf-91d8f42e3677",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "056da8b2-4589-4769-a781-8786e43c5e77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6b659fa-69eb-4826-a370-692e1f96205c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "056da8b2-4589-4769-a781-8786e43c5e77",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "31b0b729-e6ff-44ad-aafc-3029c5d3fe60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "1e29ffee-0d1c-4dd4-8564-e7c37632ab4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31b0b729-e6ff-44ad-aafc-3029c5d3fe60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cb3348d-251e-4a9f-9dae-2388123a6fdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31b0b729-e6ff-44ad-aafc-3029c5d3fe60",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "120adde7-c655-4dea-b989-1af06afe9cdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "64c54abc-4d6b-4c9f-87a6-ff199621c12b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "120adde7-c655-4dea-b989-1af06afe9cdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef05a52d-4f5b-4815-b68a-d4ef9de670ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "120adde7-c655-4dea-b989-1af06afe9cdf",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "b5fe81ea-ecdd-4a0d-84e2-0f3be23f23fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "3a74f813-6400-438b-b02a-62cf87f681a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5fe81ea-ecdd-4a0d-84e2-0f3be23f23fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbdefbab-0d99-467b-932a-30ada2c57fa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5fe81ea-ecdd-4a0d-84e2-0f3be23f23fc",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "e2bbc005-65b3-4149-a06f-432694688051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "4583108a-ccc3-4f4e-a420-6c64c4b548b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2bbc005-65b3-4149-a06f-432694688051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e26f11b6-c544-4332-8ab1-b2a09c32ee7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2bbc005-65b3-4149-a06f-432694688051",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "0fe6fb3b-dff1-481a-8c25-ee95d7fd1a97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "7ba3cb97-7202-47eb-9024-6602d026134b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fe6fb3b-dff1-481a-8c25-ee95d7fd1a97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35a29e9a-1333-4ef6-9157-9e98f84deea9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fe6fb3b-dff1-481a-8c25-ee95d7fd1a97",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        },
        {
            "id": "1cf3925b-6206-4afd-bbfb-d3bea051762d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "compositeImage": {
                "id": "bf30cd4d-5d5f-4bcb-a4ae-7a6d1f8cfb88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cf3925b-6206-4afd-bbfb-d3bea051762d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "336f9ce0-c4d8-467c-923f-9fe80d36a107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cf3925b-6206-4afd-bbfb-d3bea051762d",
                    "LayerId": "f37c7d4c-b813-4171-ac10-d724cb13e64f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 503,
    "layers": [
        {
            "id": "f37c7d4c-b813-4171-ac10-d724cb13e64f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36faf1d6-c802-4682-a675-f9c1c184a635",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 40,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 94,
    "xorig": 0,
    "yorig": 0
}