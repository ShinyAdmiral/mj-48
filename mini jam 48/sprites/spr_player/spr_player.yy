{
    "id": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 14,
    "bbox_right": 33,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56ff42b6-1fe3-4abc-889d-f9d96a803da8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "compositeImage": {
                "id": "b3d0f578-e274-490a-8e33-bb045fc3941c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56ff42b6-1fe3-4abc-889d-f9d96a803da8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe37eafe-af5e-45ff-a950-3e38a20c9870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56ff42b6-1fe3-4abc-889d-f9d96a803da8",
                    "LayerId": "113a694f-7304-4d59-aac8-fe08568b59fc"
                }
            ]
        },
        {
            "id": "5fa6b5fb-41fb-4936-b53f-54c678abf3ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "compositeImage": {
                "id": "0752ea4a-b38e-4bed-b92f-28d8e3af5e41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fa6b5fb-41fb-4936-b53f-54c678abf3ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d276212-c39f-40d1-b241-d4000e0408c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fa6b5fb-41fb-4936-b53f-54c678abf3ce",
                    "LayerId": "113a694f-7304-4d59-aac8-fe08568b59fc"
                }
            ]
        },
        {
            "id": "82500c4b-18f6-445c-aea4-b93d1b6d6270",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "compositeImage": {
                "id": "45b6a0f7-48ca-4cfd-91da-18fa6eee9fdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82500c4b-18f6-445c-aea4-b93d1b6d6270",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15ab53c9-c652-4d34-9b4f-19c0b6e6d71f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82500c4b-18f6-445c-aea4-b93d1b6d6270",
                    "LayerId": "113a694f-7304-4d59-aac8-fe08568b59fc"
                }
            ]
        },
        {
            "id": "9ba87cff-95d2-4d0e-ac1a-bbe9672a508e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "compositeImage": {
                "id": "27db47d7-c790-4ac7-9b8f-993233747e31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ba87cff-95d2-4d0e-ac1a-bbe9672a508e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cc08d0f-044b-4f57-be07-8124fc432ffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ba87cff-95d2-4d0e-ac1a-bbe9672a508e",
                    "LayerId": "113a694f-7304-4d59-aac8-fe08568b59fc"
                }
            ]
        },
        {
            "id": "568d95bb-58d4-4669-adb9-4e18301eecb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "compositeImage": {
                "id": "5f05d164-eac0-4d58-bb5a-4a9200b96c71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "568d95bb-58d4-4669-adb9-4e18301eecb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01e58764-833e-4bea-b694-24b9cc7693e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "568d95bb-58d4-4669-adb9-4e18301eecb3",
                    "LayerId": "113a694f-7304-4d59-aac8-fe08568b59fc"
                }
            ]
        },
        {
            "id": "73f158f6-12d5-436a-994c-8cab9958b49e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "compositeImage": {
                "id": "8d94143d-de17-4e7e-9225-bdd54f9bf2e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73f158f6-12d5-436a-994c-8cab9958b49e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80e31d95-3e70-4137-aa67-22c773f68b8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73f158f6-12d5-436a-994c-8cab9958b49e",
                    "LayerId": "113a694f-7304-4d59-aac8-fe08568b59fc"
                }
            ]
        },
        {
            "id": "0d2fe8f2-b3c8-4e40-95fd-d59e7adbccf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "compositeImage": {
                "id": "d8fd8c4c-9911-4867-be04-874680a30376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d2fe8f2-b3c8-4e40-95fd-d59e7adbccf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b9e3b25-d0fa-4b56-b240-36b73ebe3acb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d2fe8f2-b3c8-4e40-95fd-d59e7adbccf9",
                    "LayerId": "113a694f-7304-4d59-aac8-fe08568b59fc"
                }
            ]
        },
        {
            "id": "66953a83-9fdf-4649-b355-28070235dd17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "compositeImage": {
                "id": "4871b0b6-e445-4d54-8ca7-71c263a0e57f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66953a83-9fdf-4649-b355-28070235dd17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bb48106-c707-438f-b17e-52249f7f13e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66953a83-9fdf-4649-b355-28070235dd17",
                    "LayerId": "113a694f-7304-4d59-aac8-fe08568b59fc"
                }
            ]
        },
        {
            "id": "8e6dd7ef-def4-4072-8a45-d45d884f0214",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "compositeImage": {
                "id": "fb961a80-83da-4bd2-9faa-b86f5af99c27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e6dd7ef-def4-4072-8a45-d45d884f0214",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3ef3adc-59d5-468e-bdac-ed0efd9ab974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e6dd7ef-def4-4072-8a45-d45d884f0214",
                    "LayerId": "113a694f-7304-4d59-aac8-fe08568b59fc"
                }
            ]
        },
        {
            "id": "5d57cb05-7cda-4be8-9d43-3cb231fd4225",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "compositeImage": {
                "id": "53f8fb29-2739-4d76-a789-cd39e0ee9035",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d57cb05-7cda-4be8-9d43-3cb231fd4225",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd27629e-48a1-43f7-8bf2-00cb112f39ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d57cb05-7cda-4be8-9d43-3cb231fd4225",
                    "LayerId": "113a694f-7304-4d59-aac8-fe08568b59fc"
                }
            ]
        },
        {
            "id": "f8c334d1-d6e8-4c22-b8d1-5ab16d05c4ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "compositeImage": {
                "id": "c561a9b9-01c8-4108-8556-84dc92bc5d29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8c334d1-d6e8-4c22-b8d1-5ab16d05c4ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78eacabf-3a06-46c1-97d6-f1dde2c8f261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8c334d1-d6e8-4c22-b8d1-5ab16d05c4ba",
                    "LayerId": "113a694f-7304-4d59-aac8-fe08568b59fc"
                }
            ]
        },
        {
            "id": "6001d6a7-cffd-48e1-818c-5c541cb2bfe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "compositeImage": {
                "id": "6c9b8fe0-f1be-415d-8a8b-f51ec2168ab5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6001d6a7-cffd-48e1-818c-5c541cb2bfe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b21474e-6816-4bbf-ad72-b896381fe277",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6001d6a7-cffd-48e1-818c-5c541cb2bfe7",
                    "LayerId": "113a694f-7304-4d59-aac8-fe08568b59fc"
                }
            ]
        },
        {
            "id": "ad568dd6-0de5-4c7c-8dca-3071d28316b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "compositeImage": {
                "id": "4dd6eb09-1448-4cb3-b58b-1bb30c490f26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad568dd6-0de5-4c7c-8dca-3071d28316b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da3c3c51-3116-42ee-844c-ef4fcc15bb63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad568dd6-0de5-4c7c-8dca-3071d28316b1",
                    "LayerId": "113a694f-7304-4d59-aac8-fe08568b59fc"
                }
            ]
        },
        {
            "id": "55c20686-8848-48e9-8e54-52925d9e3352",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "compositeImage": {
                "id": "e97c2ecf-4dee-4aeb-9c07-baeab9959a11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55c20686-8848-48e9-8e54-52925d9e3352",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bc5cde4-aea9-46a2-a0e8-2f1b6dc0db85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55c20686-8848-48e9-8e54-52925d9e3352",
                    "LayerId": "113a694f-7304-4d59-aac8-fe08568b59fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "113a694f-7304-4d59-aac8-fe08568b59fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}