{
    "id": "2882b974-bb16-4b1e-93fd-c7ef681037fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "landing_jumping",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "147eb19f-c2ab-4c57-b234-22d3c7b65786",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2882b974-bb16-4b1e-93fd-c7ef681037fb",
            "compositeImage": {
                "id": "0c2f763d-af58-49c0-8008-a130cae5aba8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "147eb19f-c2ab-4c57-b234-22d3c7b65786",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "042babae-a3eb-4489-992c-468d4d48ca35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "147eb19f-c2ab-4c57-b234-22d3c7b65786",
                    "LayerId": "c762f37a-d47e-499d-b335-e78497c0221a"
                }
            ]
        },
        {
            "id": "b6079417-4f49-49c0-ba7a-05938a4fe8a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2882b974-bb16-4b1e-93fd-c7ef681037fb",
            "compositeImage": {
                "id": "62f9642a-892c-4e64-bd06-46a7653ffb25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6079417-4f49-49c0-ba7a-05938a4fe8a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5260fd52-6ca0-4e1c-9efa-13c076787eb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6079417-4f49-49c0-ba7a-05938a4fe8a4",
                    "LayerId": "c762f37a-d47e-499d-b335-e78497c0221a"
                }
            ]
        },
        {
            "id": "675f4b90-e01b-4dd1-8cfc-3c22bc1fe1af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2882b974-bb16-4b1e-93fd-c7ef681037fb",
            "compositeImage": {
                "id": "44f69361-457c-4e68-a3b6-3fd7fb81929f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "675f4b90-e01b-4dd1-8cfc-3c22bc1fe1af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39f9d5dc-ef42-4ac5-b06b-63d194492ac4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "675f4b90-e01b-4dd1-8cfc-3c22bc1fe1af",
                    "LayerId": "c762f37a-d47e-499d-b335-e78497c0221a"
                }
            ]
        },
        {
            "id": "3275c17c-e157-4144-94a5-487225cf2a22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2882b974-bb16-4b1e-93fd-c7ef681037fb",
            "compositeImage": {
                "id": "ec1bc46d-5aca-4828-86df-cb7004780398",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3275c17c-e157-4144-94a5-487225cf2a22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "638574a1-b63c-49fe-ad74-5ebbeb23615e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3275c17c-e157-4144-94a5-487225cf2a22",
                    "LayerId": "c762f37a-d47e-499d-b335-e78497c0221a"
                }
            ]
        },
        {
            "id": "e5c535e9-5cc5-48d6-90c7-65d9b72e9d11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2882b974-bb16-4b1e-93fd-c7ef681037fb",
            "compositeImage": {
                "id": "9daa3a34-326d-4411-b7e2-f220c9b4aaaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5c535e9-5cc5-48d6-90c7-65d9b72e9d11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "268c30ff-27e8-486d-bf3b-07278be4f343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5c535e9-5cc5-48d6-90c7-65d9b72e9d11",
                    "LayerId": "c762f37a-d47e-499d-b335-e78497c0221a"
                }
            ]
        },
        {
            "id": "3fc4f45f-fc14-4001-936c-e2a90002a88a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2882b974-bb16-4b1e-93fd-c7ef681037fb",
            "compositeImage": {
                "id": "0a8da5f5-1193-4549-b5d9-33dbe5a4b76e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fc4f45f-fc14-4001-936c-e2a90002a88a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc804e2e-0cd6-47aa-b078-e22b5ba881db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fc4f45f-fc14-4001-936c-e2a90002a88a",
                    "LayerId": "c762f37a-d47e-499d-b335-e78497c0221a"
                }
            ]
        },
        {
            "id": "38a87b45-f8dc-44f1-951b-1f950d885511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2882b974-bb16-4b1e-93fd-c7ef681037fb",
            "compositeImage": {
                "id": "44e70752-7c64-4dd2-82a4-c77ab37bbdd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38a87b45-f8dc-44f1-951b-1f950d885511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb4382ff-c4b3-4940-bef0-5174b117cd56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38a87b45-f8dc-44f1-951b-1f950d885511",
                    "LayerId": "c762f37a-d47e-499d-b335-e78497c0221a"
                }
            ]
        },
        {
            "id": "bbbbb35f-94a4-4b9d-a8a9-634d92db5037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2882b974-bb16-4b1e-93fd-c7ef681037fb",
            "compositeImage": {
                "id": "d38783e0-8fa7-46d0-953e-3766d31a23e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbbbb35f-94a4-4b9d-a8a9-634d92db5037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5621c764-8c0e-4649-bc51-14bd4e705db7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbbbb35f-94a4-4b9d-a8a9-634d92db5037",
                    "LayerId": "c762f37a-d47e-499d-b335-e78497c0221a"
                }
            ]
        },
        {
            "id": "82f0acc3-53ae-49a0-82d3-a419b173bad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2882b974-bb16-4b1e-93fd-c7ef681037fb",
            "compositeImage": {
                "id": "16fbc099-bdfe-4110-bb19-1a9354d9dd9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82f0acc3-53ae-49a0-82d3-a419b173bad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4bde5c3-93f7-44e5-bd11-4fb5255fc039",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82f0acc3-53ae-49a0-82d3-a419b173bad6",
                    "LayerId": "c762f37a-d47e-499d-b335-e78497c0221a"
                }
            ]
        },
        {
            "id": "9e811883-6c7c-4b00-98ec-ec1a21e0fee9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2882b974-bb16-4b1e-93fd-c7ef681037fb",
            "compositeImage": {
                "id": "2e0780e0-9932-4cfe-86dc-73fe61bfe9f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e811883-6c7c-4b00-98ec-ec1a21e0fee9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77b0b258-1139-4ca5-aa7c-bf8b5c323ca0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e811883-6c7c-4b00-98ec-ec1a21e0fee9",
                    "LayerId": "c762f37a-d47e-499d-b335-e78497c0221a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c762f37a-d47e-499d-b335-e78497c0221a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2882b974-bb16-4b1e-93fd-c7ef681037fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 7
}