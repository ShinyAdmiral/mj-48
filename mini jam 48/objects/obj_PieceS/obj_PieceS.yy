{
    "id": "8f986cd8-20fc-46d7-8642-fa2eb8e2c876",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_PieceS",
    "eventList": [
        {
            "id": "63dceba2-b7f6-4386-99ab-c61587c7a05a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f986cd8-20fc-46d7-8642-fa2eb8e2c876"
        },
        {
            "id": "1949459a-1d4f-43ad-b8fc-cf6cb89697d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8f986cd8-20fc-46d7-8642-fa2eb8e2c876"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d81bf536-bae4-41bd-ac19-7397ce51f0df",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}