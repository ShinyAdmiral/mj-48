/// @description Insert description here
// You can write your code in this editor
a = instance_create_layer(x,y,"Blocks", obj_Block);
b = instance_create_layer(x,y-32,"Blocks", obj_Block);
c = instance_create_layer(x+32,y,"Blocks", obj_Block);
d = instance_create_layer(x+32,y+32,"Blocks", obj_Block);

a.sprite_index = corner_bottom_left_blue;
b.sprite_index = end_block_up_blue;
c.sprite_index = corner_top_right_blue;
d.sprite_index = end_block_down_blue;

rotation = 0; 

locked = false;