{
    "id": "ad25bb1a-ed0a-44d9-bf79-7d6426a09cb0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_play",
    "eventList": [
        {
            "id": "7b52e85d-a261-4ebc-a66a-c480bae3a6b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "ad25bb1a-ed0a-44d9-bf79-7d6426a09cb0"
        },
        {
            "id": "cc408817-277d-43de-b535-d0287572d4d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "ad25bb1a-ed0a-44d9-bf79-7d6426a09cb0"
        },
        {
            "id": "e8c1b744-1b5b-4fa8-972e-91732fee0f1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ad25bb1a-ed0a-44d9-bf79-7d6426a09cb0"
        },
        {
            "id": "604fd635-39f8-4a8a-b407-85828d87195f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "ad25bb1a-ed0a-44d9-bf79-7d6426a09cb0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1d6668c6-ab69-42ea-9609-acd367c9944a",
    "visible": true
}