{
    "id": "4c5ef8e6-5291-43df-8f94-b251b4b57656",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_PieceT",
    "eventList": [
        {
            "id": "51dbeb57-b3bf-4701-a097-3c4b475635a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4c5ef8e6-5291-43df-8f94-b251b4b57656"
        },
        {
            "id": "88065e78-1d95-469a-a2c8-827225c25f7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4c5ef8e6-5291-43df-8f94-b251b4b57656"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d81bf536-bae4-41bd-ac19-7397ce51f0df",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}