/// @description Insert description here
// You can write your code in this editor
a = instance_create_layer(x,y,"Blocks", obj_Block);
b = instance_create_layer(x,y-32,"Blocks", obj_Block);
c = instance_create_layer(x-32,y,"Blocks", obj_Block);
d = instance_create_layer(x+32,y,"Blocks", obj_Block);

a.sprite_index = intersection_block_down;
b.sprite_index = end_block_up_black;
c.sprite_index = end_block_left_black;
d.sprite_index = end_block_right_black;

rotation = 0; 

locked = false;