{
    "id": "9113fe58-667f-45a2-9db7-b529fcce954a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rightUI",
    "eventList": [
        {
            "id": "af53cb51-320a-43c7-bb38-e4c9d01c4c29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9113fe58-667f-45a2-9db7-b529fcce954a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f084849f-12ab-4519-9d73-834e7a798bf6",
    "visible": true
}