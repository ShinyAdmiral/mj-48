{
    "id": "65a2293f-0394-47e9-90c0-fc8c4f41a0a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lockedS",
    "eventList": [
        {
            "id": "e52f26df-248e-462c-afff-7ebefb09e42d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "65a2293f-0394-47e9-90c0-fc8c4f41a0a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5cca9975-8fcc-4f1c-b56d-5ced4fb123b0",
    "visible": true
}