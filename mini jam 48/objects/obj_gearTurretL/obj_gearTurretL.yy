{
    "id": "43dc6d47-3e35-41fb-8cb9-b186f9e936c5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gearTurretL",
    "eventList": [
        {
            "id": "9d373c86-806c-45fa-a288-c8b8d6d5aad8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "43dc6d47-3e35-41fb-8cb9-b186f9e936c5"
        },
        {
            "id": "71070028-fa42-42f1-a505-7f7afdc5e3cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "43dc6d47-3e35-41fb-8cb9-b186f9e936c5"
        },
        {
            "id": "9f544741-87be-4916-a05e-6b3fa7698a8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "43dc6d47-3e35-41fb-8cb9-b186f9e936c5"
        },
        {
            "id": "d4bf0799-82a3-4a29-a05a-57cbdc7eda7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "43dc6d47-3e35-41fb-8cb9-b186f9e936c5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "72a31251-1860-40f5-8b2c-28b71ab8da5e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "13f9e085-2950-4de3-b4e9-469a410c583d",
    "visible": true
}