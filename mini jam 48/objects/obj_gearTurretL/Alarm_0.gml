if (x < room_width/2){
var a = instance_create_layer(x+32,y,"Player", obj_laserBolt)
a.hspeed = bullet_speed;
}
else{
var a = instance_create_layer(x-32,y,"Player", obj_laserBolt)
a.hspeed = -bullet_speed;
}

audio_sound_pitch(PEW, random_range(.8,1.2));
audio_play_sound(PEW,10,false);

alarm[1] = freeze;