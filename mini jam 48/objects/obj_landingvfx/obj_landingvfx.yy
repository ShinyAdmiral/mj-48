{
    "id": "d10dee4f-192e-40cd-9c1b-78d9d3a65b44",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_landingvfx",
    "eventList": [
        {
            "id": "33413ee9-37ee-4a51-b25f-1c7791e6afd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "d10dee4f-192e-40cd-9c1b-78d9d3a65b44"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2882b974-bb16-4b1e-93fd-c7ef681037fb",
    "visible": true
}