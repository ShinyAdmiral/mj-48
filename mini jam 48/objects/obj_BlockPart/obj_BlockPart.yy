{
    "id": "fdaaa202-f544-4a89-bfbb-101fd04eb93d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_BlockPart",
    "eventList": [
        {
            "id": "89d88458-1c6f-49ae-a142-b0884f6be9be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "fdaaa202-f544-4a89-bfbb-101fd04eb93d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "71e1c995-e8ce-4d46-92e9-d8acf4279131",
    "visible": true
}