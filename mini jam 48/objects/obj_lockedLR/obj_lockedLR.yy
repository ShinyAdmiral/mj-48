{
    "id": "f966582c-c4fa-4606-86d7-02f34f4f4f3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lockedLR",
    "eventList": [
        {
            "id": "34fb9d1a-4b6b-4446-beca-037efb86b130",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "f966582c-c4fa-4606-86d7-02f34f4f4f3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fd69b501-3fb2-4d78-88b8-a4288a74d698",
    "visible": true
}