{
    "id": "9c9aa85a-0447-4366-8d5f-caabf29846e0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lockedI",
    "eventList": [
        {
            "id": "888e763e-4ee5-4d2c-8a84-a27abf04adc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "9c9aa85a-0447-4366-8d5f-caabf29846e0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b747b58-cac4-4df7-a4aa-84d87bef3300",
    "visible": true
}