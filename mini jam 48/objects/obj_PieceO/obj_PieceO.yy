{
    "id": "573dce0f-a226-49e9-9ac5-0dec16f5dd25",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_PieceO",
    "eventList": [
        {
            "id": "ce36a7d1-5990-4e10-9d7e-c5caddd9f65b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "573dce0f-a226-49e9-9ac5-0dec16f5dd25"
        },
        {
            "id": "b46d0063-4524-4e14-a941-045556b3b6e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "573dce0f-a226-49e9-9ac5-0dec16f5dd25"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d81bf536-bae4-41bd-ac19-7397ce51f0df",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}