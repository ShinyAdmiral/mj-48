{
    "id": "aa962c98-5480-433d-90e2-3ee7b501f8fa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_liquid",
    "eventList": [
        {
            "id": "a99b0d35-91e4-46ec-96cc-c72d3e8083d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aa962c98-5480-433d-90e2-3ee7b501f8fa"
        },
        {
            "id": "9f60985d-43d1-4261-8722-6997553383d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aa962c98-5480-433d-90e2-3ee7b501f8fa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7a9bf6b2-f5b3-4ff4-b391-e609f9a0f076",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2eff3cfd-79ba-4485-8e46-43c6d934500e",
    "visible": true
}