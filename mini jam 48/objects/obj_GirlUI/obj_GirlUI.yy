{
    "id": "6d72cfcc-d590-4f76-ae85-55b7eaa79889",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GirlUI",
    "eventList": [
        {
            "id": "d294bb9e-20ad-409a-962c-8dd863c57b4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6d72cfcc-d590-4f76-ae85-55b7eaa79889"
        },
        {
            "id": "1387f696-c2b1-489f-bce8-356225b09171",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6d72cfcc-d590-4f76-ae85-55b7eaa79889"
        },
        {
            "id": "0bb540c2-7ac3-429b-80b1-4e605e3f0b59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6d72cfcc-d590-4f76-ae85-55b7eaa79889"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "93d1e136-5668-4a4a-9b86-724ea32d88bb",
    "visible": true
}