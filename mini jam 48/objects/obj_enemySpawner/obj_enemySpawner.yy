{
    "id": "298bd168-7502-4b62-9bb6-cc977b017920",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemySpawner",
    "eventList": [
        {
            "id": "32f2808b-9068-4195-bb5e-b806e0dae983",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "298bd168-7502-4b62-9bb6-cc977b017920"
        },
        {
            "id": "c4af2174-3ac1-46d3-9ed6-210be0f728f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "298bd168-7502-4b62-9bb6-cc977b017920"
        },
        {
            "id": "7507c3c9-6395-4634-bb1e-2bbe64ba6ea0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "298bd168-7502-4b62-9bb6-cc977b017920"
        },
        {
            "id": "2bcefbe2-6afe-404e-8155-5dba664062a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "298bd168-7502-4b62-9bb6-cc977b017920"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}