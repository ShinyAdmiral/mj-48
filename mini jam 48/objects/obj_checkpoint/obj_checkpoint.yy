{
    "id": "8618ca23-c05f-484e-80e7-da8db7068850",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_checkpoint",
    "eventList": [
        {
            "id": "d1fdaef9-4729-418c-8443-3def2672dc03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b51ef966-717c-47b2-97a9-a3ed50ffdad8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8618ca23-c05f-484e-80e7-da8db7068850"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "d9de17af-0ac7-4ad2-ba47-c1b441e5e323",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.1",
            "varName": "threshold",
            "varType": 0
        },
        {
            "id": "a959ac69-0d05-4303-b319-72d4be398c8f",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "enemy",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "3cb4aaeb-f413-4967-a0f4-87993949d242",
    "visible": true
}