{
    "id": "b7e253fe-a94d-4186-9ebd-5768cb4231b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_PieceSR",
    "eventList": [
        {
            "id": "cc82bb04-5a6d-4f26-8fce-9104bab629bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b7e253fe-a94d-4186-9ebd-5768cb4231b3"
        },
        {
            "id": "b6d3e937-f048-4cc0-8c9e-892583d7f6fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b7e253fe-a94d-4186-9ebd-5768cb4231b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d81bf536-bae4-41bd-ac19-7397ce51f0df",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}