{
    "id": "d9323f31-c079-49fd-97fb-3f1fab647b32",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_loose",
    "eventList": [
        {
            "id": "ab6f0a02-9ef9-489c-970c-de7240e8beed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d9323f31-c079-49fd-97fb-3f1fab647b32"
        },
        {
            "id": "bf3d86a7-f05f-4fa4-bc63-d182253ecd40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d9323f31-c079-49fd-97fb-3f1fab647b32"
        },
        {
            "id": "9fa192a8-8383-4863-8880-0c1c59d29b7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "d9323f31-c079-49fd-97fb-3f1fab647b32"
        },
        {
            "id": "e14b4301-c469-4dc6-91d3-c936b4e31017",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "d9323f31-c079-49fd-97fb-3f1fab647b32"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "61796dc4-c5aa-404d-b62f-71a8c81a51a1",
    "visible": true
}