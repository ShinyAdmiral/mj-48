{
    "id": "3b3eb383-5d9b-40bd-958d-42dfead0f396",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Block",
    "eventList": [
        {
            "id": "059f3ef6-2c2e-48d8-b84f-52120f245d5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3b3eb383-5d9b-40bd-958d-42dfead0f396"
        },
        {
            "id": "f1dd0b72-7ada-41f5-854e-8624278b01f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3b3eb383-5d9b-40bd-958d-42dfead0f396"
        },
        {
            "id": "e41dceed-9f3f-4efd-b40b-235fcbf5afe4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3b3eb383-5d9b-40bd-958d-42dfead0f396"
        },
        {
            "id": "377e2a8b-2e9c-441d-9832-8d4768365d04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "3b3eb383-5d9b-40bd-958d-42dfead0f396"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "13cb11f0-1b9a-464c-b36a-c486b847d062",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "204330e0-10eb-4c9f-bd85-812eb94ff3a3",
    "visible": true
}