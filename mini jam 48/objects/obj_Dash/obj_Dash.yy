{
    "id": "60fa968d-b63b-4ae2-8cd6-cc425069f387",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Dash",
    "eventList": [
        {
            "id": "291f1034-3321-4125-971c-918afbf1319b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "60fa968d-b63b-4ae2-8cd6-cc425069f387"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "df88f139-061d-45b0-a34d-eb50ac105e6f",
    "visible": true
}