{
    "id": "01e74b63-c916-43f7-867d-32bb2c358abb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_BlockStatic",
    "eventList": [
        {
            "id": "5f40d692-893f-49e3-a9a7-4b4871231eb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "01e74b63-c916-43f7-867d-32bb2c358abb"
        },
        {
            "id": "a348f317-10e3-4b3c-8715-2f2f725b0efe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "01e74b63-c916-43f7-867d-32bb2c358abb"
        },
        {
            "id": "540c4457-e129-4532-941a-5c31bb0c004b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "01e74b63-c916-43f7-867d-32bb2c358abb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "13cb11f0-1b9a-464c-b36a-c486b847d062",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "204330e0-10eb-4c9f-bd85-812eb94ff3a3",
    "visible": true
}