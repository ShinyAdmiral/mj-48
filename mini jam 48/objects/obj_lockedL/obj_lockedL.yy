{
    "id": "c55d5be4-4927-440c-a374-bd88627f84ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lockedL",
    "eventList": [
        {
            "id": "08ee96a4-6bc5-41a0-a8be-7d03b7b20979",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "c55d5be4-4927-440c-a374-bd88627f84ab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "eb70b244-11ea-4715-8875-aa4d087c832f",
    "visible": true
}