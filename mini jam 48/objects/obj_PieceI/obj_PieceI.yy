{
    "id": "3218fb1c-845e-4fab-b6bd-4bc2483f0c37",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_PieceI",
    "eventList": [
        {
            "id": "0370a573-062a-4ad4-ad16-1712d8bf8dd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3218fb1c-845e-4fab-b6bd-4bc2483f0c37"
        },
        {
            "id": "b5a161f2-15aa-4712-8cfd-3cb6d9bf16fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3218fb1c-845e-4fab-b6bd-4bc2483f0c37"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d81bf536-bae4-41bd-ac19-7397ce51f0df",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}