{
    "id": "8fc769a4-138b-47c8-94c5-080880050451",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lockedT",
    "eventList": [
        {
            "id": "4ee28315-4196-49f3-9c92-541c02fd567b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "8fc769a4-138b-47c8-94c5-080880050451"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ac96765d-6bc0-424e-a983-2e280be4ed6d",
    "visible": true
}