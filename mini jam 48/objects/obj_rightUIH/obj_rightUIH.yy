{
    "id": "61158d0e-47b5-4701-bbe1-f2e5c451b4fa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rightUIH",
    "eventList": [
        {
            "id": "1fc91f37-1a49-431f-b2ae-650b8c3524dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "61158d0e-47b5-4701-bbe1-f2e5c451b4fa"
        },
        {
            "id": "29068f02-178e-4fa7-abed-a6e053e0d5a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "61158d0e-47b5-4701-bbe1-f2e5c451b4fa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75d681c9-bd2f-401e-b98c-d62b27cf0ccc",
    "visible": true
}