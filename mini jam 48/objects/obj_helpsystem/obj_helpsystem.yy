{
    "id": "c3faae23-4419-454f-80b0-385feb887067",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_helpsystem",
    "eventList": [
        {
            "id": "74f35f7a-7636-4e11-9b3b-283442929439",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "c3faae23-4419-454f-80b0-385feb887067"
        },
        {
            "id": "32f3c3b7-6b5c-48bf-bd4d-7bb86121007d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "c3faae23-4419-454f-80b0-385feb887067"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ee08f5cd-eb23-45ac-8bf6-ca0c82df4e89",
    "visible": true
}