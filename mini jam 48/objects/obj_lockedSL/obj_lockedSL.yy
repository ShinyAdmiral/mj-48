{
    "id": "d7b00c66-80cd-4876-bf58-f353c153b926",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lockedSL",
    "eventList": [
        {
            "id": "bfa7e5b3-4970-4f6c-a81b-641d38422ec7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "d7b00c66-80cd-4876-bf58-f353c153b926"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4f4d8a6-a7d5-44c3-8955-b1e1d57756c0",
    "visible": true
}