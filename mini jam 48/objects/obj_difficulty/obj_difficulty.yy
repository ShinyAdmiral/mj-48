{
    "id": "22bea365-3d22-4656-a0f2-c527b934a613",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_difficulty",
    "eventList": [
        {
            "id": "13407f27-c8c9-47ea-adb3-d356655e4212",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "22bea365-3d22-4656-a0f2-c527b934a613"
        },
        {
            "id": "d89da6c8-8cfb-49c1-b2cb-01ea729c32b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "22bea365-3d22-4656-a0f2-c527b934a613"
        },
        {
            "id": "9dc3b59b-2bc2-4eed-95a5-312e57906343",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "22bea365-3d22-4656-a0f2-c527b934a613"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}