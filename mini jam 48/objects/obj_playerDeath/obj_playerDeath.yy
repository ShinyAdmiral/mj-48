{
    "id": "6625d355-3a18-4390-9ae5-b96ce5e5d903",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_playerDeath",
    "eventList": [
        {
            "id": "d601109f-8319-4253-8084-db24da827fd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6625d355-3a18-4390-9ae5-b96ce5e5d903"
        },
        {
            "id": "5b250824-119e-4f21-b64f-6ede76c9a035",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "6625d355-3a18-4390-9ae5-b96ce5e5d903"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f8ba53c6-2a80-46b9-b821-9bb5409bea36",
    "visible": true
}