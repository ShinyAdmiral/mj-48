{
    "id": "c9559534-f387-43f6-9b12-6d11cbc90e5c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_damagevfx",
    "eventList": [
        {
            "id": "77cfc683-7cae-4a53-b233-cd82b746dbd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "c9559534-f387-43f6-9b12-6d11cbc90e5c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d8fd31b7-35f6-4076-9cf2-86b5c5c6ce93",
    "visible": true
}