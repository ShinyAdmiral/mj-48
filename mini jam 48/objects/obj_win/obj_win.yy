{
    "id": "1e152445-7102-4bb7-8bb5-b32a4f6dfaa9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_win",
    "eventList": [
        {
            "id": "31d2be50-f7ca-419e-9794-13b53b93d971",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1e152445-7102-4bb7-8bb5-b32a4f6dfaa9"
        },
        {
            "id": "0d3ea9a1-b181-4b87-9a3c-8b12cfda40d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1e152445-7102-4bb7-8bb5-b32a4f6dfaa9"
        },
        {
            "id": "3e855a16-cfe2-4526-ac7c-d79cd1aaee6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "1e152445-7102-4bb7-8bb5-b32a4f6dfaa9"
        },
        {
            "id": "4d1ba54a-bf5b-4e7c-a46b-6b3d595c3cb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "1e152445-7102-4bb7-8bb5-b32a4f6dfaa9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ffd29366-8958-41e2-b55b-6d771deb1aa8",
    "visible": true
}