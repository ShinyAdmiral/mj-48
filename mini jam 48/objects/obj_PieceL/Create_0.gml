/// @description Insert description here
// You can write your code in this editor
a = instance_create_layer(x,y,"Blocks", obj_Block);
b = instance_create_layer(x,y-32,"Blocks", obj_Block);
c = instance_create_layer(x,y+32,"Blocks", obj_Block);
d = instance_create_layer(x+32,y+32,"Blocks", obj_Block);

a.sprite_index = middle_block_vertical_green;
b.sprite_index = end_block_up_green;
c.sprite_index = corner_bottom_left_green;
d.sprite_index = end_block_right_green;

rotation = 0; 

locked = false;