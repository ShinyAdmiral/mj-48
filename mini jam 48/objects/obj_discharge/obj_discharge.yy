{
    "id": "e948faa0-a161-4a28-9128-2df987625ff0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_discharge",
    "eventList": [
        {
            "id": "e64a48d5-0fd1-4e63-9577-50fed6bbcc99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "e948faa0-a161-4a28-9128-2df987625ff0"
        },
        {
            "id": "e856f640-984b-44e2-8165-250bcd182fcd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "01e74b63-c916-43f7-867d-32bb2c358abb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e948faa0-a161-4a28-9128-2df987625ff0"
        },
        {
            "id": "81179afe-5a93-4eaf-8a08-6f68ec560d08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e948faa0-a161-4a28-9128-2df987625ff0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6915be37-e4e1-4a63-bc9a-8584db653389",
    "visible": true
}