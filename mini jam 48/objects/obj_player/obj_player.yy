{
    "id": "b51ef966-717c-47b2-97a9-a3ed50ffdad8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "6469e267-c3f5-4929-815f-463df763a30f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b51ef966-717c-47b2-97a9-a3ed50ffdad8"
        },
        {
            "id": "c2f2db4b-b74f-4c26-b8a6-e73f1ed9b39c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b51ef966-717c-47b2-97a9-a3ed50ffdad8"
        },
        {
            "id": "af5bb46f-55a0-4584-8afe-ea487d0fe832",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7a9bf6b2-f5b3-4ff4-b391-e609f9a0f076",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b51ef966-717c-47b2-97a9-a3ed50ffdad8"
        },
        {
            "id": "1927850b-d05e-4593-a2ba-20912348d5db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b51ef966-717c-47b2-97a9-a3ed50ffdad8"
        },
        {
            "id": "ce01929b-fa48-442a-a43b-c87115a68e66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5be5bc02-708b-4ca7-a83d-121c4392276b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b51ef966-717c-47b2-97a9-a3ed50ffdad8"
        },
        {
            "id": "57d998d1-88f7-4b2d-951b-358cdd8f7f2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b51ef966-717c-47b2-97a9-a3ed50ffdad8"
        },
        {
            "id": "1b0dd8dc-1a0c-46f5-9d3e-960789b10dd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b51ef966-717c-47b2-97a9-a3ed50ffdad8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f8d42fd6-770e-4734-a555-c943248302a3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "21d61c59-b4b4-4877-8e38-2ee8d0914d93",
    "visible": true
}