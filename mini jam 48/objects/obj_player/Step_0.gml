if (!charge)
{
	right = keyboard_check(ord("D"));
	left = keyboard_check(ord("A"));
	if !instance_exists(obj_controls){
		jump = keyboard_check_pressed(vk_space);
		discharge = keyboard_check_pressed(vk_shift);
	}
	else
	{
		jump = keyboard_check_pressed(ord("W"));
		discharge = keyboard_check_pressed(vk_space);
	}

	hor_speed = (right - left) * movement_speed;

	//Collision Checks (Horizontal)
	if (place_meeting(x+hor_speed,y,obj_solid))
	{
		while(!place_meeting(x+sign(hor_speed),y,obj_solid))
		{
			x+= sign(hor_speed)
		}
		hor_speed = 0;
	}

	x += hor_speed;

	//gravity
	if(ver_speed < maxGrav)
	{
		ver_speed += grav;
	}

	//Jumping
	if(place_meeting(x, y+1,  obj_solid))
	{
		if (jump){
			audio_sound_pitch(Jump3, random_range(.9,1.4));
			audio_play_sound(Jump3,10,false);
			ver_speed = jump * -jumpPower;
		}
	}

	//Collision Checks (Vertical)
	if (place_meeting(x,y+ver_speed,obj_solid))
	{
		while(!place_meeting(x,y+sign(ver_speed),obj_solid))
		{
			y += sign(ver_speed)
		}
		if (!landsound){
			
			instance_create_layer(x,y,"Particles", obj_landingvfx);
			
			audio_sound_pitch(landing, random_range(.7,1.1));
			audio_play_sound(landing,10,false);
			landsound = true;
		}
		ver_speed = 0;
		cam_move = true;
	}
	else
	{
		landsound = false;
		cam_move = false;
	}

	y += ver_speed;

	if (discharge && discharge_cool  <= 0){
		instance_create_layer(x,y,"Particles",obj_discharge);
		sprite_index = robert_dis;
		charge = true;
	}
	else{
		if (right && !left && ver_speed = 0){
			if (!instance_exists(obj_Dash)){
				var a = instance_create_layer(x,y,"Door", obj_Dash)
				a.image_xscale = -1;
			}
			sprite_index = spr_right;
		}
		else if (!right && left && ver_speed = 0){
			if (!instance_exists(obj_Dash)){
				var a = instance_create_layer(x,y,"Door", obj_Dash)
			}
			sprite_index = spr_left;
		}
		else //if(vspeed == 0 && !right && !left)
			sprite_index = spr_player;
			
	}
}

if (y > obj_liquid.y + 900)
{
	hp = 0;
}

if (hp <= 0 && !dead)
{
	image_alpha = 0;
	instance_create_layer(x,y,"UI",obj_playerDeath);
	instance_destroy(obj_PieceSpawner);
	charge = true;
	dead = true;
}

if (win && !win_image)
{
	image_alpha -= .01
	instance_destroy(obj_PieceSpawner);
	charge = true;
	
	if (image_alpha <= 0){
		instance_create_layer(x,y + 150,"UI", obj_win);
		win_image = true;
	}
}