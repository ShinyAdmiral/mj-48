{
    "id": "f80c5293-a2fe-41a2-b411-8303f92acf3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_PieceSpawner",
    "eventList": [
        {
            "id": "a6ec8d08-2ee3-4af0-a604-75add47411e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f80c5293-a2fe-41a2-b411-8303f92acf3c"
        },
        {
            "id": "5f0fc97e-4968-4d6b-8473-15f4bfb929a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f80c5293-a2fe-41a2-b411-8303f92acf3c"
        },
        {
            "id": "8837db1f-ecb4-4d7d-92bb-79d8d11dcd4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f80c5293-a2fe-41a2-b411-8303f92acf3c"
        },
        {
            "id": "ef82d937-2293-4e2c-908d-067610d78c62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f80c5293-a2fe-41a2-b411-8303f92acf3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}