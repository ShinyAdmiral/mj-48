{
    "id": "36f37469-420e-40bf-817d-bc4fa4e19bb5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_laserBolt",
    "eventList": [
        {
            "id": "25755f3a-1760-4173-a497-b5ba0a848edc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b51ef966-717c-47b2-97a9-a3ed50ffdad8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "36f37469-420e-40bf-817d-bc4fa4e19bb5"
        },
        {
            "id": "0d831378-462e-4170-96a8-4e54dc79a340",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "01e74b63-c916-43f7-867d-32bb2c358abb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "36f37469-420e-40bf-817d-bc4fa4e19bb5"
        },
        {
            "id": "d6545f82-8ae4-475e-81d4-4474bd8de77b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "13cb11f0-1b9a-464c-b36a-c486b847d062",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "36f37469-420e-40bf-817d-bc4fa4e19bb5"
        }
    ],
    "maskSpriteId": "31474b22-9f6e-4ad1-967e-a57aa3289c8a",
    "overriddenProperties": null,
    "parentObjectId": "7a9bf6b2-f5b3-4ff4-b391-e609f9a0f076",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "31474b22-9f6e-4ad1-967e-a57aa3289c8a",
    "visible": true
}