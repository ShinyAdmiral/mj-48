instance_destroy(other);
instance_destroy(self);

audio_sound_pitch(Demolish, random_range(.8,1.2));
audio_play_sound(Demolish,10,false);