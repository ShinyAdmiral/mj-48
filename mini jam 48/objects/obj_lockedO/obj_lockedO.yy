{
    "id": "7dbdf7fe-b0f7-4f93-ab01-04357ad16343",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lockedO",
    "eventList": [
        {
            "id": "51da9f0c-4799-492e-aa82-89876408aeed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "7dbdf7fe-b0f7-4f93-ab01-04357ad16343"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1cf24122-13b1-44b2-bd94-b3b0c936bc6d",
    "visible": true
}