{
    "id": "394d72a4-ebd6-469e-939b-743e800b3c55",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_help",
    "eventList": [
        {
            "id": "768f6a43-e811-4f09-8079-b5e9dfcec92c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "394d72a4-ebd6-469e-939b-743e800b3c55"
        },
        {
            "id": "62fa6852-cf34-4f37-a1dc-609c797897a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "394d72a4-ebd6-469e-939b-743e800b3c55"
        },
        {
            "id": "812fa7aa-2c15-4538-9b76-7e5a5d70fd72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "394d72a4-ebd6-469e-939b-743e800b3c55"
        },
        {
            "id": "24ab02c1-387b-4cd4-9882-59c44af902a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "394d72a4-ebd6-469e-939b-743e800b3c55"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "add03e69-ec2a-4178-beb1-08d350ba4d24",
    "visible": true
}