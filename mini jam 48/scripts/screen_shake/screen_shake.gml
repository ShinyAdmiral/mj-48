/// @desc ScreenShake(Duration, Magnitude)
/// @arg Duration Determines how long the object lasts for

with (obj_camera)
{
	mag = true;
	alarm[0] = argument0;
	shake = argument1;
}