
{
    "name": "room0",
    "id": "2f606a8c-b033-4396-be4c-7d118a338513",
    "creationCodeFile": "RoomCreationCode.gml",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "81c20677-6af8-42a5-aeb4-112ab7b39dc2",
        "22b4bcb1-ca07-41ec-a53d-cd0b3c56b8d3",
        "039f8412-ed4d-4bf7-b626-e8dcb19d9cae",
        "e792a5b1-dba7-4ea9-a782-1a99af93d645",
        "8c27cbf0-d19e-430b-befc-ac6eb4248076",
        "81337526-bb9a-4cf2-a9d5-9fd1d2a091ed",
        "ac3da6c0-e290-4c13-b6a3-b81427d50da1",
        "bc656cee-6eda-4d5a-be25-50ac32726ae9",
        "5bfe5e10-fd07-48a4-9e18-c43c36d7d91a",
        "94e00776-68bd-4909-b65b-0cad3f2c9ada",
        "3b72fcd6-856d-4d8d-85fd-f7320ab41853",
        "51ab2fd7-29a5-489f-83b1-55aec79a6642",
        "28a91b78-70ac-47cc-ab70-588a0ba13e5d",
        "da1a3cce-798a-4127-976c-c9764bd85df5",
        "9600cc8b-105b-42f1-b6fb-97640916e5e4",
        "4fc6f8a4-3bca-4594-b200-e042eaa1c182",
        "6c1b8d07-0951-4ca2-92ed-146bd438d2c5",
        "8ac9b672-2b56-4eb5-8c3b-de8068dcb8f2",
        "72e30b72-085e-4b8b-a5a7-39bd650e0d68",
        "bfe95096-7dd7-4266-a9b8-2c4f954d78dc",
        "3e8215f5-30c0-4edf-b3c5-49982e56fd9f",
        "c3d04dea-3ae8-45e5-a9b1-2a4bf3ec5d26",
        "8fcca061-3fbb-42c2-b3d4-ab48b16e716f"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRAssetLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Profile",
            "id": "11bddd9f-2832-477b-b3c1-b29f70fe5f01",
            "assets": [
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_6CB13F3D","id": "b05fa4c4-ed5d-49ac-a23e-9020e38e2cd2","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "813649aa-29af-46e1-b647-31636b15ea9b","userdefined_animFPS": false,"x": 55,"y": 3375},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_2B1B197E","id": "90512e74-f4d0-4572-96a9-349eee880bb0","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "f45f71d6-9e9d-4298-a27d-0f924b1e6629","userdefined_animFPS": false,"x": 481,"y": 3552}
            ],
            "depth": 0,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRAssetLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "UI",
            "id": "32af5616-cc3b-4983-82db-8281ced679b3",
            "depth": 100,
            "grid_x": 8,
            "grid_y": 8,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_1CEB0656","id": "8c27cbf0-d19e-430b-befc-ac6eb4248076","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1CEB0656","objId": "9113fe58-667f-45a2-9db7-b529fcce954a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 8,"y": 3336},
{"name": "inst_4F98F779","id": "81337526-bb9a-4cf2-a9d5-9fd1d2a091ed","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4F98F779","objId": "61158d0e-47b5-4701-bbe1-f2e5c451b4fa","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 8,"y": 3336},
{"name": "inst_7D99CC5B","id": "72e30b72-085e-4b8b-a5a7-39bd650e0d68","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7D99CC5B","objId": "6d72cfcc-d590-4f76-ae85-55b7eaa79889","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 474,"y": 3336}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances_1",
            "id": "b64b58a2-3f72-4554-afb1-5b9ff28cc0e0",
            "depth": 200,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_CFD5BE7","id": "bfe95096-7dd7-4266-a9b8-2c4f954d78dc","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_CFD5BE7","objId": "f80c5293-a2fe-41a2-b411-8303f92acf3c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 487,"y": 2920}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRAssetLayer_Model:#YoYoStudio.MVCFormat",
            "name": "UIBack",
            "id": "f43b3a32-288b-438d-a75e-a72315da9d2d",
            "assets": [
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_7B8BF4B6","id": "2648a32d-abc0-4152-a9dc-b6f9d0563f79","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "7ab8c55b-79fb-4696-bce4-8eddf3ce6348","userdefined_animFPS": false,"x": 0,"y": 3328},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_41AFAF76","id": "f9c3265a-bc49-4f0f-8d23-8d38795eacb8","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "7ab8c55b-79fb-4696-bce4-8eddf3ce6348","userdefined_animFPS": false,"x": 470,"y": 3328}
            ],
            "depth": 300,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRAssetLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Acid",
            "id": "0eb9c75f-50ac-4fe8-a05f-0b0bbb70f870",
            "depth": 400,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_35C1E84A","id": "22b4bcb1-ca07-41ec-a53d-cd0b3c56b8d3","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_35C1E84A","objId": "aa962c98-5480-433d-90e2-3ee7b501f8fa","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 96,"y": 3332}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Particles",
            "id": "f4644946-ce81-44bf-a644-207e38dcf50a",
            "depth": 500,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Blocks",
            "id": "a791cc14-f437-46e5-9744-e04d8c72387a",
            "depth": 600,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRAssetLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Fore1",
            "id": "3fe6e890-e1de-4deb-8a36-f8ea528430a2",
            "assets": [
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_78036970","id": "24e09109-28d2-405d-8283-418474622477","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "2d3e0451-2f05-4f82-87f5-47bab1232662","userdefined_animFPS": false,"x": 96,"y": 2784},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_7F08FEE0","id": "e0a29799-6e25-4501-8b8a-51dddd74bb13","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "2d3e0451-2f05-4f82-87f5-47bab1232662","userdefined_animFPS": false,"x": 96,"y": 2272},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_2870A10E","id": "9c00c013-03a6-4beb-8a32-784266efb227","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "2d3e0451-2f05-4f82-87f5-47bab1232662","userdefined_animFPS": false,"x": 96,"y": 1760},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_34C24529","id": "6714146c-1d00-4146-8d1d-94135e008b91","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "2d3e0451-2f05-4f82-87f5-47bab1232662","userdefined_animFPS": false,"x": 96,"y": 1248},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_51177A73","id": "53ddaf20-f111-4818-b5d3-287ba28f8f24","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "2d3e0451-2f05-4f82-87f5-47bab1232662","userdefined_animFPS": false,"x": 96,"y": 736},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_6780424B","id": "ca43f89f-91d8-4578-ae95-3c2ef60b1f81","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "2d3e0451-2f05-4f82-87f5-47bab1232662","userdefined_animFPS": false,"x": 96,"y": 224},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_21B3258B","id": "cb29e65f-1b0a-4cc1-8b95-31ecbe84309c","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "2d3e0451-2f05-4f82-87f5-47bab1232662","userdefined_animFPS": false,"x": 96,"y": -288},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_65C460D9","id": "551f6ac8-1222-41d8-93ea-e5884db35703","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "37eba367-ba88-4082-9e19-49f700b7b048","userdefined_animFPS": false,"x": 96,"y": 3296},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_7A1C23A2","id": "ffd75d13-3ef0-49a8-a3d0-bf91e46e95b9","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "9ea29323-9c92-4def-97dd-343e79863354","userdefined_animFPS": false,"x": 288,"y": 192}
            ],
            "depth": 700,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRAssetLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Player",
            "id": "2f3e8c4c-581e-4b55-a42a-fdc72988cc44",
            "depth": 800,
            "grid_x": 8,
            "grid_y": 8,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_2AA9F710","id": "8fcca061-3fbb-42c2-b3d4-ab48b16e716f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2AA9F710","objId": "b51ef966-717c-47b2-97a9-a3ed50ffdad8","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 288,"y": 3048}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Door",
            "id": "e14b7736-1829-47a0-8310-62a316531f7a",
            "depth": 900,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_7E79D9CE","id": "3e8215f5-30c0-4edf-b3c5-49982e56fd9f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7E79D9CE","objId": "5be5bc02-708b-4ca7-a83d-121c4392276b","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 288,"y": 128}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "db3f5dc2-6a28-4453-9325-0c28df29051b",
            "depth": 1000,
            "grid_x": 8,
            "grid_y": 8,
            "hierarchyFrozen": false,
            "hierarchyVisible": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_516D6A4","id": "81c20677-6af8-42a5-aeb4-112ab7b39dc2","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_516D6A4","objId": "a0524acc-83e8-48fe-b03c-248e27dd7739","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": -164,"y": 172},
{"name": "inst_2BC5CC63","id": "039f8412-ed4d-4bf7-b626-e8dcb19d9cae","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2BC5CC63","objId": "22bea365-3d22-4656-a0f2-c527b934a613","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 576,"y": 3024},
{"name": "inst_20917636","id": "e792a5b1-dba7-4ea9-a782-1a99af93d645","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_20917636","objId": "8618ca23-c05f-484e-80e7-da8db7068850","properties": [{"id": "11ae976b-da6a-44e6-afe5-f5e9565a872b","modelName": "GMOverriddenProperty","objectId": "8618ca23-c05f-484e-80e7-da8db7068850","propertyId": "d9de17af-0ac7-4ad2-ba47-c1b441e5e323","mvc": "1.0","value": ".05"}],"rotation": 0,"scaleX": 14,"scaleY": 1,"mvc": "1.1","x": 288,"y": 3016},
{"name": "inst_474000D4","id": "ac3da6c0-e290-4c13-b6a3-b81427d50da1","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_474000D4","objId": "13cb11f0-1b9a-464c-b36a-c486b847d062","properties": [{"id": "0441bc02-2a92-4fa4-bdc7-7e1c74fd81a3","modelName": "GMOverriddenProperty","objectId": "13cb11f0-1b9a-464c-b36a-c486b847d062","propertyId": "916eea18-d1af-429e-a441-78f0c513e7e2","mvc": "1.0","value": "96"},{"id": "15c3765c-499d-4fc0-9e5f-7d31471da16f","modelName": "GMOverriddenProperty","objectId": "13cb11f0-1b9a-464c-b36a-c486b847d062","propertyId": "4592a2db-c4fd-4719-a8e2-f38c0c6e44cb","mvc": "1.0","value": "-286"},{"id": "da15750e-2f8a-4a7d-ab18-70199fda953d","modelName": "GMOverriddenProperty","objectId": "13cb11f0-1b9a-464c-b36a-c486b847d062","propertyId": "601f9e44-cb63-491d-8dd6-b47be7b5c778","mvc": "1.0","value": "120"},{"id": "35d3bdde-f9c8-4347-92d9-a1b8e7edbf30","modelName": "GMOverriddenProperty","objectId": "13cb11f0-1b9a-464c-b36a-c486b847d062","propertyId": "dd4e42b0-aeee-4d9a-94c2-c24bc056239a","mvc": "1.0","value": "3296"}],"rotation": 0,"scaleX": 1,"scaleY": 112,"mvc": "1.1","x": 112,"y": 1504},
{"name": "inst_6AE793F","id": "bc656cee-6eda-4d5a-be25-50ac32726ae9","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6AE793F","objId": "13cb11f0-1b9a-464c-b36a-c486b847d062","properties": [{"id": "35d3c0e7-3275-40be-af37-dda45b95cf56","modelName": "GMOverriddenProperty","objectId": "13cb11f0-1b9a-464c-b36a-c486b847d062","propertyId": "916eea18-d1af-429e-a441-78f0c513e7e2","mvc": "1.0","value": "454"},{"id": "b42e9b58-ff6b-4450-853f-cacdcfddef36","modelName": "GMOverriddenProperty","objectId": "13cb11f0-1b9a-464c-b36a-c486b847d062","propertyId": "4592a2db-c4fd-4719-a8e2-f38c0c6e44cb","mvc": "1.0","value": "-286"},{"id": "b7b6fdd2-3ec4-4c28-83cf-578669e71606","modelName": "GMOverriddenProperty","objectId": "13cb11f0-1b9a-464c-b36a-c486b847d062","propertyId": "601f9e44-cb63-491d-8dd6-b47be7b5c778","mvc": "1.0","value": "478"},{"id": "dd4f3fb1-82cf-4128-bb26-635cb7da61c2","modelName": "GMOverriddenProperty","objectId": "13cb11f0-1b9a-464c-b36a-c486b847d062","propertyId": "dd4e42b0-aeee-4d9a-94c2-c24bc056239a","mvc": "1.0","value": "3296"}],"rotation": 0,"scaleX": 1,"scaleY": 112,"mvc": "1.1","x": 464,"y": 1504},
{"name": "inst_101DC683","id": "5bfe5e10-fd07-48a4-9e18-c43c36d7d91a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_101DC683","objId": "01e74b63-c916-43f7-867d-32bb2c358abb","properties": null,"rotation": 0,"scaleX": 12,"scaleY": 1,"mvc": "1.1","x": 288,"y": 3312},
{"name": "inst_1DAC9146","id": "94e00776-68bd-4909-b65b-0cad3f2c9ada","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1DAC9146","objId": "13cb11f0-1b9a-464c-b36a-c486b847d062","properties": [{"id": "a98d77c2-a7eb-4c3b-9bf4-f45941451347","modelName": "GMOverriddenProperty","objectId": "13cb11f0-1b9a-464c-b36a-c486b847d062","propertyId": "916eea18-d1af-429e-a441-78f0c513e7e2","mvc": "1.0","value": "0"},{"id": "faa73d45-74cd-40c0-85f6-725a01681f8e","modelName": "GMOverriddenProperty","objectId": "13cb11f0-1b9a-464c-b36a-c486b847d062","propertyId": "dd4e42b0-aeee-4d9a-94c2-c24bc056239a","mvc": "1.0","value": "3320"},{"id": "58dba7d5-f0e0-4850-94ce-dc9b7729a1e1","modelName": "GMOverriddenProperty","objectId": "13cb11f0-1b9a-464c-b36a-c486b847d062","propertyId": "4592a2db-c4fd-4719-a8e2-f38c0c6e44cb","mvc": "1.0","value": "3296"},{"id": "46a93df4-b9f9-4e7d-b7e5-4d283d46e4e2","modelName": "GMOverriddenProperty","objectId": "13cb11f0-1b9a-464c-b36a-c486b847d062","propertyId": "601f9e44-cb63-491d-8dd6-b47be7b5c778","mvc": "1.0","value": "576"}],"rotation": 0,"scaleX": 18,"scaleY": 0.75,"mvc": "1.1","x": 288,"y": 3308},
{"name": "inst_4D0A347A","id": "3b72fcd6-856d-4d8d-85fd-f7320ab41853","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4D0A347A","objId": "01e74b63-c916-43f7-867d-32bb2c358abb","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 112.75,"mvc": "1.1","x": 488,"y": 1504},
{"name": "inst_456365D4","id": "51ab2fd7-29a5-489f-83b1-55aec79a6642","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_456365D4","objId": "01e74b63-c916-43f7-867d-32bb2c358abb","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 112.25,"mvc": "1.1","x": 88,"y": 1504},
{"name": "inst_1238E5B","id": "28a91b78-70ac-47cc-ab70-588a0ba13e5d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1238E5B","objId": "8618ca23-c05f-484e-80e7-da8db7068850","properties": [{"id": "35aab209-049f-40b1-8a96-4d92aa838b1f","modelName": "GMOverriddenProperty","objectId": "8618ca23-c05f-484e-80e7-da8db7068850","propertyId": "d9de17af-0ac7-4ad2-ba47-c1b441e5e323","mvc": "1.0","value": ".1"},{"id": "20e9d1cc-463a-4b66-87f3-e91169030544","modelName": "GMOverriddenProperty","objectId": "8618ca23-c05f-484e-80e7-da8db7068850","propertyId": "a959ac69-0d05-4303-b319-72d4be398c8f","mvc": "1.0","value": "True"}],"rotation": 0,"scaleX": 14,"scaleY": 1,"mvc": "1.1","x": 288,"y": 2568},
{"name": "inst_658C0F5D","id": "da1a3cce-798a-4127-976c-c9764bd85df5","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_658C0F5D","objId": "8618ca23-c05f-484e-80e7-da8db7068850","properties": [{"id": "c5d7eff7-806d-42b5-b169-8f52c2f105ff","modelName": "GMOverriddenProperty","objectId": "8618ca23-c05f-484e-80e7-da8db7068850","propertyId": "d9de17af-0ac7-4ad2-ba47-c1b441e5e323","mvc": "1.0","value": ".15"},{"id": "d2f61394-64ba-4929-9b22-51d32cd9bacb","modelName": "GMOverriddenProperty","objectId": "8618ca23-c05f-484e-80e7-da8db7068850","propertyId": "a959ac69-0d05-4303-b319-72d4be398c8f","mvc": "1.0","value": "False"}],"rotation": 0,"scaleX": 14,"scaleY": 1,"mvc": "1.1","x": 296,"y": 2144},
{"name": "inst_7FDDD634","id": "9600cc8b-105b-42f1-b6fb-97640916e5e4","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7FDDD634","objId": "8618ca23-c05f-484e-80e7-da8db7068850","properties": [{"id": "d3764387-152d-4bad-bd40-c2c11b24f242","modelName": "GMOverriddenProperty","objectId": "8618ca23-c05f-484e-80e7-da8db7068850","propertyId": "d9de17af-0ac7-4ad2-ba47-c1b441e5e323","mvc": "1.0","value": ".20"},{"id": "18251555-945e-442a-83ad-e953959e8362","modelName": "GMOverriddenProperty","objectId": "8618ca23-c05f-484e-80e7-da8db7068850","propertyId": "a959ac69-0d05-4303-b319-72d4be398c8f","mvc": "1.0","value": "False"}],"rotation": 0,"scaleX": 14,"scaleY": 1,"mvc": "1.1","x": 288,"y": 1712},
{"name": "inst_D48F988","id": "4fc6f8a4-3bca-4594-b200-e042eaa1c182","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_D48F988","objId": "8618ca23-c05f-484e-80e7-da8db7068850","properties": [{"id": "dc7c278f-dcaf-431f-9e0d-e4387748ce28","modelName": "GMOverriddenProperty","objectId": "8618ca23-c05f-484e-80e7-da8db7068850","propertyId": "d9de17af-0ac7-4ad2-ba47-c1b441e5e323","mvc": "1.0","value": ".3"},{"id": "0d4ea5de-27cd-4783-9b33-c26aca522073","modelName": "GMOverriddenProperty","objectId": "8618ca23-c05f-484e-80e7-da8db7068850","propertyId": "a959ac69-0d05-4303-b319-72d4be398c8f","mvc": "1.0","value": "False"}],"rotation": 0,"scaleX": 14,"scaleY": 1,"mvc": "1.1","x": 288,"y": 1272},
{"name": "inst_128E0140","id": "6c1b8d07-0951-4ca2-92ed-146bd438d2c5","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_128E0140","objId": "8618ca23-c05f-484e-80e7-da8db7068850","properties": [{"id": "7d908c59-bf97-4b0b-8aaf-7a8eacd29eae","modelName": "GMOverriddenProperty","objectId": "8618ca23-c05f-484e-80e7-da8db7068850","propertyId": "d9de17af-0ac7-4ad2-ba47-c1b441e5e323","mvc": "1.0","value": ".4"},{"id": "4ff05f7a-1608-4faf-a878-173afa707da3","modelName": "GMOverriddenProperty","objectId": "8618ca23-c05f-484e-80e7-da8db7068850","propertyId": "a959ac69-0d05-4303-b319-72d4be398c8f","mvc": "1.0","value": "False"}],"rotation": 0,"scaleX": 14,"scaleY": 1,"mvc": "1.1","x": 280,"y": 864},
{"name": "inst_398B1589","id": "8ac9b672-2b56-4eb5-8c3b-de8068dcb8f2","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_398B1589","objId": "8618ca23-c05f-484e-80e7-da8db7068850","properties": [{"id": "ad9703f6-5600-47c1-9999-55c2a45add78","modelName": "GMOverriddenProperty","objectId": "8618ca23-c05f-484e-80e7-da8db7068850","propertyId": "d9de17af-0ac7-4ad2-ba47-c1b441e5e323","mvc": "1.0","value": ".5"},{"id": "afb47aa0-5600-42fd-9441-8cc9888f92ec","modelName": "GMOverriddenProperty","objectId": "8618ca23-c05f-484e-80e7-da8db7068850","propertyId": "a959ac69-0d05-4303-b319-72d4be398c8f","mvc": "1.0","value": "False"}],"rotation": 0,"scaleX": 14,"scaleY": 1,"mvc": "1.1","x": 288,"y": 496},
{"name": "inst_422E2D6F","id": "c3d04dea-3ae8-45e5-a9b1-2a4bf3ec5d26","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_422E2D6F","objId": "13cb11f0-1b9a-464c-b36a-c486b847d062","properties": null,"rotation": 0,"scaleX": 4,"scaleY": 2,"mvc": "1.1","x": 288,"y": 192}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": false
        },
        {
            "__type": "GMRAssetLayer_Model:#YoYoStudio.MVCFormat",
            "name": "grid",
            "id": "1220595b-f9d7-4596-ba52-30aa33521d3b",
            "assets": [
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_19A6137","id": "223552a7-21ef-4f68-ae61-8759954d279a","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "b6274efb-3128-4143-8c35-7b63ed3c9a4d","userdefined_animFPS": false,"x": 447,"y": 3295},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_F4CEAD8","id": "12d336be-2d2c-47fd-ae84-5aff44597b35","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "b6274efb-3128-4143-8c35-7b63ed3c9a4d","userdefined_animFPS": false,"x": 447,"y": 2783},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_2A91212A","id": "49414d67-072d-4240-aaf7-3530d89d6404","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "b6274efb-3128-4143-8c35-7b63ed3c9a4d","userdefined_animFPS": false,"x": 447,"y": 2271},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_7221DC43","id": "2d93a99a-d266-44e0-afef-b0045537736a","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "b6274efb-3128-4143-8c35-7b63ed3c9a4d","userdefined_animFPS": false,"x": 447,"y": 1759},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_10D53C18","id": "2dff0f94-55cc-43d5-9a96-d63a36962849","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "b6274efb-3128-4143-8c35-7b63ed3c9a4d","userdefined_animFPS": false,"x": 447,"y": 1247},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_57724E0E","id": "f264fe64-6900-48ae-bc9c-fd76b69427b1","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "b6274efb-3128-4143-8c35-7b63ed3c9a4d","userdefined_animFPS": false,"x": 447,"y": 735},
{"__type": "GMRSpriteGraphic_Model:#YoYoStudio.MVCFormat","name": "graphic_64DE9578","id": "9315a332-9185-42b7-8648-aaa55d5a6342","animationFPS": 15,"animationSpeedType": "0","colour": { "Value": 4294967295 },"frameIndex": 0,"ignore": false,"inheritItemSettings": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRSpriteGraphic","rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","spriteId": "b6274efb-3128-4143-8c35-7b63ed3c9a4d","userdefined_animFPS": false,"x": 447,"y": 223}
            ],
            "depth": 1100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRAssetLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "befa48c6-5c71-4c6b-a1d6-659958c32072",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 1200,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "cb8d7c0a-a615-490d-b065-969d13fbd66a",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": true,
            "x": 128,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "51b60e78-fe9d-4aae-b889-0aed02aea05a",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "b2304339-03f7-4b31-914e-37281a40bf59",
        "Height": 3840,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 576
    },
    "mvc": "1.0",
    "views": [
{"id": "92eb830c-3d7b-4801-ba11-fde1502fbd34","hborder": 192,"hport": 512,"hspeed": -1,"hview": 512,"inherit": false,"modelName": "GMRView","objId": "a0524acc-83e8-48fe-b03c-248e27dd7739","mvc": "1.0","vborder": 256,"visible": true,"vspeed": -1,"wport": 385,"wview": 385,"xport": 96,"xview": 96,"yport": 0,"yview": 0},
{"id": "e34de2df-58a9-4f0c-ad90-49efd20d30a5","hborder": 32,"hport": 512,"hspeed": -1,"hview": 512,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 106,"wview": 106,"xport": 0,"xview": 0,"yport": 0,"yview": 3328},
{"id": "f95eca7e-e5be-40aa-8064-d4a4f8fb8b8e","hborder": 32,"hport": 512,"hspeed": -1,"hview": 512,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 106,"wview": 106,"xport": 470,"xview": 470,"yport": 0,"yview": 3328},
{"id": "74ce6537-e77c-4047-9f9e-e2ae1a443a88","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "2c29acdb-1939-47f9-ad01-1d4d18afc4c0","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "9c7b5230-79b6-4001-8b1f-bbd795b2ebfd","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "5bad1d44-6609-4451-a11b-f6c32ec7c3c5","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "926a421c-6f5e-490a-b60d-46cdce212249","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "c9169dd8-6f77-4d05-945a-d06add6738f3",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}